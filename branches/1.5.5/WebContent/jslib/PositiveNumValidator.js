/*
 * Apache Trinidad client side implementation for validating an email.
 * The implementation is used by org.dotcomm.pagecode.validators.EmailValidator
 *
 * [\w\.-]+ : Begins with word characters, (may include periods and hypens).
 * @ : It must have a �@� symbol after initial characters.
 * ([\w\-]+\.)+ : �@� must follow by more alphanumeric characters (may include hypens.).
 * This part must also have a �.� to separate domain and subdomain names.
 * [A-Z]{2,4}$ : Must end with two to four alaphabets.
 * (This will allow domain names with 2, 3 and 4 characters e.g pa, com, net, wxyz)
 *
 * valid emails: abc@xyz.net; ab.c@tx.gov
 */

PositiveNumValidator = function() {}

PositiveNumValidator.prototype = new TrValidator();

PositiveNumValidator.prototype.validate = function (value, label, converter) {
    var facesMessage = new TrFacesMessage("Validation Error",
                                          "Invalid Value. Must be zero or greater.",
                                          TrFacesMessage.SEVERITY_ERROR);

    if (value < 0) {
        //todo get message from resource bundle, or inject
        throw new TrValidatorException(facesMessage);
    }
}