/*
 * Apache Trinidad client side implementation for validating a phone number.
 * The implementation is used by org.dotcomm.pagecode.validators.PhoneNumValidator
 *
 * Phone number format: xxx-xxxx or xxx-xxx-xxxx
 */

PhoneNumValidator = function() {}

PhoneNumValidator.prototype = new TrValidator();

PhoneNumValidator.prototype.validate = function (value, label, converter) {
    var facesMessage = new TrFacesMessage("Validation Error",
                                          "Invalid Format. Please use format: 123-456-7890 or 456-7890.",
                                          TrFacesMessage.SEVERITY_ERROR);

    var re = new RegExp("^([0-9]{3}?[-])?[0-9]{3}[-]{1}[0-9]{4}$");

    if (!re.test(value)) {
        //todo get message from resource bundle, or inject
        throw new TrValidatorException(facesMessage);
    }
}