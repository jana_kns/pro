package org.dotcomm.pagecode.validators

import javax.faces.validator.Validator
import javax.faces.application.FacesMessage
import javax.faces.context.FacesContext
import java.util.regex.Matcher
import javax.faces.validator.ValidatorException
import java.util.regex.Pattern
import javax.faces.component.UIComponent
import org.apache.myfaces.trinidad.validator.ClientValidator

/**
 * Jsf custom validator for phone numbers.
 *
 * @author jdolinski
 */
class PostiveNumValidator implements Validator, ClientValidator {

    void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        /** retrieve the string value of the field */
        Number number = (Number)value

        if (number < 0) {
            FacesMessage msg = new FacesMessage()
            msg.setDetail('Invalid Value. Must be zero or greater.')
            msg.setSummary('Invalid Value. Must be zero or greater.')
            msg.setSeverity(FacesMessage.SEVERITY_ERROR)
            //todo get message from resource bundle, or inject
            throw new ValidatorException(msg)
        }
    }

    public String getClientLibrarySource(FacesContext facesContext) {
        return facesContext.getExternalContext().getRequestContextPath() + '/jslib/PositiveNumValidator.js'
    }

    public Collection<String> getClientImportNames() {
        return null
    }

    public String getClientScript(FacesContext facesContext, UIComponent uiComponent) {
        return null
    }

    public String getClientValidation(FacesContext facesContext, UIComponent uiComponent) {
        return "new PositiveNumValidator()"
    }

}