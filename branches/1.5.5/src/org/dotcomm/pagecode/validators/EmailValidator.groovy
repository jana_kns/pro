package org.dotcomm.pagecode.validators

import javax.faces.validator.ValidatorException
import javax.faces.application.FacesMessage
import java.util.regex.Pattern
import java.util.regex.Matcher
import javax.faces.context.FacesContext
import javax.faces.component.UIComponent
import javax.faces.validator.Validator
import org.apache.myfaces.trinidad.validator.ClientValidator

/**
 * Jsf custom validator for email addresses.
 *
 * @author jdolinski
 */
class EmailValidator implements Validator, ClientValidator {

    /**
     * [\\w\\.-]+ : Begins with word characters, (may include periods and hypens).
     * @ : It must have a �@� symbol after initial characters.
     * ([\\w\\-]+\\.)+ : �@� must follow by more alphanumeric characters (may include hypens.).
     * This part must also have a �.� to separate domain and subdomain names.
     * [A-Z]{2,4}$ : Must end with two to four alaphabets.
     * (This will allow domain names with 2, 3 and 4 characters e.g pa, com, net, wxyz)
     *
     * valid emails: abc@xyz.net; ab.c@tx.gov
     */
    private static final String EMAIL = '^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$'

    /** Error message to send back to user */
    String errMsg = "Invalid Format. Please use format: abc@xyz.com"

    void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        /** create a mask */
        Pattern mask = Pattern.compile(EMAIL, Pattern.CASE_INSENSITIVE)

        /** retrieve the string value of the field */
        String email = (String)value

        /** check to ensure that the value is valid */
        Matcher matcher = mask.matcher(email)

        if (!matcher.matches()) {
            FacesMessage msg = new FacesMessage()
            msg.setDetail(errMsg)
            msg.setSummary(errMsg)
            msg.setSeverity(FacesMessage.SEVERITY_ERROR)
            //todo get message from resource bundle, or inject
            throw new ValidatorException(msg)
        }
    }

    public String getClientLibrarySource(FacesContext facesContext) {
        return facesContext.getExternalContext().getRequestContextPath() + '/jslib/EmailValidator.js'
    }

    public Collection<String> getClientImportNames() {
        return null
    }

    public String getClientScript(FacesContext facesContext, UIComponent uiComponent) {
        return null
    }

    public String getClientValidation(FacesContext facesContext, UIComponent uiComponent) {
        return "new EmailValidator()"
    }
    
}