package org.dotcomm.pagecode.workorder

import javax.faces.model.SelectItem
import org.apache.myfaces.trinidad.event.RangeChangeEvent
import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.services.WorkOrderService
import org.dotcomm.util.CsvBean
import org.dotcomm.util.FacesUtils
import org.dotcomm.util.PagedListDataModel
import org.dotcomm.pagecode.adaptors.*
import javax.faces.event.ActionEvent
import org.springframework.security.AccessDeniedException

/**
 * Jsf managed bean for the searchwo.xhtml.
 *
 * @author jdolinski
 */
class SearchWo {

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Injected Property */
    WorkOrderService workOrderService

    /** Page code data */
    WorkOrderRow workOrderRow

    /** Page code data */
    List<WorkOrderRow> pcListWorkOrders

    /** Page code data */
    List<SelectItem> listLocations

    /** Page code data */
    List<SelectItem> listDepts

    /** Page code data */
    List<SelectItem> listStatus

    /** Page code data */
    List<SelectItem> listWoTypes

    /** Page code data */
    List<SelectItem> listMtnceGroups

    /** Page code data */
    List<SelectItem> listUsers

    /** Page code data */
    List<SelectItem> listServiceItems

    /** Page code data */
    Integer pcServiceItemId

    /** Page code data, database pagination */
    PagedListDataModel dataModel

    /** Bean to download data from table into csv format. */
    CsvBean csvBean

    /** Constructor */
    SearchWo(AdminSecurityService adminSecurityService,
                    AdminWorkOrderService adminWorkOrderService,
                    WorkOrderService workOrderService) {
        this.adminSecurityService = adminSecurityService
        this.adminWorkOrderService = adminWorkOrderService
        this.workOrderService = workOrderService
        csvBean = new CsvBean()
        workOrderRow = new WorkOrderRow()
        populateDropdowns()
        this.workOrderRow.sortOrder = (String)FacesUtils.getPageFlowScopeParameter('sortOrder')
    }

    /**
     * Get data available for selections.
     */
    private void populateDropdowns() {
        SelectLocationRowAdaptor selectLocationRowAdaptor = new SelectLocationRowAdaptor()
        listLocations = selectLocationRowAdaptor.adapt(adminSecurityService.getAllActiveLocations())
        SelectDeptAdaptor selectDeptAdaptor = new SelectDeptAdaptor()
        listDepts = selectDeptAdaptor.adapt(adminSecurityService.getAllActiveDepartments())
        SelectStatusRowAdaptor selectStatusRowAdaptor = new SelectStatusRowAdaptor()
        listStatus = selectStatusRowAdaptor.adapt(workOrderService.getAllWoStatusItems())
        SelectWoTypeAdaptor selectWoTypeAdaptor = new SelectWoTypeAdaptor()
        listWoTypes = selectWoTypeAdaptor.adapt(adminWorkOrderService.getAllActiveWorkOrderTypeItems())
        SelectMtnceGroupRowAdaptor selectMtnceGroupRowAdaptor = new SelectMtnceGroupRowAdaptor()
        listMtnceGroups = selectMtnceGroupRowAdaptor.adapt(adminSecurityService.getAllActiveMtnceGroups())
        SelectUserRowAdaptor selectUserRowAdaptor = new SelectUserRowAdaptor()
        listUsers = selectUserRowAdaptor.adapt(adminSecurityService.getAllUsers())
        SelectServiceGroupItemRowAdaptor itemRowAdaptor = new SelectServiceGroupItemRowAdaptor()
        listServiceItems = itemRowAdaptor.adapt(adminWorkOrderService.getAllActiveServiceItems())
    }

    /**
     * Invoke search for work orders.
     */
    public String doButtonSearch() {
        try {
            search(0)
        } catch (AccessDeniedException e) {
            return 'accessdenied'
        }
        return 'searchwo'
    }

    /**
     * Refresh the previously searched work orders keeping paged data location.
     * @return The navigation rule.
     */
    public String doRefreshSearch() {
        if (csvBean.table != null) {
            search(dataModel?.first==null?0:dataModel.first)
        }
        return "searchwo"
    }

    /**
     * Search work orders
     * @param pageNum The database page user last navigated.
     */
    private void search(Integer pageNum) {
        List<Integer> selectedServiceItem = new ArrayList<Integer>()
        if (pcServiceItemId != null) {
            selectedServiceItem.add (pcServiceItemId)
        }
        this.workOrderRow.listServiceItems = selectedServiceItem
        //validate from date does not occur after the end date
        if (this.workOrderRow.endCreateDateTime != null
        &&  this.workOrderRow.startCreateDateTime > this.workOrderRow.endCreateDateTime) {
            FacesUtils.addErrorMessage FacesUtils.getStringFromBundle('fromdatebeforetodate')
        }
        else {
            Integer totalSize = workOrderService.getCountWorkOrders(this.workOrderRow);
            csvBean.table.first = pageNum
            pcListWorkOrders = workOrderService.getFilteredWorkOrders(this.workOrderRow, csvBean.table.first, csvBean.table.rows)
            dataModel = new PagedListDataModel(pcListWorkOrders, totalSize, csvBean.table.rows);
            dataModel.first = csvBean.table.first
        }
    }

    /**
     * View the work order
     * @return The navigation rule
     */
    public String doView() {
        Integer woId = (Integer)FacesUtils.getPageFlowScopeParameter("woid")
        this.workOrderService.updateStatusMtnceViewed(woId)
        return "viewwo"
    }

    /**
     * Edit the work order
     * @return The navigation rule
     */
    public String doEdit() {
        Integer woId = (Integer)FacesUtils.getPageFlowScopeParameter("woid")
        this.workOrderService.updateStatusMtnceViewed(woId)
        return "editwo"
    }

    /**
     * Clear the search filters.
     */
    public void clear() {
        this.workOrderRow.woStatusId = null
        this.workOrderRow.woLocationId = null
        this.workOrderRow.woDepartmentId = null
        this.workOrderRow.woNumber = null
        this.pcServiceItemId = null
        this.workOrderRow.woTypeId = null
        this.workOrderRow.mtnceGroup = null
        this.workOrderRow.woRequestorUserId = null
        this.workOrderRow.archived = null
        this.workOrderRow.startCreateDateTime = null
        this.workOrderRow.endCreateDateTime = null
        this.workOrderRow.woReqComments = null
    }

    /**
     * Used to implement database pagination.
     * @param e The event when the range changes.
     */
    void rangeChange(RangeChangeEvent e) {
        if (csvBean.table.showAll) { //we are showing all records
            pcListWorkOrders = workOrderService.getFilteredWorkOrders(this.workOrderRow, null, null)
            dataModel = new PagedListDataModel(pcListWorkOrders, csvBean.table.rowCount, pcListWorkOrders.size());
        }
        else { //Only display a page of data at a time, based on the tables rows attribute
            pcListWorkOrders = workOrderService.getFilteredWorkOrders(this.workOrderRow, csvBean.table.first, csvBean.table.rows)
            dataModel = new PagedListDataModel(pcListWorkOrders, csvBean.table.rowCount, csvBean.table.rows);
            dataModel.first = csvBean.table.first
        }
    }


   /**
    * Sort the selected column on the search results table.
    * @param e The selected column.
    */
    void sortColumn(ActionEvent e) {
        this.workOrderRow.sortOrder = (String)FacesUtils.getPageFlowScopeParameter('sortOrder')
        String sortDeptValue = "locationId.${e.component.id}"
        if (this.workOrderRow.sortOrder.equals(e.component.id) || this.workOrderRow.sortOrder.equals(sortDeptValue)) {
            this.workOrderRow.sortOrder = "${e.component.id} desc"
        } else {
            this.workOrderRow.sortOrder = e.component.id
        }
        this.workOrderRow.sortOrder = this.workOrderRow.sortOrder.replaceAll("deptId","locationId.deptId")
        FacesUtils.putPageFlowScopeParameter 'sortOrder', this.workOrderRow.sortOrder
        doButtonSearch()
    }
}
