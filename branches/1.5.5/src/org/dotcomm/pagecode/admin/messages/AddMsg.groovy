package org.dotcomm.pagecode.admin.messages

import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.pagecode.rows.MsgPostRow
import org.dotcomm.pagecode.admin.AdminMain
import org.dotcomm.util.FacesUtils

/**
 * Jsf managed bean for the addmsg.xhtml.
 *
 * @author jdolinski
 */
class AddMsg {

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService
    
    /** Page code data */
    MsgPostRow msgPostRow

    /** Constructor */
    AddMsg() {
        msgPostRow = new MsgPostRow()
    }

    /**
     * Action method to add a new message to the system.
     * @return Faces navigation text.
     */
    String newMsg() {
        adminWorkOrderService.addNewMsgPost (msgPostRow)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

}
