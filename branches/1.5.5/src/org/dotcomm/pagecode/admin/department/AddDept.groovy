package org.dotcomm.pagecode.admin.department

import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.rows.DeptRow
import org.dotcomm.pagecode.admin.AdminMain
import org.dotcomm.util.FacesUtils;

/**
 * Jsf managed bean for the adddept.xhtml.
 *
 * @author jdolinski
 */
class AddDept {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Page code data */
    DeptRow deptRow

    /** Constructor */
    AddDept() {
        deptRow = new DeptRow()
        deptRow.deptActive = new Boolean(true)
    }

    /**
     * Action method to add a new department to the system.
     * @return Faces navigation text.
     */
    String newDept() {
        adminSecurityService.addNewDept (deptRow)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

}
