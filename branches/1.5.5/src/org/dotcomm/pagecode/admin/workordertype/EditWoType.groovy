package org.dotcomm.pagecode.admin.workordertype

import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.rows.WoTypeRow
import org.dotcomm.pagecode.admin.AdminMain

/**
 * Jsf managed bean for the editwotype.xhtml.
 *
 * @author jdolinski
 */
class EditWoType {

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** Page code data */
    WoTypeRow woTypeRow

    /** Constructor */
    EditWoType(AdminWorkOrderService adminWorkOrderService) {
        this.adminWorkOrderService = adminWorkOrderService
        Integer woTypeId = (Integer)FacesUtils.getPageFlowScopeParameter("wotypeid")
        woTypeRow = this.adminWorkOrderService.getWorkOrderTypeItem(woTypeId)
    }

    /**
     * Action method to edit an existing work order type in the system.
     * @return Faces navigation text.
     */
    String updateWoType() {
        adminWorkOrderService.updateWoType(woTypeRow)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

}
