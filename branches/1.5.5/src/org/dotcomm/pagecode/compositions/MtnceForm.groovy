package org.dotcomm.pagecode.compositions

import org.dotcomm.services.AdminSecurityService
import javax.faces.model.SelectItem
import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.pagecode.adaptors.SelectWoTypeAdaptor
import org.dotcomm.pagecode.adaptors.SelectUserRowAdaptor
import org.dotcomm.pagecode.adaptors.SelectLocationRowAdaptor
import org.dotcomm.pagecode.rows.MtnceGroupRow

/**
 * Facelet compostion backing bean for the mtnceform.xhtml.
 *
 * @author JDolinski
 */
class MtnceForm {

    /** Page code data */
    MtnceGroupRow mtnceGroupRow

    /** All active work order types */
    List<SelectItem> pcListWoTypes

    /** All active users that have the maintenance role */
    List<SelectItem> pcListUsers

    /** All active locations */
    List<SelectItem> pcListLocations

    /** Constructor */
    MtnceForm(AdminSecurityService adminSecurityService, AdminWorkOrderService adminWorkOrderService) {
        SelectWoTypeAdaptor selectWoTypeAdaptor = new SelectWoTypeAdaptor()
        SelectUserRowAdaptor selectUserRowAdaptor = new SelectUserRowAdaptor()
        SelectLocationRowAdaptor selectLocationRowAdaptor = new SelectLocationRowAdaptor()
        pcListWoTypes = selectWoTypeAdaptor.adapt(adminWorkOrderService.getAllActiveWorkOrderTypeItems())
        pcListUsers = selectUserRowAdaptor.adapt(adminSecurityService.getAllActiveMtnceUsers())
        pcListLocations = selectLocationRowAdaptor.adapt(adminSecurityService.getAllActiveLocations())
    }

}