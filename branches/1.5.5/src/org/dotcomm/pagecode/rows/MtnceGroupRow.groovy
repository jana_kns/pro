package org.dotcomm.pagecode.rows
/**
 * Page code bean for displaying maintenance group information.
 *
 * @author jdolinski
 */
class MtnceGroupRow {

    Integer groupId
    Date lastUpd
    String groupName
    String groupDesc
    String groupStatus
    Boolean active
    List<UserRow> listMtncePersonnel
    List<Integer> listMtncePersonnelSelections
    List<Integer> listLocations
    List<Integer> listWoTypeSelections
    
}