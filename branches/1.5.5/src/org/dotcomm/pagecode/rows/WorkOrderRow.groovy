package org.dotcomm.pagecode.rows;

/**
 * Page code bean for displaying work order information.
 *
 * @author jdolinski
 */
class WorkOrderRow {

    Long woId
    BigInteger woNumber
    Date createDateTime
    Date startCreateDateTime
    Date endCreateDateTime
    Boolean archived
    String archivedString
    Integer woStatusId
    String woStatus
    Integer woLocationId
    String woLocation
    Integer woDepartmentId
    String woDepartment
    Integer woTypeId
    Integer woTypeIdOld
    String woType
    Date woReqCompDate
    String woReqComments
    String woRequestor
    String woRequestorPhone
    String woRequestorEmail
    Integer woRequestorUserId
    Date woEnteredDate
    String woLastUpdated
    Date woLastUpdDate
    Date woStartDate
    Date woCompletionDate
    String woCompletedBy
    String woInstructions
    String woCompletionComments
    String woChrgManHrsAcct
    String woChrgPartsAcct
    Integer woNumberTrips
    Integer woNumberPersons
    Double woManHrs
    Double woTravelHrs
    Double woTotalHrs
    List<MaterialRow> listMaterials
    String materials
    List<Integer> listServiceItems
    String serviceItems
    Integer mtnceGroup
    String sortOrder
    
}
