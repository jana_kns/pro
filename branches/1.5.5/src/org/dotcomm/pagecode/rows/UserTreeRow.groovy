package org.dotcomm.pagecode.rows;

/**
 * Page code bean for displaying user's heirarchy in a tree.
 *
 * @author jdolinski
 */
class UserTreeRow {

    Integer id
    String name
    boolean renderEdit
    String navText
    List<UserTreeRow> children = new ArrayList()

    /** Constructor */
    UserTreeRow(Integer id, String name, String navText, boolean renderEdit) {
        this.id = id
        this.name = name
        this.navText = navText
        this.renderEdit = renderEdit
    }

}
