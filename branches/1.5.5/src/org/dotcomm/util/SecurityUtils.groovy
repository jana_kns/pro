package org.dotcomm.util

import org.springframework.security.userdetails.UserDetails
import org.springframework.security.context.SecurityContextHolder

/**
 * Define security access utilities.
 * @author jdolinski
 */
interface SecurityUtils {

    /**
     * This method gets the user logged into the application.
     *
     * @return principal The acegi user logged in.
     */
    String getUserNameLoggedIn()

    /**
     * This method gets the user logged into the application.
     *
     * @return userId The database user id of the user logged in.
     */
    Integer getUserIdLoggedIn() 

}