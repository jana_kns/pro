package org.dotcomm.dto.propertymaintenance.adaptors

import org.dotcomm.util.Adaptor
import org.dotcomm.pagecode.rows.WoTypeRow
import org.dotcomm.dto.propertymaintenance.WoType
import org.dotcomm.util.FacesUtils
import org.dotcomm.dto.propertymaintenance.MtnceGroup
import org.dotcomm.pagecode.rows.MtnceGroupRow
import org.dotcomm.persistence.DataAccessObject

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class WorkOrderTypeRowAdaptor implements Adaptor {

    /** Injected Property */
    DataAccessObject dao

    /**
     * Adapt persistant dto for view.
     * @return object for view
     */
    public Object adapt(Object obj) {
        WoTypeRow workOrderTypeRow = new WoTypeRow()
        WoType woType = (WoType) obj
        if (woType != null) {
            workOrderTypeRow.woTypeId = woType.woTypeId
            workOrderTypeRow.woTypeName = woType.name
            workOrderTypeRow.woTypeDesc = woType.descr
            workOrderTypeRow.woLastUpd = woType.lastUpdatedDateTime
            workOrderTypeRow.woTypeStatus = woType.enabled.intValue() == 1 ? 'Active' : 'Inactive'
            workOrderTypeRow.woActive = woType.enabled.intValue() == 1 ? new Boolean(true) : new Boolean(false)
            workOrderTypeRow.listMtnceGroups = new ArrayList<MtnceGroupRow>()
            for (MtnceGroup mg : woType.mtnceGroupMany) {
                workOrderTypeRow.listMtnceGroups.add(new MtnceGroupRow(groupId:mg.mtnceGroupId, groupName:mg.name))
            }
        }
        else {
            return null
        }
        return workOrderTypeRow
    }

    public Class<WoType> getFromType() {
        return WoType
    }

    public Class<WoTypeRow> getToType() {
        return WoTypeRow
    }

    /**
     * Merge object for view to persistant dto.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        WoType woType = new WoType()
        WoTypeRow woTypeRow = (WoTypeRow) obj
        if (woTypeRow != null) {
            woType.woTypeId = woTypeRow.woTypeId
            woType.name = woTypeRow.woTypeName
            woType.descr = woTypeRow.woTypeDesc
            woType.lastUpdatedDateTime = woTypeRow.woLastUpd
            woType.enabled = woTypeRow.woActive ? new Integer(1) : new Integer(0)
            woType.mtnceGroupMany = new ArrayList<MtnceGroup>()
            for (MtnceGroupRow row : woTypeRow.listMtnceGroups) {
                //load() method always tries to return a proxy and only returns
                //an initialized instance if it is already managed in the current persistence context.
                //A get will always hit the database. Since we are just adding a item to a collection
                //we do not need to hit the db and get the value, the proxy will work just fine.
                woType.mtnceGroupMany.add(dao.load(MtnceGroup.class, row.groupId))
            }
        }
        else {
            return null
        }
        return woType
    }

}