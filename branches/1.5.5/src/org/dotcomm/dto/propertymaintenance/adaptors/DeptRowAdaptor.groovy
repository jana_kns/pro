package org.dotcomm.dto.propertymaintenance.adaptors

import org.dotcomm.util.Adaptor
import org.dotcomm.dto.propertymaintenance.Departments
import org.dotcomm.pagecode.rows.DeptRow
import org.dotcomm.pagecode.rows.LocationRow
import org.dotcomm.dto.propertymaintenance.Location
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.dto.propertymaintenance.Users

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class DeptRowAdaptor implements Adaptor {

    /** Injected Property */
    LocationRowAdaptor locationRowAdaptor

    /** Injected Property */
    UserRowAdaptor userRowAdaptor

    /**
     * Adapt persistant dto for view.
     * @return object for view
     */
    public Object adapt(Object obj) {
        DeptRow deptRow = new DeptRow()
        Departments departments = (Departments) obj
        if (departments != null) {
            deptRow.deptId = departments.deptId
            deptRow.lastUpd = departments.lastUpdatedDateTime
            deptRow.deptName = departments.deptName
            deptRow.deptPhone = departments.deptPhone
            deptRow.deptDesc = departments.deptDescr
            deptRow.deptStatus = departments.enabled.intValue() == 1 ? 'Active' : 'Inactive'
            deptRow.deptActive = departments.enabled.intValue() == 1 ? new Boolean(true) : new Boolean(false)
            //deptRow.listLocations = departments.location.collect { Location l ->
            //    locationRowAdaptor.adapt(l)
            //}
            deptRow.listLocations = new ArrayList<LocationRow>()
            for (Location l : departments.location) {
                deptRow.listLocations.add(locationRowAdaptor.adapt(l))
            }
            deptRow.listUsers = new ArrayList<UserRow>()
            for (Users u : departments.users) {
                deptRow.listUsers.add(userRowAdaptor.adapt(u))
            }
        }
        else {
            return null
        }
        return deptRow
    }

    public Class<Departments> getFromType() {
        return Departments
    }

    public Class<DeptRow> getToType() {
        return DeptRow
    }

    /**
     * Merge object for view to persistant dto.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        Departments departments = new Departments()
        DeptRow deptRow = (DeptRow) obj
        if (deptRow != null) {
            departments.deptId = deptRow.deptId
            departments.lastUpdatedDateTime = deptRow.lastUpd
            departments.deptName = deptRow.deptName
            departments.deptPhone = deptRow.deptPhone
            departments.deptDescr = deptRow.deptDesc
            departments.enabled = deptRow.deptActive ? new Integer(1) : new Integer(0)
            departments.location = new ArrayList<Location>()
            for (LocationRow l : deptRow.listLocations) {
                departments.location.add(locationRowAdaptor.merge(l))
            }
            departments.users = new ArrayList<Users>()
            for (UserRow u : deptRow.listUsers) {
                departments.users.add(userRowAdaptor.merge(u))
            }
        }
        else {
            return null
        }
        return departments
    }
}