package org.dotcomm.test.dao.impl;

import org.dotcomm.dao.WorkOrderDao;
import org.dotcomm.dto.propertymaintenance.*;
import org.dotcomm.pagecode.reports.ReportSearchCriteria;
import org.dotcomm.test.util.SpringContextUtil;
import org.junit.Test;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

public class WorkOrderDaoImplTest extends AbstractTransactionalDataSourceSpringContextTests {


    WorkOrderDao workOrderDao;
    Departments departments;
    Location location;
    Users superUser;
    Users mtnceUser;
    WoType woType;
    WorkOrder workOrder;
    ServiceGroup serviceGroup;
    ServiceItem serviceItem;
    MtnceGroup mtnceGroup;

    /**
     * Spring will automatically inject object on startup
     * @param workOrderDao The injected dao.
     */
    public void setWorkOrderDao(WorkOrderDao workOrderDao) {
        this.workOrderDao = workOrderDao;
    }

    @Override
    protected String[] getConfigLocations() {
        return new String[] { SpringContextUtil
                .getApplicationContextDirectory()
                + "applicationContext.xml" };
    }

    @Override
    protected void onSetUp() throws Exception {
        //department
        departments = new Departments();
        departments.setDeptId(99999999);
        departments.setEnabled(1);
        departments.setDeptName("junit test");
        //location
        location = new Location();
        location.setLocationId(99999999);
        location.setName("junit test");
        location.setEnabled(1);
        location.setDeptId(departments);
        //Admin User
        Roles admin = new Roles();
        admin.setRoleId(4); //Already be loaded by seed data.
        superUser = new Users();
        superUser.setUserId(99999999);
        superUser.setDeptId(departments);
        superUser.setRoleId(admin);
        superUser.setEnabled(1);
        superUser.setUsername("junit");
        superUser.setPassword("password");
        //Maintenance User
        Roles mtnce = new Roles();
        mtnce.setRoleId(3); //Already be loaded by seed data.
        mtnceUser = new Users();
        mtnceUser.setUserId(99999998);
        mtnceUser.setDeptId(departments);
        mtnceUser.setRoleId(mtnce);
        mtnceUser.setEnabled(1);
        mtnceUser.setUsername("junit2");
        mtnceUser.setPassword("password");
        //Work Order type
        woType = new WoType();
        woType.setWoTypeId(9999999);
        woType.setName("junit testing");
        woType.setEnabled(1);
        //Non-Archived work order
        Archive archived = new Archive();
        archived.setArchiveId(1);
        WoStatus completed = new WoStatus();
        completed.setStatusId(3); //Already be loaded by seed data.
        workOrder = new WorkOrder();
        workOrder.setWorkOrderId(999999999);
        workOrder.setWoTypeId(woType);
        workOrder.setLocationId(location);
        workOrder.setStatusId(completed);
        workOrder.setRequestedCompDate(new Date());
        workOrder.setRequestComments("junit-asfjasdfjsa");
        workOrder.setUserId(superUser);
        workOrder.setArchiveId(archived);
        workOrder.setLastUpdatedBy("junit tst");
        workOrder.setServiceItemMany(new ArrayList());
        //Service Group
        serviceGroup = new ServiceGroup();
        serviceGroup.setServiceGroupId(999999999);
        serviceGroup.setName("junit");
        serviceGroup.setEnabled(1);
        //Service Item
        serviceItem = new ServiceItem();
        serviceItem.setServiceItemId(999999999);
        serviceItem.setName("junit item");
        serviceItem.setEnabled(1);
        serviceItem.setServiceGroupId(serviceGroup);
        //Maintenance Group
        mtnceGroup = new MtnceGroup();
        mtnceGroup.setMtnceGroupId(999999999);
        mtnceGroup.setName("junit test");
        mtnceGroup.setEnabled(1);
        super.onSetUp();
    }

    @Override
    protected void onSetUpInTransaction() throws Exception {
        loadDatabase();
        super.onSetUpInTransaction();
    }

    /**
     * Loads database records for testing
     */
    private void loadDatabase() {
        String query = "Insert into mtn001.departments (dept_id,dept_name,enabled) "
                        + "values (" + departments.getDeptId()
                        + ",'" + departments.getDeptName()
                        + "'," + departments.getEnabled() + ")";
        jdbcTemplate.execute(query);
        query = "Insert into mtn001.location (location_id,name,enabled,dept_id) "
                        + "values (" + location.getLocationId()
                        + ",'" + location.getName()
                        + "'," + location.getEnabled()
                        + "," + location.getDeptId().getDeptId() + ")";
        jdbcTemplate.execute(query);
        query = "Insert into mtn001.users (user_id,username,enabled,dept_id,password,role_id) "
                        + "values (" + superUser.getUserId()
                        + ",'" + superUser.getUsername()
                        + "'," + superUser.getEnabled()
                        + "," + superUser.getDeptId().getDeptId()
                        + ",'" + superUser.getPassword()
                        + "'," + superUser.getRoleId().getRoleId() + ")";
        jdbcTemplate.execute(query);
        query = "Insert into mtn001.users (user_id,username,enabled,dept_id,password,role_id) "
                        + "values (" + mtnceUser.getUserId()
                        + ",'" + mtnceUser.getUsername()
                        + "'," + mtnceUser.getEnabled()
                        + "," + mtnceUser.getDeptId().getDeptId()
                        + ",'" + mtnceUser.getPassword()
                        + "'," + mtnceUser.getRoleId().getRoleId() + ")";
        jdbcTemplate.execute(query);
        query = "Insert into mtn001.wo_type (wo_type_id,name,enabled) "
                        + "values (" + woType.getWoTypeId()
                        + ",'" + woType.getName()
                        + "'," + woType.getEnabled() + ")";
        jdbcTemplate.execute(query);
        query = "Insert into mtn001.work_order (work_order_id,last_updated_by,wo_type_id,location_id,status_id,requested_comp_date,last_updated_date_time,request_comments,archive_id,user_id) "
                        + "values (" + workOrder.getWorkOrderId()
                        + ",'" + workOrder.getLastUpdatedBy()
                        + "'," + workOrder.getWoTypeId().getWoTypeId()
                        + "," + workOrder.getLocationId().getLocationId()
                        + "," + workOrder.getStatusId().getStatusId()
                        + "," + "current date,'1977-01-06 12:00:00.123'"
                        + ",'" + workOrder.getRequestComments()
                        + "'," + workOrder.getArchiveId().getArchiveId()
                        + "," + workOrder.getUserId().getUserId() + ")";
        jdbcTemplate.execute(query);
        query = "Insert into mtn001.service_group (service_group_id,name,enabled,last_updated_date_time) "
                        + "values (" + serviceGroup.getServiceGroupId()
                        + ",'" + serviceGroup.getName()
                        + "'," + serviceGroup.getEnabled() + ",current timestamp)";
        jdbcTemplate.execute(query);
        query = "Insert into mtn001.service_item (service_item_id,name,enabled,service_group_id) "
                        + "values (" + serviceItem.getServiceItemId()
                        + ",'" + serviceItem.getName()
                        + "'," + serviceItem.getEnabled()
                        + "," + serviceItem.getServiceGroupId().getServiceGroupId() + ")";
        jdbcTemplate.execute(query);
        query = "Insert into mtn001.wo_service (service_item_id,work_order_id) "
                        + "values (" + serviceItem.getServiceItemId()
                        + "," + workOrder.getWorkOrderId() + ")";
        jdbcTemplate.execute(query);
        query = "Insert into mtn001.mtnce_group (mtnce_group_id,name,enabled,last_updated_date_time) "
                        + "values (" + mtnceGroup.getMtnceGroupId()
                        + ",'" + mtnceGroup.getName()
                        + "'," + mtnceGroup.getEnabled() + ",current timestamp)";
        jdbcTemplate.execute(query);
        query = "Insert into mtn001.mtnce_group_type (mtnce_group_id,wo_type_id) "
                        + "values (" + mtnceGroup.getMtnceGroupId()
                        + "," + woType.getWoTypeId() + ")";
        jdbcTemplate.execute(query);
        query = "Insert into mtn001.user_mtnce_group (mtnce_group_id,user_id) "
                        + "values (" + mtnceGroup.getMtnceGroupId()
                        + "," + mtnceUser.getUserId() + ")";
        jdbcTemplate.execute(query);
    }

    @Test
    public void testFilterWorkOrdersForSuper() {
        List<WorkOrder> list = workOrderDao.filterWorkOrdersForSuper(workOrder, null, null, 0, 10);
        assertEquals(1, list.size());
    }

    @Test
    public void testCountWorkOrdersForSuper() {
        Integer num = workOrderDao.countWorkOrdersForSuper(workOrder, null, null);
        assertEquals(1, num.intValue());
    }

    @Test
    public void testFilterWorkOrdersForMtnce() {
        //Note: The mtnce group user filter stores the user id in the mtnce group id.
        //user belongs to mtnce group
        workOrder.setMtnceGroup(mtnceUser.getUserId());
        List<WorkOrder> list = workOrderDao.filterWorkOrdersForMtnce(workOrder, null, null, 0, 10);
        assertEquals(1, list.size());
        //user does not belong to mtnce group
        workOrder.setMtnceGroup(superUser.getUserId());
        list = workOrderDao.filterWorkOrdersForMtnce(workOrder, null, null, 0, 10);
        assertEquals(0, list.size());
    }

    @Test
    public void testCountWorkOrdersForMtnce() {
        //Note: The mtnce group user filter stores the user id in the mtnce group id.
        //user belongs to mtnce group
        workOrder.setMtnceGroup(mtnceUser.getUserId());
        Integer num = workOrderDao.countWorkOrdersForMtnce(workOrder, null, null);
        assertEquals(1, num.intValue());
        //user does not belong to mtnce group
        workOrder.setMtnceGroup(superUser.getUserId());
        num = workOrderDao.countWorkOrdersForMtnce(workOrder, null, null);
        assertEquals(0, num.intValue());
    }

    @Test
    public void testFilterWorkOrdersForDeptHead() {
        //test user can see work order they submitted.
        workOrder.setMtnceGroup(mtnceGroup.getMtnceGroupId());
        List<WorkOrder> list = workOrderDao.filterWorkOrdersForDeptHead(workOrder, null, null, 0, 10);
        assertEquals(1, list.size());
        //test user can see the work order, they did not submit but in their dept.
        Users user = new Users();
        user.setUserId(999999991);
        workOrder.setUserId(user);
        list = workOrderDao.filterWorkOrdersForDeptHead(workOrder, null, null, 0, 10);
        assertEquals(1, list.size());
        //test dept head does not see work orders outside their dept.
        Departments departments = new Departments();
        departments.setDeptId(999999991);
        workOrder.getLocationId().setDeptId(departments);
        list = workOrderDao.filterWorkOrdersForDeptHead(workOrder, null, null, 0, 10);
        assertEquals(0, list.size());
    }

    @Test
    public void testCountWorkOrdersForDeptHead() {
        //test user can see work order they submitted.
        workOrder.setMtnceGroup(mtnceGroup.getMtnceGroupId());
        Integer num = workOrderDao.countWorkOrdersForDeptHead(workOrder, null, null);
        assertEquals(1, num.intValue());
        //test user can see the work order, they did not submit but in their dept.
        Users user = new Users();
        user.setUserId(999999991);
        workOrder.setUserId(user);
        num = workOrderDao.countWorkOrdersForDeptHead(workOrder, null, null);
        assertEquals(1, num.intValue());
        //test dept head does not see work orders outside their dept.
        Departments departments = new Departments();
        departments.setDeptId(999999991);
        workOrder.getLocationId().setDeptId(departments);
        num = workOrderDao.countWorkOrdersForDeptHead(workOrder, null, null);
        assertEquals(0, num.intValue());
    }

    @Test
    public void testFilterWorkOrdersForLocHead() {
        //test user can see work order they submitted.
        List<WorkOrder> list = workOrderDao.filterWorkOrdersForLocHead(workOrder, 0, 10);
        assertEquals("Location head should see work order they submitted.",1, list.size());
        //test user can see the work order, they did not submit, but in their location.
        Users user = new Users();
        user.setUserId(999999991);
        workOrder.setUserId(user);
        list = workOrderDao.filterWorkOrdersForLocHead(workOrder, 0, 10);
        assertEquals("Location head should see work order submitted by another user for same location.",1, list.size());
        Location loc = new Location();
        loc.setLocationId(1234567890);
        workOrder.setLocationId(loc);
        list = workOrderDao.filterWorkOrdersForLocHead(workOrder, 0, 10);
        assertEquals("Location head should not see work order in another location.",0, list.size());
    }

    @Test
    public void testCountWorkOrdersForLocHead() {
        //test user can see work order they submitted.
        Integer num = workOrderDao.countWorkOrdersForLocHead(workOrder);
        assertEquals("Location head should see work order they submitted.",1, num.intValue());
        //test user can see the work order, they did not submit, but in their location.
        Users user = new Users();
        user.setUserId(999999991);
        workOrder.setUserId(user);
        num = workOrderDao.countWorkOrdersForLocHead(workOrder);
        assertEquals("Location head should see work order submitted by another user for same location.",1, num.intValue());
        Location loc = new Location();
        loc.setLocationId(1234567890);
        workOrder.setLocationId(loc);
        num = workOrderDao.countWorkOrdersForLocHead(workOrder);
        assertEquals("Location head should not see work order in another location.",0, num.intValue());
    }

    @Test
    public void testFilterWorkOrdersForUser() {
        //test user can see work order they submitted.
        List<WorkOrder> list = workOrderDao.filterWorkOrdersForUser(workOrder, 0, 10);
        assertEquals(1, list.size());
        //test user does not see the work order, they did not submit.
        Users user = new Users();
        user.setUserId(999999991);
        workOrder.setUserId(user);
        list = workOrderDao.filterWorkOrdersForUser(workOrder, 0, 10);
        assertEquals(0, list.size());
    }

    @Test
    public void testCountWorkOrdersForUser() {
        //test user can see work order they submitted.
        Integer num = workOrderDao.countWorkOrdersForUser(workOrder);
        assertEquals(1, num.intValue());
        //test user does not see the work order, they did not submit.
        Users user = new Users();
        user.setUserId(999999991);
        workOrder.setUserId(user);
        num = workOrderDao.countWorkOrdersForUser(workOrder);
        assertEquals(0, num.intValue());
    }

    @Test
    public void testGetWoTypesCounts() {
        List<Object[]> list = workOrderDao.getWoTypesCounts(new ReportSearchCriteria());
        boolean foundWorkOrder = false;
        for (Object[] o : list) {
            if ((o[1]).equals(workOrder.getWoTypeId().getName())) {
                assertEquals("1", o[0].toString());
                foundWorkOrder = true;
            }
        }
        assertTrue (foundWorkOrder);
    }

    @Test
    public void testGetServiceGroupCounts() {
        List<Object[]> list = workOrderDao.getServiceGroupCounts(new ReportSearchCriteria());
        boolean foundWorkOrder = false;
        for (Object[] o : list) {
            if ((o[1]).equals(serviceGroup.getName())) {
                assertEquals("1", o[0].toString());
                foundWorkOrder = true;
            }
        }
        assertTrue (foundWorkOrder);
    }

    @Test
    public void testGetDeptCounts() {
        List<Object[]> list = workOrderDao.getDeptCounts(new ReportSearchCriteria());
        boolean foundWorkOrder = false;
        for (Object[] o : list) {
            if ((o[1]).equals(workOrder.getLocationId().getDeptId().getDeptName())) {
                assertEquals("1", o[0].toString());
                foundWorkOrder = true;
            }
        }
        assertTrue (foundWorkOrder);
    }

    @Test
    public void testGetLastThirtyDayCreatedWorkOrders() {
        List<Object[]> list = workOrderDao.getLastThirtyDayCreatedWorkOrders();
        assertNotNull (list);
        assertTrue (list.size() > 0);
    }

    @Test
    public void testGetWoStatusCounts() {
        List<Object[]> list = workOrderDao.getWoStatusCounts(new ReportSearchCriteria());
        assertNotNull (list);
        assertTrue (list.size() > 0);
    }

    @Test
    public void testUpdateArchiveIndNoneFound() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.YEAR, -100);
        int recsArchived = workOrderDao.updateArchiveInd(now.getTime());
        assertEquals(0, recsArchived);
        now.add(Calendar.YEAR, 80);
        recsArchived = workOrderDao.updateArchiveInd(now.getTime());
        assertEquals(0, recsArchived);
    }

    @Test
    public void testCompletedWo() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.YEAR, -100);
        List<WorkOrder> list = workOrderDao.completedWo(now.getTime());
        assertEquals(0, list.size());
        now.add(Calendar.YEAR, 80);
        list = workOrderDao.completedWo(now.getTime());
        assertEquals(1, list.size());
    }

    /*public void testCache()
    {
        //final String cacheRegion = Message.class.getCanonicalName();
        //SecondLevelCacheStatistics settingsStatistics = sessionFactory.getStatistics().getSecondLevelCacheStatistics(cacheRegion);
        StopWatch stopWatch = new StopWatch();
        for (int i = 0; i < 10; i++)
        {
            stopWatch.start();
            //workOrderDao.getAllObjects(Roles.class);
            stopWatch.stop();
            System.out.println("Query time : " + stopWatch.getTotalTimeMillis());
            System.out.println("Cache hit count:" + sessionFactory.getStatistics().getSecondLevelCacheHitCount());
            System.out.println("Cache miss count:" + sessionFactory.getStatistics().getSecondLevelCacheMissCount());
            System.out.println("Cache put count:" + sessionFactory.getStatistics().getSecondLevelCachePutCount());
            //assertEquals(0, settingsStatistics.getMissCount());
            //assertEquals(numberOfMessages * i, settingsStatistics.getHitCount());
            //stopWatch.reset();
            //System.out.println(settingsStatistics);
            endTransaction();

            // spring creates a transaction when test starts - so we first end it then start a new in the loop
            startNewTransaction();
        }
    }*/
}

