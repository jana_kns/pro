package org.dotcomm.test.services.impl

import org.junit.Test
import org.dotcomm.dto.propertymaintenance.MsgPost
import org.dotcomm.pagecode.rows.DeptRow
import org.dotcomm.services.impl.AdminWorkOrderServiceImpl
import org.dotcomm.persistence.DataAccessObject
import org.dotcomm.dto.propertymaintenance.adaptors.MsgPostRowAdaptor
import org.dotcomm.pagecode.rows.MsgPostRow
import org.dotcomm.dto.propertymaintenance.ServiceGroup
import org.dotcomm.dto.propertymaintenance.adaptors.ServiceGroupRowAdaptor
import org.dotcomm.pagecode.rows.ServiceGroupRow
import org.dotcomm.dto.propertymaintenance.ServiceItem
import org.dotcomm.pagecode.rows.ServiceGroupItemRow
import org.dotcomm.dto.propertymaintenance.adaptors.ServiceGroupItemRowAdaptor
import org.dotcomm.pagecode.rows.WoTypeRow
import org.dotcomm.dto.propertymaintenance.WoType
import org.dotcomm.dto.propertymaintenance.adaptors.WorkOrderTypeRowAdaptor
import org.dotcomm.util.SecurityUtils
import org.dotcomm.dto.propertymaintenance.Users

/**
 * Junit test cases for the AdminWorkOrderServiceImpl
 * @author jdolinski
 */
class AdminWorkOrderServiceImplTest extends GroovyTestCase {

    AdminWorkOrderServiceImpl adminWorkOrderService
    MsgPost mockMsgPost
    ServiceGroup mockServiceGroup
    ServiceItem mockServiceItem
    WoType mockWoType

    protected void setUp() {
        adminWorkOrderService = new AdminWorkOrderServiceImpl()
        adminWorkOrderService.msgPostRowAdaptor = new MsgPostRowAdaptor()
        adminWorkOrderService.serviceGroupRowAdaptor = new ServiceGroupRowAdaptor()
        adminWorkOrderService.serviceGroupItemRowAdaptor = new ServiceGroupItemRowAdaptor()
        adminWorkOrderService.workOrderTypeRowAdaptor = new WorkOrderTypeRowAdaptor()
        mockMsgPost = new MsgPost(msgPostId:1,msg:'ta;lfjsdfl;j',msgTitle:'hashusdfha',lastUpdatedDateTime:new Date())
        mockServiceGroup = new ServiceGroup(serviceGroupId:1,name:'lalalala',createDateTime:new Date(),enabled:1,lastUpdatedDateTime:new Date(),descr:'as;ldjfo')
        mockServiceItem = new ServiceItem(serviceItemId:3,name:'asdf',createDateTime:new Date(),lastUpdatedDateTime:new Date(),serviceGroupId:mockServiceGroup,enabled:1)
        mockWoType = new WoType(woTypeId:1,name:'asdfasf',descr:'poyih',enabled:1)
    }

    @Test
    void testGetAllMsgPostItems() {
        List data = [mockMsgPost]
        def dao = [
                getAllObjects:{
                    data}] as DataAccessObject
        adminWorkOrderService.dao = dao
        List<DeptRow> list = adminWorkOrderService.getAllMsgPostItems()
        list.eachWithIndex {MsgPostRow row, int count ->
            MsgPost msgPost = (MsgPost)data[count]
            assert row.msgId == msgPost.msgPostId
            assert row.msg == msgPost.msg
            assert row.msgTitle == msgPost.msgTitle
            assert row.msgDate == msgPost.lastUpdatedDateTime
        }
        assert list.size() == 1
    }

    @Test
    void testGetMsgPostItem() {
        MsgPost data = mockMsgPost
        def dao = [
                getObjectById:{o1,o2 ->
                    data}] as DataAccessObject
        adminWorkOrderService.dao = dao
        MsgPostRow value = adminWorkOrderService.getMsgPostItem(mockMsgPost.msgPostId)
        assert data.msgPostId == value.msgId
        assert data.msg == value.msg
        assert data.msgTitle == value.msgTitle
        assert data.lastUpdatedDateTime == value.msgDate
    }

    @Test
    void testAddNewMsgPost() {
        def dao = [
                save:{},
                getObjectById:{o1, o2 ->
                    new Users(fname:'junit',lname:'lname')
                }] as DataAccessObject
        def securityUtils = [
                getUserIdLoggedIn:{
                    1
                }] as SecurityUtils
        MsgPostRow msgPostRow = new MsgPostRow(msgId:1,msg:'junit',msgTitle:'tiasdf',msgDate:new Date())
        adminWorkOrderService.dao = dao
        adminWorkOrderService.securityUtils = securityUtils
        adminWorkOrderService.addNewMsgPost(msgPostRow)
        assert true
    }

    @Test
    void testUpdateMsgPost() {
        def dao = [
                merge:{},
                getObjectById:{o1, o2 ->
                    new Users(fname:'junit',lname:'lname')
                }] as DataAccessObject
        def securityUtils = [
                getUserIdLoggedIn:{
                    1
                }] as SecurityUtils
        adminWorkOrderService.dao = dao
        adminWorkOrderService.securityUtils = securityUtils
        MsgPostRow msgPostRow = new MsgPostRow(msgId:1,msg:'junit',msgTitle:'title',msgDate:new Date())
        adminWorkOrderService.updateMsgPost(msgPostRow)
        assert true
    }

    @Test
    void testDeleteMsgPost() {
        def dao = [
                delete:{},
                getObjectById:{o1, o2 ->
                    mockMsgPost}] as DataAccessObject
        adminWorkOrderService.dao = dao
        adminWorkOrderService.deleteMsgPost(mockMsgPost.msgPostId)
        assert true
    }

    @Test
    void testGetAllServiceGroupItems() {
        List data = [mockServiceGroup]
        List data2 = [mockServiceItem]
        def dao = [
                getAllObjects:{
                    data},
                getObjectsByExample:{o1 ->
                    data2}] as DataAccessObject
        adminWorkOrderService.dao = dao
        List<DeptRow> list = adminWorkOrderService.getAllServiceGroupItems()
        list.eachWithIndex {ServiceGroupRow row, int count ->
            ServiceGroup serviceGroup = (ServiceGroup)data[count]
            assert row.servGroupId == serviceGroup.serviceGroupId
            assert row.servGroup == serviceGroup.name
            assert row.servGroupDesc == serviceGroup.descr
            assert row.servGroupStatus == 'Active'
            assert row.active == true
            assert row.lastUpd == serviceGroup.lastUpdatedDateTime
            row.listItems.eachWithIndex {ServiceGroupItemRow row2, int count2 ->
                ServiceItem serviceItem = (ServiceItem)data2[count2]
                assert row2.itemId == serviceItem.serviceItemId
                assert row2.groupId == serviceItem.serviceGroupId.serviceGroupId
                assert row2.active == true
                assert row2.lastUpd == serviceItem.lastUpdatedDateTime
                assert row2.status == 'Active'
                }
            assert row.listItems.size() == 1
        }
        assert list.size() == 1
    }

    @Test
    void testGetAllActiveServiceGroupItems() {
        List data = [mockServiceGroup]
        def dao = [
                getObjectsByExample:{
                    data}] as DataAccessObject
        adminWorkOrderService.dao = dao
        List<DeptRow> list = adminWorkOrderService.getAllActiveServiceGroupItems()
        list.eachWithIndex {ServiceGroupRow row, int count ->
            ServiceGroup serviceGroup = (ServiceGroup)data[count]
            assert row.servGroupId == serviceGroup.serviceGroupId
            assert row.servGroup == serviceGroup.name
            assert row.servGroupDesc == serviceGroup.descr
            assert row.servGroupStatus == 'Active'
            assert row.active == true
            assert row.lastUpd == serviceGroup.lastUpdatedDateTime
        }
        assert list.size() == 1
    }

    @Test
    void testGetAllActiveServiceItems() {
        List data = [mockServiceItem]
        def dao = [
                getObjectsByExample:{
                    data}] as DataAccessObject
        adminWorkOrderService.dao = dao
        List<DeptRow> list = adminWorkOrderService.getAllActiveServiceItems()
        list.eachWithIndex {ServiceGroupItemRow row, int count ->
            ServiceItem serviceItem = (ServiceItem)data[count]
            assert row.itemId == serviceItem.serviceItemId
            assert row.groupId == serviceItem.serviceGroupId.serviceGroupId
            assert row.name == serviceItem.name
            assert row.status == 'Active'
            assert row.active == true
            assert row.lastUpd == serviceItem.lastUpdatedDateTime
        }
        assert list.size() == 1
        mockServiceItem.serviceGroupId.enabled = 0
        list = adminWorkOrderService.getAllActiveServiceItems()
        assert list.size() == 0
    }

    @Test
    void testGetAllServiceItems() {
        List data = [mockServiceItem]
        def dao = [
                getAllObjects:{
                    data}] as DataAccessObject
        adminWorkOrderService.dao = dao
        List<DeptRow> list = adminWorkOrderService.getAllServiceItems()
        list.eachWithIndex {ServiceGroupItemRow row, int count ->
            ServiceItem serviceItem = (ServiceItem)data[count]
            assert row.itemId == serviceItem.serviceItemId
            assert row.groupId == serviceItem.serviceGroupId.serviceGroupId
            assert row.name == serviceItem.name
            assert row.status == 'Active'
            assert row.active == true
            assert row.lastUpd == serviceItem.lastUpdatedDateTime
        }
        assert list.size() == 1
    }

    @Test
    void testGetServiceGroup() {
        ServiceGroup data = mockServiceGroup
        def dao = [
                getObjectById:{o1,o2 ->
                    data}] as DataAccessObject
        adminWorkOrderService.dao = dao
        ServiceGroupRow value = adminWorkOrderService.getServiceGroup(mockServiceGroup.serviceGroupId)
        assert data.serviceGroupId == value.servGroupId
        assert data.name == value.servGroup
        assert data.descr == value.servGroupDesc
        assert data.lastUpdatedDateTime == value.lastUpd
        assert value.active == true
        assert value.servGroupStatus == 'Active'
    }

    @Test
    void testAddNewServiceGroup() {
        def dao = [
                save:{}] as DataAccessObject
        adminWorkOrderService.dao = dao
        ServiceGroupRow serviceGroupRow = new ServiceGroupRow(servGroup:'asfd',servGroupDesc:'lajsf',active:true)
        adminWorkOrderService.addNewServiceGroup(serviceGroupRow)
        assert true
    }

    @Test
    void testUpdateServiceGroup() {
        def dao = [
                merge:{}] as DataAccessObject
        adminWorkOrderService.dao = dao
        ServiceGroupRow serviceGroupRow = new ServiceGroupRow(servGroupId:1,servGroup:'asfd',servGroupDesc:'lajsf',active:true)
        adminWorkOrderService.updateServiceGroup(serviceGroupRow)
        assert true
    }

    @Test
    void testGetAllWorkOrderTypeItems() {
        List data = [mockWoType]
        def dao = [
                getAllObjects:{
                    data}] as DataAccessObject
        adminWorkOrderService.dao = dao
        List<WoTypeRow> list = adminWorkOrderService.getAllWorkOrderTypeItems()
        list.eachWithIndex {WoTypeRow row, int count ->
            WoType woType = (WoType)data[count]
            assert row.woTypeId == woType.woTypeId
            assert row.woTypeDesc == woType.descr
            assert row.woTypeName == woType.name
            assert row.woTypeStatus == 'Active'
            assert row.woActive == true
            assert row.woLastUpd == woType.lastUpdatedDateTime
        }
        assert list.size() == 1
    }

    @Test
    void testGetAllActiveWorkOrderTypeItems() {
        List data = [mockWoType]
        def dao = [
                getObjectsByExample:{
                    data}] as DataAccessObject
        adminWorkOrderService.dao = dao
        List<WoTypeRow> list = adminWorkOrderService.getAllActiveWorkOrderTypeItems()
        list.eachWithIndex {WoTypeRow row, int count ->
            WoType woType = (WoType)data[count]
            assert row.woTypeId == woType.woTypeId
            assert row.woTypeDesc == woType.descr
            assert row.woTypeName == woType.name
            assert row.woTypeStatus == 'Active'
            assert row.woActive == true
            assert row.woLastUpd == woType.lastUpdatedDateTime
        }
        assert list.size() == 1
    }

    @Test
    void testGetWorkOrderTypeItem() {
        WoType data = mockWoType
        def dao = [
                getObjectById:{o1,o2 ->
                    data}] as DataAccessObject
        adminWorkOrderService.dao = dao
        WoTypeRow value = adminWorkOrderService.getWorkOrderTypeItem(mockWoType.woTypeId)
        assert value.woTypeId == data.woTypeId
        assert value.woTypeDesc == data.descr
        assert value.woTypeName == data.name
        assert value.woTypeStatus == 'Active'
        assert value.woActive == true
        assert value.woLastUpd == data.lastUpdatedDateTime
    }

    @Test
    void testAddNewWoType() {
        def dao = [
                save:{}] as DataAccessObject
        adminWorkOrderService.dao = dao
        WoTypeRow woTypeRow = new WoTypeRow(woTypeName:'asfdasf',woTypeDesc:'iishih',woActive:true)
        adminWorkOrderService.addNewWoType(woTypeRow)
        assert true
    }

    @Test
    void testUpdateWoType() {
        def dao = [
                merge:{}] as DataAccessObject
        adminWorkOrderService.dao = dao
        WoTypeRow woTypeRow = new WoTypeRow(woTypeId:1,woTypeName:'asfdasf',woTypeDesc:'iishih',woActive:true)
        adminWorkOrderService.updateWoType(woTypeRow)
        assert true
    }

}