The property files are loaded by spring from the specified directory in the application context. These files must exist 
and be located for the application to successfully work. Drop them into the specified directory on the machine
the application is running on.