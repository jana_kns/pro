package org.dotcomm.dto.propertymaintenance;

/**
 * DTO for the Materials table.
 *
 * @author jdolinski
 */
class Materials {

	Integer materialId
	WorkOrder workOrderId
	String partMaterial
	Integer quantity
	String poNumber

}
