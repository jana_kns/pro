package org.dotcomm.pagecode.workorder

import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.services.AdminWorkOrderService
import javax.faces.model.SelectItem
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.adaptors.SelectLocationRowAdaptor
import org.dotcomm.pagecode.adaptors.SelectWoTypeAdaptor
import org.dotcomm.services.WorkOrderService
import org.dotcomm.util.FacesUtils

/**
 * Jsf managed bean for the addwo.xhtml.
 *
 * @author jdolinski
 */
class AddWo {

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Injected Property */
    WorkOrderService workOrderService

    /** Page code data */
    WorkOrderRow workOrderRow

    /** Page code data */
    List<SelectItem> listLocations

    /** Page code data */
    List<SelectItem> listWoTypes

    /** Constructor */
    AddWo(AdminWorkOrderService adminWorkOrderService, AdminSecurityService adminSecurityService) {
        this.adminWorkOrderService = adminWorkOrderService
        this.adminSecurityService = adminSecurityService
        workOrderRow = new WorkOrderRow()
        //default the requested completion date to 7 days ahead.
        //Calendar c1 = Calendar.getInstance()
        //c1.add(Calendar.DATE,7)
        workOrderRow.woReqCompDate = new Date()
        SelectLocationRowAdaptor selectLocationRowAdaptor = new SelectLocationRowAdaptor()
        SelectWoTypeAdaptor selectWoTypeAdaptor = new SelectWoTypeAdaptor()
        listLocations = selectLocationRowAdaptor.adapt(adminSecurityService.getAllActiveLocations())
        listWoTypes = selectWoTypeAdaptor.adapt(adminWorkOrderService.getAllActiveWorkOrderTypeItems())
    }

    /**
     * Action method to add a new work order to the system.
     * @return Faces navigation text.
     */
    public String newWorkOrder() {
            Integer id = workOrderService.addNewWorkOrder (workOrderRow)
            FacesUtils.putPageFlowScopeParameter ("woid", id)
            return "viewwo";
        }

}
