package org.dotcomm.pagecode.admin;

import org.dotcomm.pagecode.rows.MsgPostRow
import org.dotcomm.pagecode.rows.WoTypeRow
import org.dotcomm.pagecode.rows.ServiceGroupRow
import org.dotcomm.pagecode.rows.DeptRow
import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.rows.LocationRow
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.pagecode.rows.MtnceGroupRow
import org.dotcomm.util.FacesUtils
import org.apache.myfaces.trinidad.component.core.output.CoreOutputText

/**
 * Jsf managed bean for the adminmain.xhtml.
 *
 * @author jdolinski
 */
class AdminMain {

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** List of announcements/messages displayed on the page */
    List<MsgPostRow> listMessages

    /** List of work order types displayed on the page */
    List<WoTypeRow> listWoTypes

    /** List of work order types displayed on the page */
    List<ServiceGroupRow> listServiceGroups

    /** List of departments displayed on the page */
    List<DeptRow> listDepts

    /** List of locations displayed on the page */
    List<LocationRow> listLocs

    /** List of users displayed on the page */
    List<UserRow> listUsers

    /** List of maintenance groups displayed on the page */
    List<MtnceGroupRow> listMtnceGroups

    /** Constructor */
    AdminMain(AdminWorkOrderService adminWorkOrderService, AdminSecurityService adminSecurityService) {
        this.adminWorkOrderService = adminWorkOrderService
        this.adminSecurityService = adminSecurityService
    }

    /**
     *  Populate page with admin records.
     */
    void doSearch() {
        listMessages = this.adminWorkOrderService.getAllMsgPostItems()
        listWoTypes = this.adminWorkOrderService.getAllWorkOrderTypeItems()
        listServiceGroups = this.adminWorkOrderService.getAllServiceGroupItems()
        listDepts = this.adminSecurityService.getAllDepartments()
        listLocs = this.adminSecurityService.getAllLocations()
        listUsers = this.adminSecurityService.getAllUsers()
        listMtnceGroups = this.adminSecurityService.getAllMtnceGroups()
        FacesUtils.putPageFlowScopeParameter 'adminbean', this
    }

    /**
     * Action method to delete an existing message in the system.
     * @return Faces navigation text.
     */
    String deleteMsg() {
        Integer msgId = (Integer)FacesUtils.getPageFlowScopeParameter("msgid")
        adminWorkOrderService.deleteMsgPost(msgId)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

    /**
     * Action method to navigate to add a new service group.
     * @return Faces navigation text.
     */
    String addServGroup() {
        FacesUtils.removePageFlowScopeParameter("ServFormListItems")
        return "addservgroup"
    }

    /**
     * Action method to navigate to add a new service group.
     * @return Faces navigation text.
     */
    String editServGroup() {
        FacesUtils.removePageFlowScopeParameter("ServFormListItems")
        return "editservgroup"
    }

    /** Controls the toggle for the message toggle panel. */
    void openMsgPanel() {
        if ((boolean)FacesUtils.getPageFlowScopeParameter("openmsgpanel")) {
            FacesUtils.putPageFlowScopeParameter ("openmsgpanel", Boolean.FALSE)
        }
        else {
            FacesUtils.putPageFlowScopeParameter ("openmsgpanel", Boolean.TRUE)            
        }
    }

    /** Controls the toggle for the wo type toggle panel. */
    void openWoTypePanel() {
        if ((boolean)FacesUtils.getPageFlowScopeParameter("openwotypepanel")) {
            FacesUtils.putPageFlowScopeParameter ("openwotypepanel", Boolean.FALSE)
        }
        else {
            FacesUtils.putPageFlowScopeParameter ("openwotypepanel", Boolean.TRUE)
        }
    }

    /** Controls the toggle for the department toggle panel. */
    void openDeptPanel() {
        if ((boolean)FacesUtils.getPageFlowScopeParameter("opendeptpanel")) {
            FacesUtils.putPageFlowScopeParameter ("opendeptpanel", Boolean.FALSE)
        }
        else {
            FacesUtils.putPageFlowScopeParameter ("opendeptpanel", Boolean.TRUE)
        }
    }

    /** Controls the toggle for the user toggle panel. */
    void openUserPanel() {
        if ((boolean)FacesUtils.getPageFlowScopeParameter("openuserpanel")) {
            FacesUtils.putPageFlowScopeParameter ("openuserpanel", Boolean.FALSE)
        }
        else {
            FacesUtils.putPageFlowScopeParameter ("openuserpanel", Boolean.TRUE)
        }
    }

    /** Controls the toggle for the location toggle panel. */
    void openLocPanel() {
        if ((boolean)FacesUtils.getPageFlowScopeParameter("openlocpanel")) {
            FacesUtils.putPageFlowScopeParameter ("openlocpanel", Boolean.FALSE)
        }
        else {
            FacesUtils.putPageFlowScopeParameter ("openlocpanel", Boolean.TRUE)
        }
    }

    /** Controls the toggle for the location toggle panel. */
    void openSerPanel() {
        if ((boolean)FacesUtils.getPageFlowScopeParameter("openserpanel")) {
            FacesUtils.putPageFlowScopeParameter ("openserpanel", Boolean.FALSE)
        }
        else {
            FacesUtils.putPageFlowScopeParameter ("openserpanel", Boolean.TRUE)
        }
    }

    /** Controls the toggle for the maintenance group toggle panel. */
    void openMtnPanel() {
        if ((boolean)FacesUtils.getPageFlowScopeParameter("openmtnpanel")) {
            FacesUtils.putPageFlowScopeParameter ("openmtnpanel", Boolean.FALSE)
        }
        else {
            FacesUtils.putPageFlowScopeParameter ("openmtnpanel", Boolean.TRUE)
        }
    }

}
