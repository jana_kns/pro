package org.dotcomm.pagecode.admin.maintenancegroup

import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.rows.MtnceGroupRow
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.admin.AdminMain;

/**
 * Jsf managed bean for the editmtnce.xhtml.
 *
 * @author jdolinski
 */
class EditMtnce {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Page code data */
    MtnceGroupRow mtnceGroupRow

    /** Constructor */
    EditMtnce(AdminSecurityService adminSecurityService) {
        this.adminSecurityService = adminSecurityService
        Integer mtnceGroupId = (Integer)FacesUtils.getPageFlowScopeParameter("mtncegroupid")
        mtnceGroupRow = this.adminSecurityService.getMtnceGroup(mtnceGroupId)
    }

    /**
     * Action method to edit an existing maintenance group in the system.
     * @return Faces navigation text.
     */
    String updateMtnceGroup() {
        adminSecurityService.updateMtnceGroup(mtnceGroupRow)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

}
