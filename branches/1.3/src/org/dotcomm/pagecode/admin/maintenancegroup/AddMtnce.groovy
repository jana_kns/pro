package org.dotcomm.pagecode.admin.maintenancegroup

import org.dotcomm.pagecode.rows.MtnceGroupRow
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.admin.AdminMain
import org.dotcomm.util.FacesUtils;

/**
 * Jsf managed bean for the addmtnce.xhtml.
 *
 * @author jdolinski
 */
class AddMtnce {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Page code data */
    MtnceGroupRow mtnceGroupRow

    /** Constructor */
    AddMtnce() {
        mtnceGroupRow = new MtnceGroupRow()
        mtnceGroupRow.active = new Boolean(true)
    }

    /**
     * Action method to add a new maintenance group to the system.
     * @return Faces navigation text.
     */
    String newMtnceGroup() {
        adminSecurityService.addNewMtnceGroup (mtnceGroupRow)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

}
