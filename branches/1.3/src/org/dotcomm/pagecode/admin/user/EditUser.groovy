package org.dotcomm.pagecode.admin.user

import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.admin.AdminMain

/**
 * Jsf managed bean for the edituser.xhtml.
 *
 * @author jdolinski
 */
class EditUser {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Page code data */
    UserRow userRow

    /** Constructor */
    EditUser(AdminSecurityService adminSecurityService) {
        this.adminSecurityService = adminSecurityService
        Integer userId = (Integer)FacesUtils.getPageFlowScopeParameter("userid")
        userRow = this.adminSecurityService.getUser(userId)
    }

    /**
     * Action method to edit an existing user in the system.
     * @return Faces navigation text.
     */
    String updateUser() {
        adminSecurityService.updateUser(userRow)
        String navText = (String)FacesUtils.getPageFlowScopeParameter("editusernav")
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return navText
    }

    /**
     * Action method for the cancel button.
     * @return Faces navigation text.
     */
    String cancel() {
        String navText = (String)FacesUtils.getPageFlowScopeParameter("editusernav")
        return navText
    }

}
