package org.dotcomm.pagecode.admin.location

import org.dotcomm.pagecode.rows.LocationRow
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.admin.AdminMain

/**
 * Jsf managed bean for the editloc.xhtml.
 *
 * @author jdolinski
 */
class EditLoc {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Page code data */
    LocationRow locationRow

    /** Constructor */
    EditLoc(AdminSecurityService adminSecurityService) {
        this.adminSecurityService = adminSecurityService
        Integer locId = (Integer)FacesUtils.getPageFlowScopeParameter("locid")
        locationRow = this.adminSecurityService.getLocation(locId)
    }

    /**
     * Action method to edit an existing location in the system.
     * @return Faces navigation text.
     */
    String updateLoc() {
        adminSecurityService.updateLocation(locationRow)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

}