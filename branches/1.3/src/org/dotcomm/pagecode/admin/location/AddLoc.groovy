package org.dotcomm.pagecode.admin.location

import org.dotcomm.pagecode.rows.LocationRow
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.admin.AdminMain
import org.dotcomm.util.FacesUtils

/**
 * Jsf managed bean for the addloc.xhtml.
 *
 * @author jdolinski
 */
class AddLoc {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Page code data */
    LocationRow locationRow

    /** Constructor */
    AddLoc() {
        locationRow = new LocationRow()
        locationRow.locActive = new Boolean(true)
    }

    /**
     * Action method to add a new location to the system.
     * @return Faces navigation text.
     */
    String newLocation() {
        adminSecurityService.addNewLocation (locationRow)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

}