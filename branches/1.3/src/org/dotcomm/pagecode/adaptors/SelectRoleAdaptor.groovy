package org.dotcomm.pagecode.adaptors;

import org.dotcomm.pagecode.rows.RoleRow;

import javax.faces.model.SelectItem;
import java.util.List;
import java.util.ArrayList
import org.dotcomm.util.Adaptor;

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
public class SelectRoleAdaptor implements Adaptor {

    /**
     * Adapt a list of view rows to selection items.
     * @return list of select items
     */
    public Object adapt(Object obj) {
        List<SelectItem> listSelectItems = new ArrayList<SelectItem>()
        List<RoleRow> listRoleRows = (ArrayList<RoleRow>) obj
        for (RoleRow r : listRoleRows) {
            listSelectItems.add(new SelectItem(r.roleId, r.roleName, r.roleDesc))
        }
        return listSelectItems.sort {SelectItem it -> it.label}
    }

    public Class<ArrayList<RoleRow>> getFromType() {
        return ArrayList
    }

    public Class<ArrayList<SelectItem>> getToType() {
        return ArrayList
    }

    /**
     * Merge object from list of select items to list of view rows.
     * @return list view row
     */
    public Object merge(Object obj) {
        List<RoleRow> listDeptRows = new ArrayList<RoleRow>()
        List<SelectItem> listSelectItems = (ArrayList<SelectItem>) obj
        for (SelectItem i : listSelectItems) {
            listDeptRows.add(new RoleRow(deptId:i.getValue(),deptName:i.getLabel(),deptDesc:i.getDescription()))
        }
        return listDeptRows
    }

}
