package org.dotcomm.pagecode.converters

import javax.faces.convert.Converter
import javax.faces.context.FacesContext
import javax.faces.component.UIComponent

/**
 * This jsf converter is needed to use value pass thru on a component that has a list of
 * selectable objects that return an integer id representing the object. Without the converter
 * and using value pass thru the application will throw an exception trying to pass in a
 * String value for the id.
 * @author JDolinski
 */
class IntegerConverter implements Converter {

    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String submittedValue) {
        if (submittedValue != null) {
            if (submittedValue.length() > 0) {
                return new Integer(submittedValue)
            }
        }
        return null
    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (o != null) {
            return o.toString()
        }
        return null
    }


}