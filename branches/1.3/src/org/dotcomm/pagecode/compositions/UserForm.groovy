package org.dotcomm.pagecode.compositions

import javax.faces.model.SelectItem
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.adaptors.SelectDeptAdaptor
import org.dotcomm.pagecode.adaptors.SelectRoleAdaptor
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.adaptors.SelectMtnceGroupRowAdaptor
import javax.faces.application.FacesMessage
import javax.faces.component.UIComponent
import javax.faces.context.FacesContext
import javax.faces.validator.ValidatorException
import org.apache.myfaces.trinidad.component.UIXInput;

/**
 * Facelet compostion backing bean for the userform.xhtml.
 *
 * @author JDolinski
 */
class UserForm {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Page code data */
    List<SelectItem> pcListDepts
    List<SelectItem> pcListRoles
    List<SelectItem> pcListMtnceGroups

    /** Constructor */
    UserForm(AdminSecurityService adminSecurityService) {
        this.adminSecurityService = adminSecurityService
        SelectDeptAdaptor selectDeptAdaptor = new SelectDeptAdaptor()
        SelectRoleAdaptor selectRoleAdaptor = new SelectRoleAdaptor()
        SelectMtnceGroupRowAdaptor selectMtnceGroupRowAdaptor = new SelectMtnceGroupRowAdaptor()
        pcListDepts = selectDeptAdaptor.adapt(adminSecurityService.getAllActiveDepartments())
        pcListRoles = selectRoleAdaptor.adapt(adminSecurityService.getAllRoles())
        pcListMtnceGroups = selectMtnceGroupRowAdaptor.adapt(adminSecurityService.getAllActiveMtnceGroups())
    }

    /**
     * Validator to see if the username is already used in the system. Username's are unique.
     * @param context The current faces context.
     * @param component The component the validator is registered on.
     * @param value The form value submitted.
     */
    void userNameValidator(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        /** retrieve the string value of the field */
        String userName = (String)value

        /**
         * If we are editing a user, this variable will be populated with user's database id we are working with.
         *  This is the component id on the edituser.xhtml page.
         */
        UIXInput editPageUserId = (UIXInput)FacesUtils.findComponentInRoot("userid")
        Integer userId = (Integer)editPageUserId?.value

        /** Get the error msg from the message bundle */
        String errorMsg = FacesUtils.getStringFromBundle('usernameused')

        /** Look up the username in the database */
        UserRow userRow = adminSecurityService.getUser(userName)

        if (userRow != null) {  //check if user name is in the system
            if (userId != userRow.userId) { //check to see if the username belongs to another user
                FacesMessage msg = new FacesMessage()
                msg.setDetail(errorMsg)
                msg.setSummary(errorMsg)
                msg.setSeverity(FacesMessage.SEVERITY_ERROR)
                throw new ValidatorException(msg)
            }
        }
    }

}
