package org.dotcomm.pagecode.compositions

import org.dotcomm.services.AdminSecurityService
import javax.faces.model.SelectItem
import org.dotcomm.pagecode.adaptors.SelectDeptAdaptor

/**
 * Facelet compostion backing bean for the locform.xhtml.
 *
 * @author JDolinski
 */
class LocForm {

    List<SelectItem> pcListDepts

    /** Constructor */
    LocForm(AdminSecurityService adminSecurityService) {
        SelectDeptAdaptor selectDeptAdaptor = new SelectDeptAdaptor()
        pcListDepts = selectDeptAdaptor.adapt(adminSecurityService.getAllActiveDepartments())
    }

}