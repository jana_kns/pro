package org.dotcomm.util

import org.springframework.security.userdetails.UserDetails
import org.springframework.security.context.SecurityContextHolder

/**
 * The implementation for {@link SecurityUtils}.
 *
 * @author JDolinski
 *
 */
class SecurityUtilsImpl implements SecurityUtils {

    String getUserNameLoggedIn() {
        Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal()
        String principal = null
        if (obj instanceof UserDetails) {
            principal = ((UserDetails) obj).getUsername()
        } else {
            principal = obj.toString()
        }
        return principal
    }

    Integer getUserIdLoggedIn() {
        Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal()
        Integer userid
        if (obj instanceof UserDetails) {
            userid = ((CustomUser) obj).userId
        } else {
            userid = null
        }
        return userid
    }

}