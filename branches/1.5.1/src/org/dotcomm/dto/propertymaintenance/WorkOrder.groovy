package org.dotcomm.dto.propertymaintenance;

/**
 * DTO for the WorkOrder table.
 *
 * @author jdolinski
 */
class WorkOrder {

	Integer workOrderId
    Date createDateTime
    WoStatus statusId
	Location locationId
	WoType woTypeId
    Archive archiveId
	Date lastUpdatedDateTime
    Users userId
	Date requestedCompDate
	String requestComments
	Date startDateTime
	Date compDateTime
	String lastUpdatedBy
	String compBy
	String workInstructions
	String compComments
	Integer estLaborManHrs
	Integer numTrips
	Integer numPersons
	Double laborManHrs
	Double travelManHrs
	String chrgManHrsAcct
	String chgrPartsAcct
	Collection serviceItemMany
	Collection materials
    Integer mtnceGroup
    String sortColumn

}
