package org.dotcomm.dto.propertymaintenance.adaptors

import org.dotcomm.dto.propertymaintenance.ServiceGroup
import org.dotcomm.pagecode.rows.ServiceGroupRow
import org.dotcomm.util.Adaptor
import org.dotcomm.dto.propertymaintenance.ServiceItem
import org.dotcomm.pagecode.rows.ServiceGroupItemRow
import org.dotcomm.util.FacesUtils

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class ServiceGroupRowAdaptor implements Adaptor {

    /**
     * Adapt persistant dto for view.
     * @return object for view
     */
    public Object adapt(Object obj) {
        ServiceGroupRow serviceGroupRow = new ServiceGroupRow()
        ServiceGroup serviceGroup = (ServiceGroup) obj
        if (serviceGroup != null) {
            serviceGroupRow.servGroupId = serviceGroup.serviceGroupId
            serviceGroupRow.lastUpd = serviceGroup.lastUpdatedDateTime
            serviceGroupRow.servGroup = serviceGroup.name
            serviceGroupRow.servGroupDesc = serviceGroup.descr
            serviceGroupRow.servGroupStatus = serviceGroup.enabled.intValue() == 1 ? 'Active' : 'Inactive'
            serviceGroupRow.active = serviceGroup.enabled.intValue() == 1 ? new Boolean(true) : new Boolean(false)
            serviceGroupRow.listItems = new ArrayList<ServiceGroupItemRow>()
            ServiceGroupItemRowAdaptor itemRowAdaptor = new ServiceGroupItemRowAdaptor()
            for (ServiceItem s : serviceGroup.serviceItem) {
                serviceGroupRow.listItems.add(itemRowAdaptor.adapt(s))    
            }
        }
        else {
            return null
        }
        return serviceGroupRow
    }

    public Class<ServiceGroup> getFromType() {
        return ServiceGroup
    }

    public Class<ServiceGroupRow> getToType() {
        return ServiceGroupRow
    }

    /**
     * Merge object for view to persistant dto.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        ServiceGroup serviceGroup = new ServiceGroup()
        ServiceGroupRow serviceGroupRow = (ServiceGroupRow) obj
        if (serviceGroupRow != null) {
            serviceGroup.serviceGroupId = serviceGroupRow.servGroupId
            serviceGroup.lastUpdatedDateTime = serviceGroupRow.lastUpd
            serviceGroup.name = serviceGroupRow.servGroup
            serviceGroup.descr = serviceGroupRow.servGroupDesc
            serviceGroup.enabled = serviceGroupRow.active ? new Integer(1) : new Integer(0)
            serviceGroup.serviceItem = new ArrayList<ServiceItem>()
            ServiceGroupItemRowAdaptor itemRowAdaptor = new ServiceGroupItemRowAdaptor()
            for (ServiceGroupItemRow i : serviceGroupRow.listItems) {
                ServiceItem serviceItem = itemRowAdaptor.merge(i)
                serviceItem.serviceGroupId = serviceGroup
                serviceGroup.serviceItem.add(serviceItem)    
            }
        }
        else {
            return null
        }
        return serviceGroup
    }

}