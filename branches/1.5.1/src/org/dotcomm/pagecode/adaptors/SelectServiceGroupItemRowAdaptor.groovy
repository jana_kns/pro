package org.dotcomm.pagecode.adaptors

import org.dotcomm.util.Adaptor
import javax.faces.model.SelectItem
import org.dotcomm.pagecode.rows.ServiceGroupItemRow

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class SelectServiceGroupItemRowAdaptor implements Adaptor {

    /**
     * Adapt a list of view rows to selection items.
     * @return list of select items
     */
    public Object adapt(Object obj) {
        List<SelectItem> listSelectItems = new ArrayList<SelectItem>()
        List<ServiceGroupItemRow> listItemRows = (ArrayList<ServiceGroupItemRow>) obj
        for (ServiceGroupItemRow r : listItemRows) {
            listSelectItems.add(new SelectItem(r.itemId, r.groupName + " - " + r.name, r.groupDescr))
        }
        return listSelectItems.sort {SelectItem it -> it.label}
    }

    public Class<ArrayList<ServiceGroupItemRow>> getFromType() {
        return ArrayList
    }

    public Class<ArrayList<SelectItem>> getToType() {
        return ArrayList
    }

    /**
     * Merge object from list of select items to list of view rows.
     * @return list view row
     */
    public Object merge(Object obj) {
        List<ServiceGroupItemRow> listDeptRows = new ArrayList<ServiceGroupItemRow>()
        List<SelectItem> listSelectItems = (ArrayList<SelectItem>) obj
        for (SelectItem i : listSelectItems) {
            listDeptRows.add(new ServiceGroupItemRow(itemId:i.getValue(),name:i.getLabel()))
        }
        return listDeptRows
    }

}