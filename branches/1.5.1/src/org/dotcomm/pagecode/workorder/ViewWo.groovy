package org.dotcomm.pagecode.workorder

import org.dotcomm.services.WorkOrderService
import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.util.FacesUtils
import javax.faces.model.SelectItem
import org.dotcomm.pagecode.adaptors.SelectServiceGroupItemRowAdaptor
import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.util.JasperJsfReportUtil
import org.dotcomm.util.JasperJsfReportUtil.Format;

/**
 * Jsf managed bean for the viewwo.xhtml.
 *
 * @author jdolinski
 */
class ViewWo {

    /** Injected Property */
    WorkOrderService workOrderService

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** Page code data */
    WorkOrderRow workOrderRow

    /** Page code data */
    List<SelectItem> pcListServiceItems

    /** Constructor */
    ViewWo(WorkOrderService workOrderService, AdminWorkOrderService adminWorkOrderService) {
        this.workOrderService = workOrderService
        Integer woId = (Integer)FacesUtils.getPageFlowScopeParameter("woid")
        workOrderRow = this.workOrderService.getWorkOrder(woId)
        SelectServiceGroupItemRowAdaptor itemRowAdaptor = new SelectServiceGroupItemRowAdaptor()
        pcListServiceItems = itemRowAdaptor.adapt(adminWorkOrderService.getAllServiceItems())
    }

    /** Create pdf of the details for the Work Order */
    String printDetails() {
        List<WorkOrderRow> results = new ArrayList<WorkOrderRow>()
        results.add(this.workOrderRow)
        JasperJsfReportUtil jasperJsfReportUtil = new JasperJsfReportUtil()
        jasperJsfReportUtil.runReport(Format.pdf, "/WEB-INF/reports/workOrderDetails.jrxml", results)
        return null;
    }

}
