package org.dotcomm.pagecode.compositions

import javax.faces.model.SelectItem
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.adaptors.SelectDeptAdaptor
import org.dotcomm.pagecode.adaptors.SelectRoleAdaptor
import org.dotcomm.pagecode.adaptors.SelectMtnceGroupRowAdaptor
import javax.faces.event.ValueChangeEvent
import org.dotcomm.pagecode.adaptors.SelectLocationRowAdaptor
import javax.faces.context.FacesContext
import javax.faces.component.UIComponent
import javax.faces.validator.ValidatorException
import org.apache.myfaces.trinidad.component.UIXSelectOne
import org.dotcomm.util.FacesUtils
import javax.faces.application.FacesMessage
import org.dotcomm.pagecode.rows.UserRow

/**
 * Facelet compostion backing bean for the userform.xhtml.
 *
 * @author JDolinski
 */
class UserForm {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Page code data */
    List<SelectItem> pcListDepts
    List<SelectItem> pcListRoles
    List<SelectItem> pcListLocs
    List<SelectItem> pcListMtnceGroups
    UserRow userRow

    /** Constructor */
    UserForm(AdminSecurityService adminSecurityService) {
        this.adminSecurityService = adminSecurityService
        SelectDeptAdaptor selectDeptAdaptor = new SelectDeptAdaptor()
        SelectRoleAdaptor selectRoleAdaptor = new SelectRoleAdaptor()
        SelectMtnceGroupRowAdaptor selectMtnceGroupRowAdaptor = new SelectMtnceGroupRowAdaptor()
        pcListDepts = selectDeptAdaptor.adapt(adminSecurityService.getAllActiveDepartments())
        pcListRoles = selectRoleAdaptor.adapt(adminSecurityService.getAllRoles())
        pcListMtnceGroups = selectMtnceGroupRowAdaptor.adapt(adminSecurityService.getAllActiveMtnceGroups())
    }


    /**
     * Update the location list with locations from the select department.
     * @param event The change event.
     */
    void deptSelection(ValueChangeEvent event) {
      Integer deptId = (Integer)event.newValue
      SelectLocationRowAdaptor selectLocationRowAdaptor = new SelectLocationRowAdaptor()
      pcListLocs = selectLocationRowAdaptor.adapt(adminSecurityService.getAllActiveDeptLocations(deptId))
    }

}
