package org.dotcomm.pagecode.rows

/**
 * Chart row data for the view.
 *
 * @author JDolinski
 */
class ChartRow {

    List<String> seriesLabels
    ArrayList<List<Double>> yValues
    List<String> groupLabels
    Double yMax

}