package org.dotcomm.pagecode.reports.adaptors

import org.apache.myfaces.trinidad.model.ChartModel
import org.dotcomm.pagecode.rows.ChartRow
import org.dotcomm.util.Adaptor

/**
 * Adaptor pattern to convert presentation objects to Apache Trinidad specific objects.
 *
 * @author jdolinski
 */
class ChartModelAdaptor extends ChartModel implements Adaptor {

    List<String> groupLabels
    List<String> seriesLabels
    ArrayList<List<Double>> chartYValues
    String title 
    Double minXValue = 6.0
    Double maxXValue = 10.0
    Double minYValue = 0.0
    Double maxYValue
    ChartRow chartRow

    /** Constructor */
    public ChartModelAdaptor(String title) {
        this.title = title
    }

    public Object adapt(Object obj) {
        ChartRow chartRow = (ChartRow)obj
        this.chartRow = chartRow
        seriesLabels = chartRow.seriesLabels
        groupLabels = new ArrayList<String>()
        chartYValues = chartRow.yValues
        maxYValue = chartRow.yMax
        return this
    }

    public Class<ChartRow> getFromType() {
        return ChartRow
    }

    public Class<ChartModel> getToType() {
        return ChartModel
    }

    /**
     * Not implemented
     * @return obj
     */
    public Object merge(Object obj) {
        //do nothing
    }


    public List<List<Double>> getYValues() {
        return chartYValues;
    }

}


