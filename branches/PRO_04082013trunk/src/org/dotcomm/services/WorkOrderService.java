package org.dotcomm.services;

import org.dotcomm.pagecode.rows.WoStatusRow;
import org.dotcomm.pagecode.rows.WorkOrderRow;

import java.util.List;

/**
 * Defines the business services for work order
 * information in the application.
 *
 * @author JDolinski
 *
 */
public interface WorkOrderService {

    /**
     * Get all the active work order statuses in the system.
     * @return list List of statuses
     */
    List<WoStatusRow> getAllActiveWorkOrderStatuses();

    /**
     * Get all the work order statuses in the system.
     * @return list List of statuses
     */
    List<WoStatusRow> getAllWoStatusItems();

    /**
     * Get work orders in the system. The work order filter hides records based on user's role.
     * A user can only see work orders within their department. A maintenance man can only see
     * work orders that belong to his maintenance groups. A super user can see all work orders
     * including archived.
     * @param workOrderRow WorkOrderRow object
     * @param firstResult The database pagination page to display.
     * @param maxRows The number of results to return from query.
     * @return list List of work orders
     */
    List<WorkOrderRow> getFilteredWorkOrders(WorkOrderRow workOrderRow, Integer firstResult, Integer maxRows);

    /**
     * Get the count of work orders in the role/user can view in the system. The work order filter hides records based on user's role.
     * A user can only see work orders within their department. A maintenance man can only see
     * work orders that belong to his maintenance groups. A super user can see all work orders
     * including archived.
     * @param workOrderRow WorkOrderRow object
     * @return Integer Count of the number of records available.
     */
    List<WorkOrderRow> getFilteredWorkOrders(WorkOrderRow workOrderRow, Integer firstResult, Integer maxRows, Integer i);
    Integer getCountWorkOrders(WorkOrderRow workOrderRow);

    /**
     * Add a new work order.
     * @param workOrderRow object
     * @return Integer The id of the record.
     */
    Integer addNewWorkOrder(WorkOrderRow workOrderRow);

    /**
     * If the work order has a status of "unviewed" and the user requesting
     * is mtnce then update the work order to "pending"
     * @param id The work order to check. 
     */
    void updateStatusMtnceViewed(Integer id);

    /**
     * Get a specific work order in the system.
     * @param id of the record
     * @return workorder A single workorder object
     */
    WorkOrderRow getWorkOrder(Integer id);

    /**
     * Get a specific work order in the system.
     * This method is being used as there is a problem with the trinidad component.
     * If a work order has multiple service items, and one of them is inactivated, the
     * remaining "active" service items are not selected in the list box. This method
     * will remove the "inactive" service items so the component will correctly display
     * the active selected service items. This method can be removed it Apache fixes
     * this problem. Below is the warning received:
     * WARNING: Some entries in value of CoreSelectManyListbox[UIXEditableFacesBeanImpl, id=servdone] not found in SelectItems: [java.lang.Object@fc99d6, java.lang.Object@fc99d6, 7]
     * @param id of the record
     * @return workorder A single workorder object
     */
    WorkOrderRow getWorkOrderForEdit(Integer id);

    /**
     * Update an existing work order.
     * @param workOrderRow object
     */
    void updateWorkOrder(WorkOrderRow workOrderRow);

    /**
     * Archive the work orders when they are completed.
     */
    void updateArchiveIndicators();

    /**
     * Purge work orders from the system.
     */
    void purgeWorkOrders();
}
