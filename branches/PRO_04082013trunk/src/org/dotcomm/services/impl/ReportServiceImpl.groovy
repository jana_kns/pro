package org.dotcomm.services.impl;

import org.dotcomm.pagecode.rows.ChartRow;
import org.dotcomm.services.ReportService
import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.dto.propertymaintenance.WorkOrder
import org.dotcomm.dto.propertymaintenance.ServiceItem
import org.dotcomm.dto.propertymaintenance.adaptors.WorkOrderRowAdaptor
import org.dotcomm.persistence.DataAccessObject
import org.dotcomm.dao.WorkOrderDao
import org.dotcomm.pagecode.reports.ReportSearchCriteria
import org.dotcomm.pagecode.reports.adaptors.ReportWorkOrderRowAdaptor

/**
 * The implementation for {@link ReportService}.
 *
 * @author JDolinski
 */
class ReportServiceImpl implements ReportService {

    /** Injected Property  */
    WorkOrderRowAdaptor workOrderRowAdaptor

    /** Injected Property  */
    DataAccessObject dao

    /** Injected Property  */
    WorkOrderDao workOrderDao

    /** {@inheritDoc} */
    ChartRow getWorkOrderStatusesInSystem(ReportSearchCriteria criteria) {
        ChartRow chartRows = new ChartRow()
        List<Object[]> list = this.workOrderDao.getWoStatusCounts(criteria)
        List<String> s = new ArrayList<String>()
        List<Double> d = new ArrayList<Double>()
        for (Iterator it = list.iterator(); it.hasNext();) {
            Object[] o = (Object[]) it.next()
            d.add(((Integer) o[0]).doubleValue())
            s.add((String) o[1])
        }
        ArrayList<List<Double>> yvalues = new ArrayList<List<Double>>()
        yvalues.add(d)
        chartRows.seriesLabels = s
        chartRows.yValues = yvalues
        return chartRows
    }

    /** {@inheritDoc} */
    ChartRow getNumberOfWorkOrdersForEachDept(ReportSearchCriteria criteria) {
        ChartRow chartRows = new ChartRow()
        List<Object[]> list = this.workOrderDao.getDeptCounts(criteria)
        List<String> s = new ArrayList<String>()
        List<Double> d = new ArrayList<Double>()
        Integer highestWorkOrderCnt = new Integer(0)
        for (Iterator it = list.iterator(); it.hasNext();) {
            Object[] o = (Object[]) it.next()
            d.add(((Integer) o[0]).doubleValue())
            s.add((String) o[1])
            if (o[0] > highestWorkOrderCnt) {
                highestWorkOrderCnt = o[0] * 1.5
            }
        }
        ArrayList<List<Double>> yvalues = new ArrayList<List<Double>>()
        yvalues.add(d)
        chartRows.yMax = highestWorkOrderCnt.toDouble()
        chartRows.seriesLabels = s
        chartRows.yValues = yvalues
        return chartRows
    }

    /** {@inheritDoc} */
    ChartRow getNumberOfWorkOrdersServiceGroups(ReportSearchCriteria criteria) {
        ChartRow chartRows = new ChartRow()
        List<Object[]> list = this.workOrderDao.getServiceGroupCounts(criteria)
        List<String> s = new ArrayList<String>()
        List<Double> d = new ArrayList<Double>()
        for (Iterator it = list.iterator(); it.hasNext();) {
            Object[] o = (Object[]) it.next()
            d.add(((Integer) o[0]).doubleValue())
            s.add((String) o[1])
        }
        ArrayList<List<Double>> yvalues = new ArrayList<List<Double>>()
        yvalues.add(d)
        chartRows.seriesLabels = s
        chartRows.yValues = yvalues
        return chartRows
    }

    /** {@inheritDoc} */
    ChartRow getNumberOfWorkByType(ReportSearchCriteria criteria) {
        ChartRow chartRows = new ChartRow()
        List<Object[]> list = this.workOrderDao.getWoTypesCounts(criteria)
        List<String> s = new ArrayList<String>()
        List<Double> d = new ArrayList<Double>()
        for (Iterator it = list.iterator(); it.hasNext();) {
            Object[] o = (Object[]) it.next()
            d.add(((Integer) o[0]).doubleValue())
            s.add((String) o[1])
        }
        ArrayList<List<Double>> yvalues = new ArrayList<List<Double>>()
        yvalues.add(d)
        chartRows.seriesLabels = s
        chartRows.yValues = yvalues
        return chartRows
    }

    /** {@inheritDoc} */
    List<WorkOrderRow> getWorkOrderReportData(ReportSearchCriteria criteria) {
        List<WorkOrderRow> list = new ArrayList<WorkOrderRow>()
        ReportWorkOrderRowAdaptor adaptor = new ReportWorkOrderRowAdaptor()
        for (WorkOrder w : workOrderDao.woReportData(criteria)) {
            list.add(adaptor.adapt(w))
        }
        return list
    }

    /** {@inheritDoc} */
    List<WorkOrderRow> getServiceItemReportData(ReportSearchCriteria criteria) {
        List<WorkOrderRow> list = new ArrayList<WorkOrderRow>()
        ReportWorkOrderRowAdaptor adaptor = new ReportWorkOrderRowAdaptor()
        for (WorkOrder w : workOrderDao.servItemReportData(criteria)) {
            for (ServiceItem s: w.serviceItemMany) {
                WorkOrderRow row = adaptor.adapt(w)
                row.serviceItems = "$s.serviceGroupId.name - $s.name"
                list.add(row)
            }
        }
        return list
    }

}
