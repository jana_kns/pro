package org.dotcomm.jobs

import org.quartz.JobExecutionContext
import org.dotcomm.services.WorkOrderService
import org.springframework.scheduling.quartz.QuartzJobBean
import org.quartz.JobExecutionException

/**
 * The following job will purge work orders from the online system.
 *
 * @author JDolinski
 */
class PurgeJob extends QuartzJobBean {

    private WorkOrderService workOrderService

    protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
        workOrderService.purgeWorkOrders()
    }
}