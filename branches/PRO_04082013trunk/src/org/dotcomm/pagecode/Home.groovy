package org.dotcomm.pagecode

import org.dotcomm.pagecode.rows.MsgPostRow
import org.dotcomm.services.AdminWorkOrderService

/**
 * Jsf managed bean for the home.xhtml.
 *
 * @author jdolinski
 */
class Home {

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** List of announcements/messages displayed on the page */
    List<MsgPostRow> listMessages

    /** Constructor */
    Home(adminWorkOrderService) {
        this.adminWorkOrderService = adminWorkOrderService
        listMessages = this.adminWorkOrderService.getAllMsgPostItems()
    }

}
