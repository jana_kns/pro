package org.dotcomm.pagecode.reports

import org.apache.myfaces.trinidad.model.ChartModel
import org.dotcomm.pagecode.rows.ChartRow

/**
 * Chart model for presentation view.
 */
class ReportChartModel extends ChartModel {

    List<String> groupLabels
    List<String> seriesLabels
    ArrayList<List<Double>> chartYValues
    String title
    Double minXValue = 6.0
    Double maxXValue = 10.0
    Double minYValue = 0.0
    Double maxYValue
    ChartRow chartRow

    public List<List<Double>> getYValues() {
        return chartYValues;
    }

}


