package org.dotcomm.pagecode.reports.adaptors

import org.apache.myfaces.trinidad.model.ChartModel
import org.dotcomm.pagecode.rows.ChartRow
import org.dotcomm.util.Adaptor
import org.dotcomm.pagecode.reports.ReportChartModel

/**
 * Adaptor pattern to convert presentation objects to Apache Trinidad chart objects.
 *
 * @author jdolinski
 */
class ChartModelAdaptor implements Adaptor {

    public Class<ChartRow> getFromType() {
        return ChartRow
    }

    public Class<ChartModel> getToType() {
        return ChartModel
    }

    public Object adapt(Object obj) {
        ReportChartModel chartModel = new ReportChartModel()
        ChartRow chartRow = (ChartRow)obj
        if (chartRow != null) {
            chartModel.chartRow = chartRow
            chartModel.seriesLabels = chartRow.seriesLabels
            chartModel.groupLabels = new ArrayList<String>()
            chartModel.chartYValues = chartRow.yValues
            chartModel.maxYValue = chartRow.yMax
        } else {
            return null
        }
        return chartModel
    }

    /**
     * Not implemented
     * @return obj
     */
    public Object merge(Object obj) {
        throw new UnsupportedOperationException("Method is not implemented.")
    }

}


