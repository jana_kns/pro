package org.dotcomm.pagecode.rows

/**
 * Page code bean for displaying user information.
 *
 * @author jdolinski
 */
class UserRow {

    Integer userId
    Date lastUpd
    String fname
    String lname
    String uname
    String fullName
    String pwd
    String userStatus
    Boolean active
    String phone
    String email
    String dept
    Integer deptId
    String role
    Integer roleId
    String location
    Integer locationId
    List<MtnceGroupRow> listMtnceGroups
    
}