package org.dotcomm.dto.propertymaintenance

/**
 * DTO for the WoPurge table.
 *
 * @author jdolinski
 */
class WoPurge {

    Integer purgeId
    Date purgeDateTime
    Integer workOrderId
    Date createDateTime
    Date lastUpdatedDateTime
    String lastUpdatedBy
    String woType
    String status
    String location
    String requestorName
    Date requestedCompDate
    String requestComments
    String services
    String materials
    Date startDateTime
    Date compDateTime
    String compBy
    String workInstructions
    String compComments
    Integer estLaborManHrs
    Integer numTrips
    Integer numPersons
    Double laborManHrs
    Double travelManHrs
    String chrgManHrsAcct
    String chrgPartsAcct

}