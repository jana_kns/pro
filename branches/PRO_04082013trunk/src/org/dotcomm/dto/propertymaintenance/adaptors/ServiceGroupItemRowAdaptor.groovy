package org.dotcomm.dto.propertymaintenance.adaptors

import org.dotcomm.util.Adaptor
import org.dotcomm.pagecode.rows.ServiceGroupItemRow
import org.dotcomm.dto.propertymaintenance.ServiceItem
import org.dotcomm.util.FacesUtils

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class ServiceGroupItemRowAdaptor implements Adaptor {

    /**
     * Adapt persistant dto for view.
     * @return object for view
     */
    public Object adapt(Object obj) {
        ServiceGroupItemRow serviceGroupItemRow = new ServiceGroupItemRow()
        ServiceItem serviceItem = (ServiceItem) obj
        if (serviceItem != null) {
            serviceGroupItemRow.itemId = serviceItem.serviceItemId
            serviceGroupItemRow.lastUpd = serviceItem.lastUpdatedDateTime
            serviceGroupItemRow.name = serviceItem.name
            serviceGroupItemRow.groupId = serviceItem.serviceGroupId.serviceGroupId
            serviceGroupItemRow.groupName = serviceItem.serviceGroupId.name
            serviceGroupItemRow.groupDescr = serviceItem.serviceGroupId.descr
            serviceGroupItemRow.status = serviceItem.enabled == 1 ? 'Active' : 'Inactive'
            serviceGroupItemRow.active = serviceItem.enabled == 1 ? new Boolean(true) : new Boolean(false)
            serviceGroupItemRow.dbStored = new Boolean(true)
            serviceGroupItemRow.readOnly = false
        }
        else {
            return null
        }
        return serviceGroupItemRow
    }

    public Class<ServiceItem> getFromType() {
        return ServiceItem
    }

    public Class<ServiceGroupItemRow> getToType() {
        return ServiceGroupItemRow
    }

    /**
     * Merge object for view to persistant dto.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        ServiceItem serviceItem = new ServiceItem()
        ServiceGroupItemRow serviceGroupItemRow = (ServiceGroupItemRow) obj
        if (serviceGroupItemRow != null) {
            serviceItem.serviceItemId = serviceGroupItemRow.itemId
            serviceItem.createDateTime = new Date()
            serviceItem.lastUpdatedDateTime = serviceGroupItemRow.lastUpd
            serviceItem.name = serviceGroupItemRow.name
            serviceItem.enabled = serviceGroupItemRow.active ? new Integer(1) : new Integer(0)
        }
        else {
            return null
        }
        return serviceItem
    }

}