<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:aop="http://www.springframework.org/schema/aop"
	xmlns:tx="http://www.springframework.org/schema/tx"
	xmlns:util="http://www.springframework.org/schema/util"
	xsi:schemaLocation="
http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.0.xsd
http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-2.0.xsd
http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-2.0.xsd
http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-2.0.xsd">

    <bean id="placeholderConfig" class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
        <property name="locations">
            <list>
                <value>
                    file:///var/www/deploy/propertymaintenance/properties/database.properties
                </value>
                <value>
                    file:///var/www/deploy/propertymaintenance/properties/url.properties
                </value>
            </list>
        </property>
    </bean>

    <!-- Bean used to set properties to be used by JSF and delegating variable resolver -->
    <bean id="appProps" class="org.dotcomm.util.ApplicationProperties">
        <property name="test" value="${org.dotcomm.propertymaintenance.database.test}"/>
        <property name="helpUrl" value="${org.dotcomm.propertymaintenance.help.url}"/>
    </bean>

    <bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource" destroy-method="close">
        <property name="driverClass" value="${org.dotcomm.propertymaintenance.database.driver}"/>
        <property name="jdbcUrl" value="${org.dotcomm.propertymaintenance.database.url}"/>
        <property name="user" value="${org.dotcomm.propertymaintenance.database.user}"/>
        <property name="password" value="${org.dotcomm.propertymaintenance.database.password}"/>
        <property name="maxPoolSize" value="25"/>
        <property name="minPoolSize" value="0"/>
        <property name="acquireIncrement" value="1"/>
        <property name="maxIdleTime" value="1800"/>
        <property name="idleConnectionTestPeriod" value="28800"/>
        <property name="testConnectionOnCheckin" value="true"/>
        <property name="preferredTestQuery" value="select 1 from roles"/>
    </bean>

    <bean id="sessionFactory" class="org.springframework.orm.hibernate3.LocalSessionFactoryBean">
        <property name="dataSource" ref="dataSource" />
        <property name="hibernateProperties">
            <props>
                <prop key="hibernate.dialect">${org.dotcomm.propertymaintenance.database.dialect}</prop>
                <prop key="hibernate.show_sql">false</prop>
                <prop key="hibernate.cache.provider_class">org.hibernate.cache.EhCacheProvider</prop>
                <prop key="hibernate.cache.use_second_level_cache">true</prop>
                <prop key="hibernate.cache.use_query_cache">true</prop>
                <prop key="hibernate.generate_statistics">false</prop>
            </props>
        </property>
        <property name="mappingDirectoryLocations">
            <list>
                <value>classpath:/org/dotcomm/dto/propertymaintenance</value>
            </list>
        </property>
        <!--<property name="configurationClass">
            <value>org.hibernate.cfg.AnnotationConfiguration</value>
        </property>
        <property name="configLocation">
            <value>/WEB-INF/hibernate.cfg.xml</value>
        </property> -->
    </bean>

    <!-- Spring Data Access Exception Translator Defintion -->
    <bean id="jdbcExceptionTranslator" class="org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator">
        <property name="dataSource" ref="dataSource" />
    </bean>

    <!-- Hibernate Template Defintion -->
    <bean id="hibernateTemplate" class="org.springframework.orm.hibernate3.HibernateTemplate">
        <property name="sessionFactory" ref="sessionFactory" />
        <property name="jdbcExceptionTranslator" ref="jdbcExceptionTranslator" />
    </bean>

    <!-- Hibernate Transaction Manager Definition -->
    <bean id="transactionManager" class="org.springframework.orm.hibernate3.HibernateTransactionManager">
        <property name="sessionFactory" ref="sessionFactory" />
    </bean>

    <bean id="transactionCommitInterceptor" class="org.dotcomm.util.JsfHibernateOptimisticLockExceptionHandler" >
	    <property name="order" value="1" />
    </bean>

    <tx:advice id="txAdvice" transaction-manager="transactionManager">
        <tx:attributes>
            <tx:method name="get*" read-only="true" timeout="30" />
            <tx:method name="search*" read-only="true" timeout="30" />
            <tx:method name="*" timeout="30" />
        </tx:attributes>
    </tx:advice>

    <aop:config>
        <aop:pointcut id="allServiceMethodsPointCut" expression="execution(* org.dotcomm.services..*(..))" />
        <aop:advisor advice-ref="txAdvice" pointcut-ref="allServiceMethodsPointCut" order="2" />
        <aop:aspect id="staleData" ref="transactionCommitInterceptor">
              <aop:around method="commitTransaction" pointcut-ref="allServiceMethodsPointCut"/>
        </aop:aspect>
    </aop:config>

    <!-- Debug Logger Aspect
    <bean id="LoggingAspect"  class="org.dotcomm.util.MyLogger"/>
    <aop:config>
          <aop:aspect ref="LoggingAspect">
             <aop:pointcut id="myCutLogging"
                        expression="execution(* org.dotcomm..*(..))"/>
             <aop:around pointcut-ref="myCutLogging" method="log"/>
          </aop:aspect>
    </aop:config>       -->

    <!-- ========================= Start Data Access Object beans ======================= -->
    <bean id="dao" class="org.dotcomm.persistence.hibernate.HibernateDataAccessObjectImpl">
        <property name="hibernateTemplate" ref="hibernateTemplate" />
    </bean>

    <bean id="workOrderDao" class="org.dotcomm.dao.hibernate.HibernateWorkOrderDao" parent="dao"/>

    <!-- ========================= Start of Service beans =============================== -->
    <bean id="securityUtils" class="org.dotcomm.util.SecurityUtilsImpl" />

    <bean id="adminSecurityService" class="org.dotcomm.services.impl.AdminSecurityServiceImpl">
        <property name="dao" ref="dao" />
        <property name="securityUtils" ref="securityUtils" />
        <property name="deptRowAdaptor" ref="deptRowAdaptor" />
        <property name="roleRowAdaptor" ref="roleRowAdaptor" />
        <property name="userRowAdaptor" ref="userRowAdaptor" />
        <property name="locationRowAdaptor" ref="locationRowAdaptor" />
        <property name="mtnceGroupRowAdaptor" ref="mtnceGroupRowAdaptor" />
    </bean>

    <bean id="adminWorkOrderService" class="org.dotcomm.services.impl.AdminWorkOrderServiceImpl">
        <property name="dao" ref="dao" />
        <property name="securityUtils" ref="securityUtils" />
        <property name="msgPostRowAdaptor" ref="msgPostRowAdaptor"/>
        <property name="workOrderTypeRowAdaptor" ref="workOrderTypeRowAdaptor" />
        <property name="serviceGroupRowAdaptor" ref="serviceGroupRowAdaptor" />
        <property name="serviceGroupItemRowAdaptor" ref="serviceGroupItemRowAdaptor"/>
    </bean>

    <bean id="workOrderService" class="org.dotcomm.services.impl.WorkOrderServiceImpl">
        <property name="dao" ref="dao" />
        <property name="securityUtils" ref="securityUtils" />
        <property name="workOrderDao" ref="workOrderDao" />
        <property name="workOrderRowAdaptor" ref="workOrderRowAdaptor"/>
        <property name="woStatusRowAdaptor" ref="woStatusRowAdaptor"/>
        <property name="woPurgeAdaptor" ref="woPurgeAdaptor"/>
        <property name="serviceGroupItemRowAdaptor" ref="serviceGroupItemRowAdaptor"/>
    </bean>

    <bean id="reportService" class="org.dotcomm.services.impl.ReportServiceImpl">
        <property name="dao" ref="dao" />
        <property name="workOrderDao" ref="workOrderDao" />
        <property name="workOrderRowAdaptor" ref="workOrderRowAdaptor"/>
    </bean>

    <!-- ======================== Adaptors ============================================== -->
    <bean id="msgPostRowAdaptor" class="org.dotcomm.dto.propertymaintenance.adaptors.MsgPostRowAdaptor"/>

    <bean id="workOrderTypeRowAdaptor" class="org.dotcomm.dto.propertymaintenance.adaptors.WorkOrderTypeRowAdaptor" >
        <property name="dao" ref="dao"/>
    </bean>

    <bean id="deptRowAdaptor" class="org.dotcomm.dto.propertymaintenance.adaptors.DeptRowAdaptor">
        <property name="locationRowAdaptor" ref="locationRowAdaptor"/>
        <property name="userRowAdaptor" ref="userRowAdaptor"/>
    </bean>

    <bean id="userRowAdaptor" class="org.dotcomm.dto.propertymaintenance.adaptors.UserRowAdaptor">
        <property name="dao" ref="dao"/>
    </bean>

    <bean id="roleRowAdaptor" class="org.dotcomm.dto.propertymaintenance.adaptors.RoleRowAdaptor" />

    <bean id="locationRowAdaptor" class="org.dotcomm.dto.propertymaintenance.adaptors.LocationRowAdaptor" >
        <property name="dao" ref="dao"/>
    </bean>

    <bean id="serviceGroupRowAdaptor" class="org.dotcomm.dto.propertymaintenance.adaptors.ServiceGroupRowAdaptor" />

    <bean id="serviceGroupItemRowAdaptor" class="org.dotcomm.dto.propertymaintenance.adaptors.ServiceGroupItemRowAdaptor"/>

    <bean id="mtnceGroupRowAdaptor" class="org.dotcomm.dto.propertymaintenance.adaptors.MtnceGroupRowAdaptor">
        <property name="dao" ref="dao"/>
    </bean>

    <bean id="workOrderRowAdaptor" class="org.dotcomm.dto.propertymaintenance.adaptors.WorkOrderRowAdaptor">
        <property name="dao" ref="dao"/>
    </bean>

    <bean id="woStatusRowAdaptor" class="org.dotcomm.dto.propertymaintenance.adaptors.WoStatusRowAdaptor"/>

    <bean id="woPurgeAdaptor" class="org.dotcomm.dto.propertymaintenance.adaptors.WoPurgeAdaptor" />
    <!-- ======================== Start of JSF beans ==================================== -->

    <bean id="login" class="org.dotcomm.pagecode.Login" scope="request"/>

    <bean id="home" class="org.dotcomm.pagecode.Home" scope="request">
        <constructor-arg index="0" ref="adminWorkOrderService"/>
    </bean>

    <bean id="layout" class="org.dotcomm.pagecode.compositions.Layout" scope="request">
        <constructor-arg index="0" ref="adminSecurityService"/>
    </bean>

    <bean id="adminMain" class="org.dotcomm.pagecode.admin.AdminMain" scope="request">
        <constructor-arg index="0" ref="adminWorkOrderService"/>
        <constructor-arg index="1" ref="adminSecurityService"/>
    </bean>

    <bean id="addMsg" class="org.dotcomm.pagecode.admin.messages.AddMsg" scope="request">
        <property name="adminWorkOrderService" ref="adminWorkOrderService"/>
    </bean>

    <bean id="editMsg" class="org.dotcomm.pagecode.admin.messages.EditMsg" scope="request">
        <constructor-arg index="0" ref="adminWorkOrderService"/>
    </bean>

    <bean id="addWoType" class="org.dotcomm.pagecode.admin.workordertype.AddWoType" scope="request">
        <property name="adminWorkOrderService" ref="adminWorkOrderService"/>
    </bean>

    <bean id="editWoType" class="org.dotcomm.pagecode.admin.workordertype.EditWoType" scope="request">
        <constructor-arg index="0" ref="adminWorkOrderService"/>
    </bean>

    <bean id="addDept" class="org.dotcomm.pagecode.admin.department.AddDept" scope="request">
        <property name="adminSecurityService" ref="adminSecurityService"/>
    </bean>

    <bean id="editDept" class="org.dotcomm.pagecode.admin.department.EditDept" scope="request">
        <constructor-arg index="0" ref="adminSecurityService" />
    </bean>

    <bean id="addUser" class="org.dotcomm.pagecode.admin.user.AddUser" scope="request">
        <constructor-arg index="0" ref="adminSecurityService" />
    </bean>
    
    <bean id="editUser" class="org.dotcomm.pagecode.admin.user.EditUser" scope="request">
        <constructor-arg index="0" ref="adminSecurityService"/>
        <constructor-arg index="1" ref="securityUtils"/>
    </bean>

    <bean id="addLoc" class="org.dotcomm.pagecode.admin.location.AddLoc" scope="request">
        <constructor-arg index="0" ref="adminSecurityService"/>
    </bean>

    <bean id="editLoc" class="org.dotcomm.pagecode.admin.location.EditLoc" scope="request">
        <constructor-arg index="0" ref="adminSecurityService"/>    
    </bean>

    <bean id="addServ" class="org.dotcomm.pagecode.admin.service.AddServ" scope="request">
        <property name="adminWorkOrderService" ref="adminWorkOrderService"/>
    </bean>

    <bean id="editServ" class="org.dotcomm.pagecode.admin.service.EditServ" scope="request">
        <constructor-arg index="0" ref="adminWorkOrderService"/>    
    </bean>

    <bean id="addMtnce" class="org.dotcomm.pagecode.admin.maintenancegroup.AddMtnce" scope="request">
        <constructor-arg index="0" ref="adminSecurityService"/>
        <constructor-arg index="1" ref="adminWorkOrderService"/>
    </bean>

    <bean id="editMtnce" class="org.dotcomm.pagecode.admin.maintenancegroup.EditMtnce" scope="request">
        <constructor-arg index="0" ref="adminSecurityService"/>
        <constructor-arg index="1" ref="adminWorkOrderService"/>
    </bean>

    <bean id="addWo" class="org.dotcomm.pagecode.workorder.AddWo" scope="request">
        <constructor-arg index="0" ref="adminWorkOrderService"/>
        <constructor-arg index="1" ref="adminSecurityService"/>
        <property name="workOrderService" ref="workOrderService"/>
    </bean>

    <bean id="searchWo" class="org.dotcomm.pagecode.workorder.SearchWo" scope="session">
        <constructor-arg index="0" ref="adminSecurityService"/>
        <constructor-arg index="1" ref="adminWorkOrderService"/>
        <constructor-arg index="2" ref="workOrderService"/>
    </bean>

    <bean id="viewWo" class="org.dotcomm.pagecode.workorder.ViewWo" scope="request">
        <constructor-arg index="0" ref="workOrderService"/>
        <constructor-arg index="1" ref="adminWorkOrderService"/>
    </bean>

    <bean id="editWo" class="org.dotcomm.pagecode.workorder.EditWo" scope="request">
        <constructor-arg index="0" ref="workOrderService"/>
        <constructor-arg index="1" ref="adminSecurityService"/>
        <constructor-arg index="2" ref="adminWorkOrderService"/>
    </bean>

    <bean id="dashboard" class="org.dotcomm.pagecode.reports.Dashboard" scope="session">
        <constructor-arg index="0" ref="reportService"/>
        <constructor-arg index="1" ref="adminSecurityService"/>
        <constructor-arg index="2" ref="reportSearchCriteria"/>
    </bean>

    <bean id="userNameValidator" class="org.dotcomm.pagecode.validators.UserNameValidator">
        <property name="adminSecurityService" ref="adminSecurityService"/>
    </bean>

    <bean id="reportSearchCriteria" class="org.dotcomm.pagecode.reports.ReportSearchCriteria" scope="session">
        <property name="adminSecurityService" ref="adminSecurityService"/>
    </bean>
    <!-- ======================= Facelet Compositions ===================== -->
    <bean id="servForm" class="org.dotcomm.pagecode.compositions.ServForm" scope="request"/>

    <!-- ==================== Start of Scheduled Batch Jobs ============================== -->
    <!-- Archive Job -->
    <bean id="archiveJob" class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="workOrderService" />
        <property name="targetMethod" value="updateArchiveIndicators" />
        <property name="concurrent" value="false" />
    </bean>
    <!-- Simple trigger used for testing -->
    <bean id="archiveSimpleTrigger" class="org.springframework.scheduling.quartz.SimpleTriggerBean">
        <!-- see the example of method invoking job above -->
        <property name="jobDetail" ref="archiveJob" />
        <!-- 10 seconds -->
        <property name="startDelay" value="10000" />
        <!-- repeat every 50 seconds -->
        <property name="repeatInterval" value="10000" />
    </bean>
    <bean id="arhiveCronTrigger" class="org.springframework.scheduling.quartz.CronTriggerBean">
        <property name="jobDetail" ref="archiveJob" />
        <!-- run every morning at 6 AM -->
        <property name="cronExpression" value="0 0 6 * * ?" />
    </bean>
    <bean class="org.springframework.scheduling.quartz.SchedulerFactoryBean">
        <property name="triggers">
            <list>
                <ref bean="arhiveCronTrigger" />
                <!--<ref bean="archiveSimpleTrigger" />-->
            </list>
        </property>
    </bean>
    <!-- Purge Job -->
    <bean id="purgeJob" class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="workOrderService" />
        <property name="targetMethod" value="purgeWorkOrders" />
        <property name="concurrent" value="false" />
    </bean>
    <!-- Simple trigger used for testing -->
    <bean id="purgeSimpleTrigger" class="org.springframework.scheduling.quartz.SimpleTriggerBean">
        <!-- see the example of method invoking job above -->
        <property name="jobDetail" ref="purgeJob" />
        <!-- 10 seconds -->
        <property name="startDelay" value="10000" />
        <!-- repeat every 50 seconds -->
        <property name="repeatInterval" value="10000" />
    </bean>
    <bean id="purgeCronTrigger" class="org.springframework.scheduling.quartz.CronTriggerBean">
        <property name="jobDetail" ref="purgeJob" />
        <!-- run every morning at 5 AM -->
        <property name="cronExpression" value="0 0 5 * * ?" />
    </bean>
    <bean class="org.springframework.scheduling.quartz.SchedulerFactoryBean">
        <property name="triggers">
            <list>
                <ref bean="purgeCronTrigger" />
                <!--<ref bean="purgeSimpleTrigger" />-->
            </list>
        </property>
    </bean>
</beans>


