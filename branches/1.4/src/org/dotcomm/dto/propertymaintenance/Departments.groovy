package org.dotcomm.dto.propertymaintenance;

/**
 * DTO for the Departments table.
 *
 * @author jdolinski
 */
class Departments {

	Integer deptId
    Date createDateTime
    Collection users
	Date lastUpdatedDateTime
	String deptName
	String deptDescr
	String deptPhone
	Integer enabled
	Collection location

}
