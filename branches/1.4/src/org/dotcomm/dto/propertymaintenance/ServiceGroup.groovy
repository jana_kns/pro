package org.dotcomm.dto.propertymaintenance;

/**
 * DTO for the ServiceGroup table.
 *
 * @author jdolinski
 */
class ServiceGroup {

	Integer serviceGroupId
    Date createDateTime
	Date lastUpdatedDateTime
	String name
	String descr
	Integer enabled
	Collection serviceItem

}
