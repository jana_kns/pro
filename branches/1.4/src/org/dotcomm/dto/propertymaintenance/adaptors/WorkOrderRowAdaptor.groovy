package org.dotcomm.dto.propertymaintenance.adaptors

import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.dto.propertymaintenance.WorkOrder
import org.dotcomm.dto.propertymaintenance.WoStatus
import org.dotcomm.dto.propertymaintenance.Location
import org.dotcomm.dto.propertymaintenance.WoType
import java.text.Format
import java.text.SimpleDateFormat
import org.dotcomm.persistence.DataAccessObject
import org.dotcomm.pagecode.rows.MaterialRow
import org.dotcomm.dto.propertymaintenance.Materials
import org.dotcomm.dto.propertymaintenance.ServiceItem
import org.dotcomm.util.Adaptor
import org.dotcomm.dto.propertymaintenance.Departments
import org.dotcomm.dto.propertymaintenance.Archive
import org.dotcomm.dto.propertymaintenance.Users

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class WorkOrderRowAdaptor implements Adaptor {

    /** Injected Property */
    DataAccessObject dao

    /**
     * Adapt persistant dto for view.
     * @return object for view
     */
    public Object adapt(Object obj) {
        WorkOrderRow workOrderRow = new WorkOrderRow()
        WorkOrder workOrder = (WorkOrder) obj
        if (workOrder != null) {
            workOrderRow.woId = workOrder.workOrderId
            workOrderRow.createDateTime = workOrder.createDateTime
            workOrderRow.archived = workOrder.archiveId.archiveId.intValue() == 1 ? new Boolean(true) : new Boolean(false)
            workOrderRow.archivedString = workOrder.archiveId.archiveId.intValue() == 1 ? "Yes" : "No"
            Format fmt = new SimpleDateFormat("yyyyMMdd")
            String woNum = fmt.format(workOrder.createDateTime) + workOrder.workOrderId
            workOrderRow.woNumber = woNum.toLong()
            workOrderRow.woStatus = workOrder.statusId.name
            workOrderRow.woStatusId = workOrder.statusId.statusId
            workOrderRow.woLocation = workOrder.locationId.name
            workOrderRow.woLocationId = workOrder.locationId.locationId
            workOrderRow.woDepartment = workOrder.locationId.deptId.deptName
            workOrderRow.woDepartmentId = workOrder.locationId.deptId.deptId
            workOrderRow.woType = workOrder.woTypeId.name
            workOrderRow.woTypeId = workOrder.woTypeId.woTypeId
            workOrderRow.woRequestor = "$workOrder.userId.fname $workOrder.userId.lname"
            workOrderRow.woRequestorPhone = workOrder.userId?.phone
            workOrderRow.woRequestorEmail = workOrder.userId?.email
            workOrderRow.woRequestorUserId = workOrder.userId.userId
            workOrderRow.woReqComments = workOrder.requestComments
            Format formatter = new SimpleDateFormat("M/d/yyyy h:mm aaa")
            workOrderRow.woEnteredDate = workOrder.createDateTime
            workOrderRow.woLastUpdDate = workOrder.lastUpdatedDateTime
            String lastUpd = formatter.format(workOrder.lastUpdatedDateTime)
            workOrderRow.woLastUpdated = "$workOrder.lastUpdatedBy - $lastUpd"
            workOrderRow.woReqCompDate = workOrder.requestedCompDate
            workOrderRow.woStartDate = workOrder.startDateTime
            workOrderRow.woCompletionDate = workOrder.compDateTime
            workOrderRow.woInstructions = workOrder.workInstructions
            workOrderRow.woCompletionComments = workOrder.compComments
            workOrderRow.woChrgManHrsAcct = workOrder.chrgManHrsAcct
            workOrderRow.woChrgPartsAcct = workOrder.chgrPartsAcct
            workOrderRow.woNumberPersons = workOrder.numPersons
            workOrderRow.woNumberTrips = workOrder.numTrips
            workOrderRow.woManHrs = workOrder.laborManHrs
            workOrderRow.woTravelHrs = workOrder.travelManHrs
            workOrderRow.woTotalHrs = (workOrder.laborManHrs == null ? 0 : workOrder.laborManHrs) + (workOrder.travelManHrs == null ? 0 : workOrder.travelManHrs) 
            workOrderRow.woCompletedBy = workOrder.compBy
            workOrderRow.mtnceGroup = workOrder.mtnceGroup
            workOrderRow.listMaterials = new ArrayList<MaterialRow>()
            //Add all the materials.
            String materials = ''
            for (Materials m : workOrder.materials) {
                workOrderRow.listMaterials.add(new MaterialRow(matId:m.materialId, matPartName:m.partMaterial, matQuantity:m.quantity, matPoNumber:m.poNumber, woId:m.workOrderId.workOrderId, readOnly:true))
                if (materials == '') {
                    materials = "$m.quantity-$m.partMaterial"
                }
                else {
                    materials = "$materials, $m.quantity-$m.partMaterial"
                }
            }
            workOrderRow.materials = materials
            workOrderRow.listServiceItems = new ArrayList<Integer>()
            //Add all the service items.
            String serviceGroupItems = ''
            for (ServiceItem s : workOrder.serviceItemMany) {
                workOrderRow.listServiceItems.add(new Integer(s.serviceItemId))
                if (serviceGroupItems == '') {
                    serviceGroupItems = "$s.serviceGroupId.name-$s.name"
                }
                else {
                    serviceGroupItems = "$serviceGroupItems, $s.serviceGroupId.name-$s.name"
                }
            }
            workOrderRow.serviceItems = serviceGroupItems
        }
        else {
            return null
        }
        return workOrderRow
    }

    public Class<WorkOrder> getFromType() {
        return WorkOrder
    }

    public Class<WorkOrderRow> getToType() {
        return WorkOrderRow
    }

    /**
     * Merge object for view to persistant dto.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        WorkOrder workOrder = new WorkOrder()
        WorkOrderRow workOrderRow = (WorkOrderRow) obj
        if (workOrderRow != null) {
            workOrder.workOrderId = workOrderRow.woId
            workOrder.createDateTime = workOrderRow.createDateTime
            workOrder.lastUpdatedDateTime = workOrderRow.woLastUpdDate
            //workOrder.requestor = workOrderRow.woRequestor
            workOrder.requestedCompDate = workOrderRow.woReqCompDate
            workOrder.requestComments = workOrderRow.woReqComments
            workOrder.startDateTime = workOrderRow.woStartDate
            workOrder.compDateTime = workOrderRow.woCompletionDate
            workOrder.workInstructions = workOrderRow.woInstructions
            workOrder.compComments = workOrderRow.woCompletionComments
            workOrder.chrgManHrsAcct = workOrderRow.woChrgManHrsAcct
            workOrder.chgrPartsAcct = workOrderRow.woChrgPartsAcct
            workOrder.numPersons = workOrderRow.woNumberPersons
            workOrder.numTrips = workOrderRow.woNumberTrips
            workOrder.laborManHrs = workOrderRow.woManHrs
            workOrder.travelManHrs = workOrderRow.woTravelHrs
            workOrder.compBy = workOrderRow.woCompletedBy
            workOrder.mtnceGroup = workOrderRow.mtnceGroup
            workOrder.sortColumn = workOrderRow.sortOrder
            workOrder.archiveId = new Archive(archiveId:workOrderRow.archived ? new Integer(1) : new Integer(0))
            workOrder.statusId = new WoStatus(statusId:workOrderRow.woStatusId)
            workOrder.locationId = new Location(locationId:workOrderRow.woLocationId)
            workOrder.woTypeId = new WoType(woTypeId:workOrderRow.woTypeId)
            workOrder.locationId.deptId = new Departments(deptId:workOrderRow.woDepartmentId)
            workOrder.userId = new Users(userId:workOrderRow.woRequestorUserId)
            //Assign materials to the work order.
            workOrder.materials = new ArrayList<Materials>()
            for (MaterialRow view : workOrderRow.listMaterials) {
                workOrder.materials.add(new Materials(materialId:view.matId, workOrderId:workOrder, partMaterial:view.matPartName, quantity:view.matQuantity, poNumber:view.matPoNumber))
            }
            //Assign service items to the work order.
            workOrder.serviceItemMany = new ArrayList<ServiceItem>()
            for (Integer i : workOrderRow.listServiceItems) {
                //load() method always tries to return a proxy and only returns
                //an initialized instance if it is already managed in the current persistence context.
                //A get will always hit the database. Since we are just adding a item to a collection
                //we do not need to hit the db and get the value, the proxy will work just fine.
                workOrder.serviceItemMany.add(dao.load(ServiceItem.class, i))
            }
        }
        else {
            return null
        }
        return workOrder
    }

}