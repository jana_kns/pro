package org.dotcomm.pagecode.admin.department

import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.rows.DeptRow
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.admin.AdminMain;

/**
 * Jsf managed bean for the editdept.xhtml.
 *
 * @author jdolinski
 */
class EditDept {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Page code data */
    DeptRow deptRow

    /** Constructor */
    EditDept(AdminSecurityService adminSecurityService) {
        this.adminSecurityService = adminSecurityService
        Integer deptId = (Integer)FacesUtils.getPageFlowScopeParameter("deptid")
        deptRow = this.adminSecurityService.getDept(deptId)
        //if we transfered from this bean to add/edit a user, this will be populated with our old values.
        DeptRow prevPageValues = FacesUtils.getPageFlowScopeParameter('editdeptrow')
        if (prevPageValues != null) {
            deptRow.deptName = prevPageValues.deptName
            deptRow.deptDesc = prevPageValues.deptDesc
            deptRow.deptPhone = prevPageValues.deptPhone
            deptRow.deptActive = prevPageValues.deptActive
            FacesUtils.removePageFlowScopeParameter 'editdeptrow'
        }
    }

    /**
     * Action method to edit an existing department in the system.
     * @return Faces navigation text.
     */
    String updateDept() {
        adminSecurityService.updateDept(deptRow)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

    /**
     * Action method for the cancel button.
     * @return Faces navigation text.
     */
    String cancel() {
        String navText = (String)FacesUtils.getPageFlowScopeParameter("depcancelnav")
        return navText
    }

    /**
     * Action method to navigate to edit user page.
     * @return Faces navigation text.
     */
    String editUser() {
        FacesUtils.putPageFlowScopeParameter 'editdeptrow', deptRow
        return 'edituser'
    }

    /**
     * Action method to navigate to add a new user page.
     * @return Faces navigation text.
     */
    String addUser() {
        FacesUtils.putPageFlowScopeParameter 'editdeptrow', deptRow
        return 'adduser'
    }

}
