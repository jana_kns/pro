package org.dotcomm.pagecode.admin.location

import org.dotcomm.pagecode.rows.LocationRow
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.admin.AdminMain
import org.dotcomm.pagecode.compositions.LocForm

/**
 * Jsf managed bean for the editloc.xhtml.
 *
 * @author jdolinski
 */
class EditLoc extends LocForm {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Constructor */
    EditLoc(AdminSecurityService adminSecurityService) {
        super(adminSecurityService)
        this.adminSecurityService = adminSecurityService
        Integer locId = (Integer)FacesUtils.getPageFlowScopeParameter("locid")
        locationRow = this.adminSecurityService.getLocation(locId)
    }

    /**
     * Action method to edit an existing location in the system.
     * @return Faces navigation text.
     */
    String updateLoc() {
        adminSecurityService.updateLocation(locationRow)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

}