package org.dotcomm.pagecode.admin.maintenancegroup

import org.dotcomm.pagecode.rows.MtnceGroupRow
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.admin.AdminMain
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.compositions.MtnceForm
import org.dotcomm.services.AdminWorkOrderService;

/**
 * Jsf managed bean for the addmtnce.xhtml.
 *
 * @author jdolinski
 */
class AddMtnce extends MtnceForm {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Constructor */
    AddMtnce(AdminSecurityService adminSecurityService, AdminWorkOrderService adminWorkOrderService) {
        super(adminSecurityService,adminWorkOrderService)
        this.adminSecurityService = adminSecurityService
        mtnceGroupRow = new MtnceGroupRow()
        mtnceGroupRow.active = new Boolean(true)
    }

    /**
     * Action method to add a new maintenance group to the system.
     * @return Faces navigation text.
     */
    String newMtnceGroup() {
        adminSecurityService.addNewMtnceGroup (mtnceGroupRow)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

}
