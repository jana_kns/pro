package org.dotcomm.pagecode.rows;

/**
 * Page code bean for displaying a work order's type rows.
 *
 * @author jdolinski
 */
class WoTypeRow {

    Integer woTypeId
    String woTypeName
    String woTypeDesc
    String woTypeStatus
    Boolean woActive
    Date woLastUpd
    List<MtnceGroupRow> listMtnceGroups

}
