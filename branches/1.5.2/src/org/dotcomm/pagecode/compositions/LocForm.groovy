package org.dotcomm.pagecode.compositions

import org.dotcomm.services.AdminSecurityService
import javax.faces.model.SelectItem
import org.dotcomm.pagecode.adaptors.SelectDeptAdaptor
import org.dotcomm.pagecode.rows.LocationRow

/**
 * Facelet compostion backing bean for the locform.xhtml.
 *
 * @author JDolinski
 */
class LocForm {

    /** Page code data */
    LocationRow locationRow
    List<SelectItem> pcListDepts

    /** Constructor */
    LocForm(AdminSecurityService adminSecurityService) {
        SelectDeptAdaptor selectDeptAdaptor = new SelectDeptAdaptor()
        pcListDepts = selectDeptAdaptor.adapt(adminSecurityService.getAllActiveDepartments())
    }

}