package org.dotcomm.pagecode.reports

import org.apache.myfaces.trinidad.model.ChartModel
import org.dotcomm.services.WorkOrderService
import org.apache.myfaces.trinidad.event.ChartDrillDownEvent
import org.dotcomm.pagecode.reports.adaptors.ChartModelAdaptor
import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.util.JasperJsfReportUtil
import org.dotcomm.util.JasperJsfReportUtil.Format
import org.dotcomm.pagecode.rows.ServiceGroupItemRow;

/**
 * Jsf managed bean for the dashboard.xhtml.
 *
 * @author jdolinski
 */
class Dashboard {

  /** Injected Property */
    WorkOrderService workOrderService

    /** Page code data */
    ChartModel statusChart

    /** Page code data */
    ChartModel deptChart

    /** Page code data */
    ChartModel servChart

    /** Page code data */
    ChartModel typeChart

    /** Constructor. */
    Dashboard(WorkOrderService workOrderService) {
        this.workOrderService = workOrderService
        ChartModelAdaptor chartModelAdaptor = new ChartModelAdaptor('')
        statusChart = chartModelAdaptor.adapt(workOrderService.getWorkOrderStatusesInSystem())
        chartModelAdaptor = new ChartModelAdaptor('')
        deptChart = chartModelAdaptor.adapt(workOrderService.getNumberOfWorkOrdersForEachDept())
        chartModelAdaptor = new ChartModelAdaptor('')
        servChart = chartModelAdaptor.adapt(workOrderService.getNumberOfWorkOrdersServiceGroups())
        chartModelAdaptor = new ChartModelAdaptor('')
        typeChart = chartModelAdaptor.adapt(workOrderService.getNumberOfWorkByType())
    }

  /** This is not working with JSF 1.1!!!! */
  void drillDown(ChartDrillDownEvent e) {
        println 'In the chart listener!!!!!!!!!!!'
    }

  /** Create pdf of the details for the current work load. */
  void printDetailCurrentStatus() {
      List<WorkOrderRow> results = workOrderService.getWorkOrderReportData(false).sort() {WorkOrderRow it -> it.woLocation}
      JasperJsfReportUtil jasperJsfReportUtil = new JasperJsfReportUtil()
      jasperJsfReportUtil.runReport(Format.pdf, "/WEB-INF/reports/currentStatus.jrxml", results.sort {WorkOrderRow it -> it.woStatus}.reverse())
  }

  /** Create pdf of the details for the type of work orders. */
  void printDetailTypeWo() {
      List<WorkOrderRow> results = workOrderService.getWorkOrderReportData(true).sort() {WorkOrderRow it -> it.woLocation}
      JasperJsfReportUtil jasperJsfReportUtil = new JasperJsfReportUtil()
      jasperJsfReportUtil.runReport(Format.pdf, "/WEB-INF/reports/typeWorkOrders.jrxml", results.sort {WorkOrderRow it -> it.woType})
  }

  /** Create pdf of the details for department activity. */
  void printDetailDeptActivity() {
      List<WorkOrderRow> results = workOrderService.getWorkOrderReportData(true).sort() {WorkOrderRow it -> it.woLocation}
      JasperJsfReportUtil jasperJsfReportUtil = new JasperJsfReportUtil()
      jasperJsfReportUtil.runReport(Format.pdf, "/WEB-INF/reports/departmentActivity.jrxml", results.sort {WorkOrderRow it -> it.woDepartment})
  }

  /** Create pdf of the details for service areas. */
  void printDetailServArea() {
      List<WorkOrderRow> results = workOrderService.getServiceItemReportData()
      JasperJsfReportUtil jasperJsfReportUtil = new JasperJsfReportUtil()
      jasperJsfReportUtil.runReport(Format.pdf, "/WEB-INF/reports/serviceAreas.jrxml", results.sort {WorkOrderRow it -> it.serviceItems})
  }

}
