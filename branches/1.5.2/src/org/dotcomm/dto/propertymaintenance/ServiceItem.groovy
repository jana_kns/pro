package org.dotcomm.dto.propertymaintenance;

/**
 * DTO for the ServiceItem table.
 *
 * @author jdolinski
 */
class ServiceItem {

	Integer serviceItemId
    Date createDateTime
	Collection workOrderMany
	ServiceGroup serviceGroupId
	Date lastUpdatedDateTime
	String name
	Integer enabled
    
}
