import groovy.sql.Sql
import java.sql.ResultSet

/**
 * Script to update email addresses from Lotus Notes file dump (Jim Hay) to current users.
 *
 * @author jdolinski
 * 5/10/2010
 *
 * Record layout - Name,Company,E-Mail
 *            IE - "Abboud Frederick L",District Court,FAbboud@co.douglas.ne.us
 */

String fileLocation = 'lotusnotes-emails.csv'
Integer matchCnt = 0
//Test
//Sql sql = Sql.newInstance("jdbc:db2://martina.co.douglas.ne.us:50002/dbt202", "mtn001", "mtn001", "com.ibm.db2.jcc.DB2Driver")
//Production
Sql sql = Sql.newInstance("jdbc:db2://martina.co.douglas.ne.us:50002/dbp202", "mtn001", "mtn001", "com.ibm.db2.jcc.DB2Driver")
new File(fileLocation).splitEachLine(',') {List tokens ->
  String email = tokens[2]
  String[] name = tokens[0].split()
  String fname = ''
  String lname = ''
  lname = name[0].toUpperCase()
  try {
    fname = name[1].toUpperCase()
  } catch (IndexOutOfBoundsException e) {}//do nothing, we have NO first name.

  sql.query ("Select user_id from mtn001.users where fname = $fname and lname = $lname", { ResultSet rs ->
      if (rs.next()) {
        if (email != null) {
          matchCnt++
          Integer userId = rs.getInt(1)
          println "Found user: $fname $lname id: $userId updating email address $email"
          String update = "update mtn001.users set email = '$email' where user_id = $userId"
          //println update
          sql.execute(update)
        }
      }
      rs.close()
  })
}
println "\nMatched $matchCnt users."
