package org.dotcomm.pagecode;

import javax.faces.context.FacesContext;
import org.dotcomm.util.FacesUtils
import org.springframework.security.AuthenticationServiceException
import org.springframework.security.DisabledException
import org.springframework.security.BadCredentialsException
import org.springframework.security.LockedException
import org.springframework.security.concurrent.ConcurrentLoginException
import org.springframework.security.ui.AbstractProcessingFilter;

/**
 * Jsf managed bean for the login.xhtml.
 *
 * @author JDolinski
 */
class Login {

	String username
	String password

	/**
	 * The constructor checks for any acegi exceptions and outputs a faces
	 * message to the page.
	 */
	Login() {
		Exception ex = (Exception) FacesContext
				.getCurrentInstance()
				.getExternalContext()
				.getSessionMap()
				.get(AbstractProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY)
		if (ex != null) {
			String userMessage = ex.getMessage()
			if (ex instanceof AuthenticationServiceException) {
			}
			if (ex instanceof BadCredentialsException) {
				userMessage = FacesUtils.getStringFromBundle('invaliduser')
			}
			if (ex instanceof DisabledException) {
                userMessage = FacesUtils.getStringFromBundle('accountdisable')
			}
			if (ex instanceof LockedException) {
			}
			if (ex instanceof ConcurrentLoginException) {
			}
			FacesUtils.addErrorMessage("j_username", "")
			FacesUtils.addErrorMessage("j_password", userMessage)
		}
	}

}
