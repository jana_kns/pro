package org.dotcomm.pagecode.adaptors

import org.dotcomm.pagecode.rows.DeptRow
import javax.faces.model.SelectItem
import org.dotcomm.util.Adaptor

/**
 * Adaptor pattern to convert presentation objects to Apache Trinidad specific objects.
 *
 * @author jdolinski
 */
class SelectDeptAdaptor implements Adaptor {

    /**
     * Adapt a list of view rows to selection items.
     * @return list of select items
     */
    public Object adapt(Object obj) {
        List<SelectItem> listSelectItems = new ArrayList<SelectItem>()
        List<DeptRow> listDeptRows = (ArrayList<DeptRow>) obj
        for (DeptRow d : listDeptRows) {
            listSelectItems.add(new SelectItem(d.deptId, d.deptName, d.deptDesc))
        }
        return listSelectItems.sort {SelectItem it -> it.label}
    }

    public Class<ArrayList<DeptRow>> getFromType() {
        return ArrayList
    }

    public Class<ArrayList<SelectItem>> getToType() {
        return ArrayList
    }

    /**
     * Merge object from list of select items to list of view rows.
     * @return list view row
     */
    public Object merge(Object obj) {
        List<DeptRow> listDeptRows = new ArrayList<DeptRow>()
        List<SelectItem> listSelectItems = (ArrayList<SelectItem>) obj
        for (SelectItem i : listSelectItems) {
            listDeptRows.add(new DeptRow(deptId:i.getValue(),deptName:i.getLabel(),deptDesc:i.getDescription()))
        }
        return listDeptRows
    }

}