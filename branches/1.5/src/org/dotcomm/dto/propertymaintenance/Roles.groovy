package org.dotcomm.dto.propertymaintenance;

/**
 * DTO for the Roles table.
 *
 * @author jdolinski
 */
class Roles {

	Integer roleId
	Collection users
	String roleName
	String roleDescr
    String role

}
