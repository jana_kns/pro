package org.dotcomm.dto.propertymaintenance.adaptors

import org.dotcomm.dto.propertymaintenance.Location
import org.dotcomm.pagecode.rows.LocationRow
import org.dotcomm.dto.propertymaintenance.Departments
import org.dotcomm.util.Adaptor
import org.dotcomm.pagecode.rows.MtnceGroupRow
import org.dotcomm.dto.propertymaintenance.MtnceGroup
import org.dotcomm.persistence.DataAccessObject

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class LocationRowAdaptor implements Adaptor {

    /** Injected Property */
    DataAccessObject dao

    /**
     * Adapt persistant dto for view.
     * @return object for view
     */
    public Object adapt(Object obj) {
        LocationRow locationRow = new LocationRow()
        Location location = (Location) obj
        if (location != null) {
            locationRow.locId = location.locationId
            locationRow.lastUpd = location.lastUpdatedDateTime
            locationRow.locName = location.name
            locationRow.locDesc = location.descr
            locationRow.locDept = location.deptId.deptName
            locationRow.locDeptId = location.deptId.deptId
            locationRow.locStatus = location.enabled.intValue() == 1 ? 'Active' : 'Inactive'
            locationRow.locActive = location.enabled.intValue() == 1 ? new Boolean(true) : new Boolean(false)
            locationRow.listMtnceGroups = new ArrayList<MtnceGroupRow>()
            for (MtnceGroup mg : location.mtnceGroupMany) {
                locationRow.listMtnceGroups.add(new MtnceGroupRow(groupId:mg.mtnceGroupId, groupName:mg.name))
            }
        }
        else {
            return null
        }
        return locationRow
    }

    public Class<Location> getFromType() {
        return Location
    }

    public Class<LocationRow> getToType() {
        return LocationRow
    }

    /**
     * Merge object for view to persistant dto.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        Location location = new Location()
        LocationRow locationRow = (LocationRow) obj
        if (locationRow != null) {
            location.locationId = locationRow.locId
            location.lastUpdatedDateTime = locationRow.lastUpd
            location.name = locationRow.locName
            location.descr = locationRow.locDesc
            location.deptId = new Departments(deptId:locationRow.locDeptId)
            location.enabled = locationRow.locActive ? new Integer(1) : new Integer(0)
            location.mtnceGroupMany = new ArrayList<MtnceGroup>()
            for (MtnceGroupRow row : locationRow.listMtnceGroups) {
                //load() method always tries to return a proxy and only returns
                //an initialized instance if it is already managed in the current persistence context.
                //A get will always hit the database. Since we are just adding a item to a collection
                //we do not need to hit the db and get the value, the proxy will work just fine.
                location.mtnceGroupMany.add(dao.load(MtnceGroup.class, row.groupId))
            }
        }
        else {
            return null
        }
        return location
    }

}