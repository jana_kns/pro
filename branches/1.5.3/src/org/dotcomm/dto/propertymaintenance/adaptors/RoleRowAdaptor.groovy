package org.dotcomm.dto.propertymaintenance.adaptors

import org.dotcomm.util.Adaptor
import org.dotcomm.dto.propertymaintenance.Roles
import org.dotcomm.pagecode.rows.RoleRow

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class RoleRowAdaptor implements Adaptor {

    /**
     * Adapt persistant dto for view.
     * @return object for view
     */
    public Object adapt(Object obj) {
        RoleRow roleRow = new RoleRow()
        Roles roles = (Roles) obj
        if (roles != null) {
            roleRow.roleId = roles.roleId
            roleRow.roleName = roles.roleName
            roleRow.roleDesc = roles.roleDescr
            roleRow.role = roles.role
        }
        else {
            return null
        }
        return roleRow
    }

    public Class<Roles> getFromType() {
        return Roles
    }

    public Class<RoleRow> getToType() {
        return RoleRow
    }

    /**
     * Merge object for view to persistant dto.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        Roles roles = new Roles()
        RoleRow roleRow = (RoleRow) obj
        if (roleRow != null) {
            roles.roleId = roleRow.roleId
            roles.roleName = roleRow.roleName
            roles.roleDescr = roleRow.roleDesc
            roles.role = roleRow.role
        }
        else {
            return null
        }
        return roles
    }

}