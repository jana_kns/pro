package org.dotcomm.services;

import org.dotcomm.pagecode.rows.*;

import java.util.List;

/**
 * Defines the business services for managing the work order
 * information in the application.
 *
 * @author JDolinski
 *
 */
public interface AdminWorkOrderService {

    /**
     * Get all the messages posted in the system and display by most recent.
     * @return list List of messages
     */
    List<MsgPostRow> getAllMsgPostItems();

    /**
     * Get a specific message posted in the system.
     * @param id of the record
     * @return message A single message object
     */
    MsgPostRow getMsgPostItem(Integer id);

    /**
     * Add a new message.
     * @param msgPostRow object
     */
    void addNewMsgPost(MsgPostRow msgPostRow);

    /**
     * Update an existing message.
     * @param msgPostRow object
     */
    void updateMsgPost(MsgPostRow msgPostRow);

    /**
     * Delete an existing message.
     * @param id of the object to delete
     */
    void deleteMsgPost(Integer id);

    /**
     * Get all the service groups in the system.
     * @return list List of service groups
     */
    List<ServiceGroupRow> getAllServiceGroupItems();

    /**
     * Get all the active service items in the system.
     * @return list List of service items
     */
    List<ServiceGroupItemRow> getAllActiveServiceItems();

    /**
     * Get all the service items in the system.
     * @return list List of service items
     */
    List<ServiceGroupItemRow> getAllServiceItems();

    /**
     * Get all the active service groups in the system.
     * @return list List of service groups
     */
    List<ServiceGroupRow> getAllActiveServiceGroupItems();

    /**
     * Get a specific service group in the system.
     * @param id of the record
     * @return Service Group A single message object
     */
    ServiceGroupRow getServiceGroup(Integer id);

    /**
     * Add a service group.
     * @param serviceGroupRow object
     */
    void addNewServiceGroup(ServiceGroupRow serviceGroupRow);

    /**
     * Update an existing service group.
     * @param serviceGroupRow object
     */
    void updateServiceGroup(ServiceGroupRow serviceGroupRow);

    /**
     * Get all the work order types in the system.
     * @return list List of types of work orders 
     */
    List<WoTypeRow> getAllWorkOrderTypeItems();

    /**
     * Get all the work order types in the system.
     * @return list List of types of work orders
     */
    List<WoTypeRow> getAllActiveWorkOrderTypeItems();

    /**
     * Get a specific work order type in the system.
     * @param id of the record
     * @return type A single work order type object
     */
    WoTypeRow getWorkOrderTypeItem(Integer id);

    /**
     * Add a new work order type.
     * @param woTypeRow object
     */
    void addNewWoType(WoTypeRow woTypeRow);

    /**
     * Update an existing message.
     * @param woTypeRow object
     */
    void updateWoType(WoTypeRow woTypeRow);

}
