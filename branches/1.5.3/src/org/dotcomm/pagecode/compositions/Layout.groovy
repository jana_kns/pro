package org.dotcomm.pagecode.compositions

import org.dotcomm.pagecode.rows.UserTreeRow
import org.apache.myfaces.trinidad.model.TreeModel
import org.apache.myfaces.trinidad.model.ChildPropertyTreeModel
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.pagecode.rows.MtnceGroupRow
import org.dotcomm.pagecode.admin.AdminMain
import org.dotcomm.util.FacesUtils

/**
 * Facelet compostion backing bean for the layout.xhtml.
 *
 * @author jdolinski
 */
class Layout {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Apache tree model. */
    TreeModel treeModel

    /** Constructor */
    Layout(AdminSecurityService adminSecurityService) {
        this.adminSecurityService = adminSecurityService
        UserRow userRow = adminSecurityService.getUserLoggedIn()
        UserTreeRow dept = new UserTreeRow(null, userRow.dept, null, false)
        UserTreeRow user = new UserTreeRow(userRow.userId, userRow.fullName, "edituser", true)
        dept.getChildren().add(user);
        for (MtnceGroupRow row : userRow.listMtnceGroups) {
            UserTreeRow mtnceGroup = new UserTreeRow(null, row.groupName, null, false)
            user.getChildren().add(mtnceGroup)
        }
        List nodes = new ArrayList()
        nodes.add(dept)
        this.treeModel = new ChildPropertyTreeModel(nodes, "children")
    }

    /**
     * Action method to edit an existing user in the system.
     * @return Faces navigation text.
     */    
    String editNode() {
        return "edituser"
    }

    /**
     * Action method to navigate and setup the admin page.
     * @return Faces navigation text.
     */
    String adminMain() {
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return 'admin'
    }

}
