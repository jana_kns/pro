package org.dotcomm.dao.hibernate

import org.dotcomm.dao.WorkOrderDao
import org.dotcomm.dto.propertymaintenance.WorkOrder
import org.hibernate.Query
import org.springframework.orm.hibernate3.support.HibernateDaoSupport
import org.dotcomm.dto.propertymaintenance.ServiceItem
import org.dotcomm.pagecode.reports.ReportSearchCriteria

/**
 * The implementation for {@link WorkOrderDao}.
 *
 * @author JDolinski
 *
 */
class HibernateWorkOrderDao extends HibernateDaoSupport implements WorkOrderDao {

    /**
     * Base hql used for the queries.
     */
    final String hqlWoSelectStatement = """
                      select wo
                      from WorkOrder as wo
                      inner join wo.userId as user
                      inner join wo.woTypeId as wotype
                      inner join wo.locationId as loc
                      inner join loc.deptId as dept
                      inner join wo.statusId as status
                      inner join wo.archiveId as archive
                                        """

    /**
     * Count hql used to determine number of records.
     */
    final String hqlWoCountStatement = """
                      select count(wo)
                      from WorkOrder as wo
                      inner join wo.userId as user
                      inner join wo.woTypeId as wotype
                      inner join wo.locationId as loc
                      inner join loc.deptId as dept
                      inner join wo.statusId as status
                      inner join wo.archiveId as archive
                                        """

    /**
     * HQL to filter work orders for a maintenance group.
     */
    final String hqlMtnceGroupFilter = """
                      and (wotype.woTypeId in
                        (select distinct(wotype.woTypeId)
                        from MtnceGroup as mgroup
                        inner join mgroup.woTypeMany as wotype
                        where mgroup.mtnceGroupId = :mtnceId)
                        and loc.locationId in
                          (select distinct(loc.locationId)
                          from MtnceGroup as mgroup
                          inner join mgroup.locationMany loc
                          where mgroup.mtnceGroupId = :mtnceId)
                      or wotype.woTypeId in
                        (select distinct(wotype.woTypeId)
                         from MtnceGroup as mgroup
                         inner join mgroup.woTypeMany as wotype
                         left outer join mgroup.locationMany woloc
                         where mgroup.mtnceGroupId = :mtnceId
                         and woloc.locationId is null
                        )
                      )
                                       """

    /**
     * HQL to filter work orders for a user that has a maintenance role.
     */
    final String hqlMtnceUserFilter = """
                    and (wo.workOrderId in
                        (select distinct(wo.workOrderId)
                        from Users as usr
                        inner join usr.roleId as role
                        inner join usr.mtnceGroupMany as mgroup
                        inner join mgroup.woTypeMany as wotype
                        inner join mgroup.locationMany as loc
                        inner join loc.workOrder as wo
                        where usr.userId = :mtnceId
                        and wo.woTypeId = wotype.woTypeId)
                    or wotype.woTypeId in (
                        select distinct(wotype.woTypeId)
                        from Users as usr
                        inner join usr.roleId as role
                        inner join usr.mtnceGroupMany as mgroup
                        inner join mgroup.woTypeMany as wotype
                        left outer join mgroup.locationMany as mgloc
                        where usr.userId = :mtnceId
                        and mgloc.locationId is null
                        )
                    )
                                      """

    /** {@inheritDoc} */
    List<WorkOrder> filterWorkOrdersForSuper(WorkOrder workOrder, Date fromCreateDate, Date toCreateDate, Integer firstResult, Integer maxRows) {
        String hql = hqlWoSelectStatement
        hql = buildQueryForSuper(toCreateDate, fromCreateDate, workOrder, hql)
        hql = hqlOrderBy(hql,workOrder.sortColumn)
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        setNamedParams(query, workOrder, fromCreateDate, toCreateDate)
        if (maxRows != null && firstResult != null) {
            query.setMaxResults(maxRows)
            query.setFirstResult(firstResult)
        }
        return (List<WorkOrder>)query.list()
    }

    /** {@inheritDoc} */
    Integer countWorkOrdersForSuper(WorkOrder workOrder, Date fromCreateDate, Date toCreateDate) {
        String hql = hqlWoCountStatement
        hql = buildQueryForSuper(toCreateDate, fromCreateDate, workOrder, hql)
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        setNamedParams(query, workOrder, fromCreateDate, toCreateDate)
        return (Integer)query.uniqueResult()
    }

    /**
     * Add the Super Administrator's query parameters to the base query.
     * @param workOrder The object with the query values
     * @param fromCreateDate The begining value for create date range
     * @param toCreateDate The ending value from create date range
     * @param hql The base statement
     */
    private String buildQueryForSuper(Date toCreateDate, Date fromCreateDate, WorkOrder workOrder, String hql) {
        if (!workOrder?.serviceItemMany?.isEmpty()) {
            hql = "$hql left outer join wo.serviceItemMany as servitem" //we are going to filter on service items
        }
        hql = "$hql where archive.archiveId = :archiveId"
        hql = locationFilter(workOrder, hql)
        hql = woTypeFilter(workOrder, hql)
        hql = statusFilter(workOrder, hql)
        hql = deptFilter(workOrder, hql)
        hql = woNumberFilter(workOrder, hql)
        hql = mtnceGroupFilter(workOrder, hql)
        hql = userFilter(workOrder, hql)
        hql = serviceFilter(workOrder, hql)
        hql = createDateFilter(fromCreateDate, toCreateDate, hql)
        hql = woReqCommentsFilter(workOrder, hql)
        return hql
    }

    /** {@inheritDoc} */
    List<WorkOrder> filterWorkOrdersForMtnce(WorkOrder workOrder, Date fromCreateDate, Date toCreateDate, Integer firstResult, Integer maxRows) {
        String hql = "$hqlWoSelectStatement where archive.archiveId = :archiveId $hqlMtnceUserFilter"
        hql = buildQueryForMtnce(toCreateDate, fromCreateDate, workOrder, hql)
        hql = hqlOrderBy(hql,workOrder.sortColumn)
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        setNamedParams(query, workOrder, fromCreateDate, toCreateDate)
        if (maxRows != null && firstResult != null) {
            query.setMaxResults(maxRows)
            query.setFirstResult(firstResult)
        }
        return (List<WorkOrder>)query.list()
    }

    /** {@inheritDoc} */
    Integer countWorkOrdersForMtnce(WorkOrder workOrder, Date fromCreateDate, Date toCreateDate) {
        String hql = "$hqlWoCountStatement where archive.archiveId = :archiveId $hqlMtnceUserFilter"
        hql = buildQueryForMtnce(toCreateDate, fromCreateDate, workOrder, hql)
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        setNamedParams(query, workOrder, fromCreateDate, toCreateDate)
        return (Integer)query.uniqueResult()
    }

    /**
     * Add the maintenance user's query parameters to the base query.
     * @param workOrder The object with the query values
     * @param fromCreateDate The begining value for create date range
     * @param toCreateDate The ending value from create date range
     * @param hql The base statement
     */
    private String buildQueryForMtnce(Date toCreateDate, Date fromCreateDate, WorkOrder workOrder, String hql) {
        hql = statusFilter(workOrder, hql)
        hql = locationFilter(workOrder, hql)
        hql = woTypeFilter(workOrder, hql)
        hql = deptFilter(workOrder, hql)
        hql = woNumberFilter(workOrder, hql)
        hql = userFilter(workOrder, hql)
        hql = createDateFilter(fromCreateDate, toCreateDate, hql)
        return hql
    }

    /** {@inheritDoc} */
    List<WorkOrder> filterWorkOrdersForDeptHead(WorkOrder workOrder, Date fromCreateDate, Date toCreateDate, Integer firstResult, Integer maxRows) {
        String hql = "$hqlWoSelectStatement"
        hql = buildQueryForDeptHead(toCreateDate, fromCreateDate, workOrder, hql)
        hql = hqlOrderBy(hql,workOrder.sortColumn)
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        setNamedParams(query, workOrder, fromCreateDate, toCreateDate)
        if (maxRows != null && firstResult != null) {
            query.setMaxResults(maxRows)
            query.setFirstResult(firstResult)
        }
        return (List<WorkOrder>)query.list()
    }

    /** {@inheritDoc} */
    Integer countWorkOrdersForDeptHead(WorkOrder workOrder, Date fromCreateDate, Date toCreateDate) {
        String hql = "$hqlWoCountStatement"
        hql = buildQueryForDeptHead(toCreateDate, fromCreateDate, workOrder, hql)
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        setNamedParams(query, workOrder, fromCreateDate, toCreateDate)
        return (Integer)query.uniqueResult()
    }

    /**
     * Add the dept head's query parameters to the base query.
     * @param workOrder The object with the query values
     * @param fromCreateDate The begining value for create date range
     * @param toCreateDate The ending value from create date range
     * @param hql The base statement
     */
    private String buildQueryForDeptHead(Date toCreateDate, Date fromCreateDate, WorkOrder workOrder, String hql) {
        hql = "$hql where archive.archiveId = :archiveId"
        hql = deptFilter(workOrder, hql)
        hql = statusFilter(workOrder, hql)
        hql = woNumberFilter(workOrder, hql)
        hql = locationFilter(workOrder, hql)
        hql = woTypeFilter(workOrder, hql)
        hql = createDateFilter(fromCreateDate, toCreateDate, hql)
        return hql
    }

    /** {@inheritDoc} */
    List<WorkOrder> filterWorkOrdersForUser(WorkOrder workOrder, Integer firstResult, Integer maxRows) {
        String hql = "$hqlWoSelectStatement"
        hql = buildQueryForUser(workOrder, hql)
        hql = hqlOrderBy(hql,workOrder.sortColumn)
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        setNamedParams(query, workOrder, null, null)
        if (maxRows != null && firstResult != null) {
            query.setMaxResults(maxRows)
            query.setFirstResult(firstResult)
        }
        return (List<WorkOrder>)query.list()
    }

    /** {@inheritDoc} */
    Integer countWorkOrdersForUser(WorkOrder workOrder) {
        String hql = "$hqlWoCountStatement"
        hql = buildQueryForUser(workOrder, hql)
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        setNamedParams(query, workOrder, null, null)
        return (Integer)query.uniqueResult()
    }

    /**
     * Add the user's query parameters to the base query.
     * @param workOrder The object with the query values
     * @param hql The base statement
     */
    private String buildQueryForUser(WorkOrder workOrder, String hql) {
        hql = "$hql where archive.archiveId = :archiveId"
        hql = userFilter(workOrder, hql)
        hql = statusFilter(workOrder, hql)
        hql = woNumberFilter(workOrder, hql)
        return hql
    }
    
    /**
     * Add the query parameters to the hql query.
     * @param query The query.
     */
    private void setNamedParams(Query query, WorkOrder workOrder, Date fromDate, Date toDate) {
        for (String s : query.getNamedParameters()) {
            switch (s) {
                case 'archiveId':
                    query.setInteger('archiveId', workOrder.archiveId.archiveId)
                    break
                case 'createDate':
                    query.setDate('createDate', workOrder.createDateTime)
                    break
                case 'fromDate':
                    query.setDate('fromDate', fromDate)
                    break
                case 'toDate':
                    query.setDate('toDate', toDate)
                    break
                case 'deptId':
                    query.setInteger('deptId', workOrder.locationId.deptId.deptId)
                    break
                case 'locId':
                    query.setInteger('locId', workOrder.locationId.locationId)
                    break
                case 'mtnceId':
                    query.setInteger('mtnceId', workOrder.mtnceGroup)
                    break                
                case 'servId':
                    ServiceItem serviceItem = ((ServiceItem)workOrder.serviceItemMany.iterator().next())
                    query.setInteger('servId', serviceItem.serviceItemId)
                    break
                case 'statusId':
                    query.setInteger('statusId', workOrder.statusId.statusId)
                    break
                case 'typeId':
                    query.setInteger('typeId', workOrder.woTypeId.woTypeId)
                    break
                case 'userId':
                    query.setInteger('userId', workOrder.userId.userId)
                    break
                case 'woNum':
                    query.setInteger('woNum', workOrder.workOrderId)
                    break
              case 'woNum':
                  query.setInteger('woNum', workOrder.workOrderId)
                  break
              case 'requestComments':
                  query.setString('requestComments', "%" + workOrder.requestComments.toLowerCase() + "%")
                  break
            }
        }
    }

    /**
     * Add an order by statement to the hql.
     * @param Current hql statement
     * @return New hql statement
     */
    private String hqlOrderBy(String hql, String sortColumn) {
        if (sortColumn == null) {
            hql = "$hql order by wo.workOrderId desc"
        } else {
            hql = "$hql order by wo.${sortColumn}"
        }
        return hql
    }

    /**
     * Add the sql to filter work orders by a maintenance group.
     * @param Work Order
     * @param Current hql statement
     * @return New hql statement
     */
    private String mtnceGroupFilter(WorkOrder workOrder, String hql) {
        if (workOrder.mtnceGroup != null) {
            hql = "$hql $hqlMtnceGroupFilter"
        }
        return hql
    }

    /**
     * Add the sql to filter work orders created by a user.
     * @param Work Order
     * @param Current hql statement
     * @return New hql statement
     */
    private String userFilter(WorkOrder workOrder, String hql) {
        if (workOrder?.userId?.userId != null) {
            hql = "$hql and user.userId = :userId"
        }
        return hql
    }

    /**
     * Add the sql to filter work orders by a location.
     * @param Work Order
     * @param Current hql statement
     * @return New hql statement
     */
    private String locationFilter(WorkOrder workOrder, String hql) {
        if (workOrder?.locationId?.locationId != null) {
            hql = "$hql and loc.locationId = :locId"
        }
        return hql
    }

    /**
     * Add the sql to filter work orders by a work order type.
     * @param Work Order
     * @param Current hql statement
     * @return New hql statement
     */
    private String woTypeFilter(WorkOrder workOrder, String hql) {
        if (workOrder?.woTypeId?.woTypeId != null) {
            hql = "$hql and wotype.woTypeId = :typeId"
        }
        return hql
    }

    /**
     * Add the sql to filter work orders by a dept.
     * @param Work Order
     * @param Current hql statement
     * @return New hql statement
     */
    private String deptFilter(WorkOrder workOrder, String hql) {
        if (workOrder?.locationId?.deptId?.deptId != null) {
            hql = "$hql and dept.deptId = :deptId"
        }
        return hql
    }

    /**
     * Add the sql to filter work orders by status.
     * @param Work Order
     * @param Current hql statement
     * @return New hql statement
     */
    private String statusFilter(WorkOrder workOrder, String hql) {
        if (workOrder?.statusId?.statusId != null) {
            hql = "$hql and status.statusId = :statusId"
        }
        return hql
    }

    /**
     * Add the sql to filter work orders by service item.
     * @param Work Order
     * @param Current hql statement
     * @return New hql statement
     */
    private String serviceFilter(WorkOrder workOrder, String hql) {
        if (!workOrder?.serviceItemMany?.isEmpty()) {
            hql = "$hql and servitem.serviceItemId = :servId"
        }
        return hql
    }

    /**
     * Add the sql to filter work orders by a work order number.
     * @param Work Order
     * @param Current hql statement
     * @return New hql statement
     */
    private String woNumberFilter(WorkOrder workOrder, String hql) {
        if (workOrder.workOrderId != null && workOrder.createDateTime != null) {
            hql = "$hql and wo.workOrderId = :woNum and date(wo.createDateTime) = :createDate"
        }
        return hql
    }

    /**
     * Add the sql to filter work orders by the requestors comments.
     * @param Work Order
     * @param Current hql statement
     * @return New hql statement
     */
    private String woReqCommentsFilter(WorkOrder workOrder, String hql) {
        if (workOrder.requestComments != null) {
            hql = "$hql and lower(wo.requestComments) like :requestComments"
        }
        return hql
    }

    /**
     * Add the sql to filter work orders by creation date range.
     * @param Work Order
     * @param Current hql statement
     * @return New hql statement
     */
    private String createDateFilter(Date fromDate, Date toDate, String hql) {
        if (fromDate != null) {
            hql = "$hql and date(wo.createDateTime) >= :fromDate"
        }
        if (toDate != null) {
            hql = "$hql and date(wo.createDateTime) <= :toDate"
        }
        return hql
    }

    /** {@inheritDoc} */
    List<Object[]> getWoTypesCounts(ReportSearchCriteria criteria) {
        String hql = """
                     select
                     count(wo.woTypeId), wotype.name
                     from WorkOrder as wo
                     inner join wo.woTypeId as wotype
                     inner join wo.archiveId as archive
                     inner join wo.locationId as loc
                     inner join loc.deptId as dept
                     where archive.archiveId = 1
                     """
        if (criteria.deptId != null) {
            hql = "$hql and dept.deptId = :deptId "
        }
        hql = createDateFilter(criteria.createStartDate,criteria.createEndDate,hql)
        hql = "$hql group by wo.woTypeId, wotype.name"
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        if (criteria.deptId != null) {
            query.setInteger('deptId', criteria.deptId)
        }
        if (criteria.createStartDate != null) {
            query.setDate('fromDate', criteria.createStartDate)
        }
        if (criteria.createEndDate != null) {
            query.setDate('toDate', criteria.createEndDate)
        }
        query.setCacheable(true)
        return (ArrayList<Object[]>)query.list()
    }

    /** {@inheritDoc} */
    List<Object[]> getWoStatusCounts(ReportSearchCriteria criteria) {
        String hql = """
                     select
                     count(status.statusId), status.name
                     from WorkOrder as wo
                     inner join wo.statusId as status
                     inner join wo.archiveId as archive
                     inner join wo.locationId as loc
                     inner join loc.deptId as dept
                     where archive.archiveId = 0
                     """
        if (criteria.deptId != null) {
            hql = "$hql and dept.deptId = :deptId "
        }
        hql = createDateFilter(criteria.createStartDate,criteria.createEndDate,hql)
        hql = "$hql group by status.statusId, status.name"
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        if (criteria.deptId != null) {
            query.setInteger('deptId', criteria.deptId)
        }
        if (criteria.createStartDate != null) {
            query.setDate('fromDate', criteria.createStartDate)
        }
        if (criteria.createEndDate != null) {
            query.setDate('toDate', criteria.createEndDate)
        }
        query.setCacheable(true)
        return (ArrayList<Object[]>)query.list()
    }

    /** {@inheritDoc} */
    List<Object[]> getServiceGroupCounts(ReportSearchCriteria criteria) {
        String hql = """
                     select
                     count(servgroup.serviceGroupId), servgroup.name
                     from WorkOrder as wo
                     inner join wo.serviceItemMany as servitem
                     inner join servitem.serviceGroupId as servgroup
                     inner join wo.archiveId as archive
                     inner join wo.locationId as loc
                     inner join loc.deptId as dept
                     where archive.archiveId = 1
                     """
        if (criteria.deptId != null) {
            hql = "$hql and dept.deptId = :deptId "
        }
        hql = createDateFilter(criteria.createStartDate,criteria.createEndDate,hql)
        hql = "$hql group by servgroup.serviceGroupId, servgroup.name"
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        if (criteria.deptId != null) {
            query.setInteger('deptId', criteria.deptId)
        }
        if (criteria.createStartDate != null) {
            query.setDate('fromDate', criteria.createStartDate)
        }
        if (criteria.createEndDate != null) {
            query.setDate('toDate', criteria.createEndDate)
        }
        query.setCacheable(true)
        return (ArrayList<Object[]>)query.list()
    }

    /** {@inheritDoc} */
    List<Object[]> getDeptCounts(ReportSearchCriteria criteria) {
        String hql = """
                     select
                     count(dept.deptId), dept.deptName
                     from WorkOrder as wo
                     inner join wo.locationId as loc
                     inner join loc.deptId as dept
                     inner join wo.archiveId as archive
                     where archive.archiveId = 1
                     """
        if (criteria.deptId != null) {
            hql = "$hql and dept.deptId = :deptId "
        }
        hql = createDateFilter(criteria.createStartDate,criteria.createEndDate,hql)
        hql = "$hql group by dept.deptId, dept.deptName"
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        if (criteria.deptId != null) {
            query.setInteger('deptId', criteria.deptId)
        }
        if (criteria.createStartDate != null) {
            query.setDate('fromDate', criteria.createStartDate)
        }
        if (criteria.createEndDate != null) {
            query.setDate('toDate', criteria.createEndDate)
        }
        query.setCacheable(true)
        return (ArrayList<Object[]>)query.list()
    }

    /** {@inheritDoc} */
    List<Object[]> getLastThirtyDayCreatedWorkOrders() {
        //todo add the date to the query
        String hql = """
                     select date(wo.createDateTime) as createDay, count(wo.workOrderId) as cnt
                     from WorkOrder as wo
                     inner join wo.archiveId as archive
                     where archive.archiveId = 0
                     group by date(wo.createDateTime)
                     """
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        query.setCacheable(true)
        return (ArrayList<Object[]>)query.list()
    }

    /** {@inheritDoc} */
    int updateArchiveInd(Date archiveOlderThanThisDate) {
        String hql = """
                    update WorkOrder wo set wo.archiveId = 1, wo.lastUpdatedDateTime = :currentDate
                    where wo.statusId = 3
                    and date(wo.lastUpdatedDateTime) <= :archiveDate
                    and wo.archiveId = 0
                    """
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        query.setDate('archiveDate', archiveOlderThanThisDate)
        query.setTimestamp('currentDate', new Date())
        return query.executeUpdate()
    }

    /** {@inheritDoc} */
    List<WorkOrder> completedWo(Date olderThanThisDate) {
        String hql = """
                    from WorkOrder wo
                    where wo.statusId = 3
                    and date(wo.lastUpdatedDateTime) <= :sqlDate
                    """
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        query.setDate('sqlDate', olderThanThisDate)
        return query.list()
    }

    /** {@inheritDoc} */
    List<WorkOrder> woReportData(ReportSearchCriteria criteria) {
        String hql = """
                    from WorkOrder wo
                    join fetch wo.locationId loc
                    join fetch loc.deptId
                    join fetch wo.userId usr
                    join fetch wo.woTypeId type
                    where wo.archiveId = :archiveId
                    """
        if (criteria.deptId != null) {
            hql = "$hql and wo.locationId.deptId = :deptId "
        }
        hql = createDateFilter(criteria.createStartDate,criteria.createEndDate,hql)
        hql = "$hql order by wo.locationId.name"
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        query.setInteger('archiveId',criteria.archiveData?1:0)
        if (criteria.deptId != null) {
            query.setInteger('deptId', criteria.deptId)
        }
        if (criteria.createStartDate != null) {
            query.setDate('fromDate', criteria.createStartDate)
        }
        if (criteria.createEndDate != null) {
            query.setDate('toDate', criteria.createEndDate)
        }
        return query.list()
    }

    /** {@inheritDoc} */
    List<WorkOrder> servItemReportData(ReportSearchCriteria criteria) {
        String hql = """
                    from WorkOrder wo
                    join fetch wo.serviceItemMany si
                    join fetch si.serviceGroupId sg
                    join fetch wo.locationId loc
                    join fetch loc.deptId
                    join fetch wo.userId usr
                    join fetch wo.woTypeId type
                    where wo.archiveId = :archiveId
                    """
        if (criteria.deptId != null) {
            hql = "$hql and wo.locationId.deptId = :deptId "
        }
        hql = createDateFilter(criteria.createStartDate,criteria.createEndDate,hql)
        Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql)
        query.setInteger('archiveId',criteria.archiveData?1:0)
        if (criteria.deptId != null) {
            query.setInteger('deptId', criteria.deptId)
        }
        if (criteria.createStartDate != null) {
            query.setDate('fromDate', criteria.createStartDate)
        }
        if (criteria.createEndDate != null) {
            query.setDate('toDate', criteria.createEndDate)
        }
        return query.list()
    }
}