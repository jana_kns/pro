<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE beans PUBLIC "-//SPRING//DTD BEAN//EN"
    "http://www.springframework.org/dtd/spring-beans.dtd">

<beans>

	<!-- ======================== FILTER CHAIN ======================= -->
	<bean id="filterChainProxy" class="org.acegisecurity.util.FilterChainProxy">
		<property name="filterInvocationDefinitionSource">
			<value>
				PATTERN_TYPE_APACHE_ANT
				/skins/**=#NONE#
				/**=httpSessionContextIntegrationFilter,logoutFilter,authenticationProcessingFilter,securityContextHolderAwareRequestFilter,rememberMeProcessingFilter,anonymousProcessingFilter,exceptionTranslationFilter,filterSecurityInterceptor
			</value>
			<!-- Put channelProcessingFilter before remoteUserFilter to turn on SSL switching -->
			<!-- It's off by default b/c Canoo WebTest doesn't support SSL out-of-the-box -->
		</property>
	</bean>

	<!-- ======================== AUTHENTICATION ======================= -->

	<!-- Note the order that entries are placed against the objectDefinitionSource is critical.
		The FilterSecurityInterceptor will work from the top of the list down to the FIRST pattern that matches the request URL.
		Accordingly, you should place MOST SPECIFIC (ie a/b/c/d.*) expressions first, with LEAST SPECIFIC (ie a/.*) expressions last -->
	<bean id="filterSecurityInterceptor" class="org.acegisecurity.intercept.web.FilterSecurityInterceptor">
		<property name="authenticationManager" ref="authenticationManager" />
		<property name="accessDecisionManager" ref="accessDecisionManager" />
		<property name="objectDefinitionSource">
			<value>
				CONVERT_URL_TO_LOWERCASE_BEFORE_COMPARISON
				PATTERN_TYPE_APACHE_ANT
                /faces/pages/login.xhtml=ROLE_ANONYMOUS,SuperUser,MtnceUser,User
                /faces/pages/workorder/editwo.xhtml=SuperUser,MtnceUser
                /faces/pages/admin/user/edituser.xhtml=SuperUser,MtnceUser,User
                /faces/pages/admin/**=SuperUser
                /faces/pages/**=SuperUser,MtnceUser,User
			</value>
		</property>
	</bean>

	<bean id="authenticationManager" class="org.acegisecurity.providers.ProviderManager">
		<property name="providers">
			<list>
				<ref local="daoAuthenticationProvider" />
				<ref local="anonymousAuthenticationProvider" />
				<ref local="rememberMeAuthenticationProvider" />
			</list>
		</property>
	</bean>

	<!-- Log failed authentication attempts to commons-logging -->
	<bean id="loggerListener" class="org.acegisecurity.event.authentication.LoggerListener" />

	<bean id="daoAuthenticationProvider" class="org.acegisecurity.providers.dao.DaoAuthenticationProvider">
		<!-- <property name="userDetailsService" ref="userDAO" /> -->
		<property name="userDetailsService">
			<!--<ref bean="inMemoryDaoImpl" />-->
            <ref bean="authenticationDao" />
		</property>
		<property name="userCache" ref="userCache" />
	</bean>

	<!-- CACHE -->
	<bean id="cacheManager" class="org.springframework.cache.ehcache.EhCacheManagerFactoryBean">
		<property name="configLocation">
			<value>classpath:ehcache.xml</value>
		</property>
	</bean>

	<bean id="userCacheBackend" class="org.springframework.cache.ehcache.EhCacheFactoryBean">
		<property name="cacheManager">
			<ref local="cacheManager" />
		</property>
		<property name="cacheName">
			<value>userCache</value>
		</property>
	</bean>

	<bean id="userCache" class="org.acegisecurity.providers.dao.cache.EhCacheBasedUserCache">
		<property name="cache">
			<ref local="userCacheBackend" />
		</property>
	</bean>

	<bean id="anonymousAuthenticationProvider" class="org.acegisecurity.providers.anonymous.AnonymousAuthenticationProvider">
		<property name="key" value="anonymous" />
	</bean>

	<bean id="roleVoter" class="org.acegisecurity.vote.RoleVoter">
		<property name="rolePrefix" value="" />
	</bean>

	<bean id="accessDecisionManager" class="org.acegisecurity.vote.AffirmativeBased">
		<property name="allowIfAllAbstainDecisions" value="false" />
		<property name="decisionVoters">
			<list>
				<ref local="roleVoter" />
			</list>
		</property>
	</bean>

	<!-- ===================== HTTP REQUEST SECURITY ==================== -->
	<bean id="httpSessionContextIntegrationFilter" class="org.acegisecurity.context.HttpSessionContextIntegrationFilter" />

	<bean id="authenticationProcessingFilter" class="org.acegisecurity.ui.webapp.AuthenticationProcessingFilter">
		<property name="authenticationManager">
			<ref bean="authenticationManager" />
		</property>
		<property name="authenticationFailureUrl" value="/faces/pages/login.xhtml?error=true" />
		<property name="defaultTargetUrl" value="/faces/pages/home.xhtml" />
		<property name="filterProcessesUrl" value="/j_acegi_security_check.jsf" />
		<property name="rememberMeServices" ref="rememberMeServices" />
	</bean>

	<bean id="anonymousProcessingFilter" class="org.acegisecurity.providers.anonymous.AnonymousProcessingFilter">
		<property name="key" value="anonymous" />
		<property name="userAttribute" value="anonymous,ROLE_ANONYMOUS" />
	</bean>

	<bean id="exceptionTranslationFilter" class="org.acegisecurity.ui.ExceptionTranslationFilter">
		<property name="authenticationEntryPoint">
			<bean class="org.acegisecurity.ui.webapp.AuthenticationProcessingFilterEntryPoint">
				<property name="loginFormUrl">
					<value>/faces/pages/login.xhtml</value>
				</property>
				<property name="forceHttps">
					<value>false</value>
				</property>
			</bean>
		</property>
		<property name="accessDeniedHandler">
			<bean class="org.acegisecurity.ui.AccessDeniedHandlerImpl">
				<property name="errorPage">
					<value>/faces/pages/accessdenied.xhtml</value>
				</property>
			</bean>
		</property>
	</bean>

	<!-- <bean id="securityEnforcementFilter"
		class="org.acegisecurity.intercept.web.SecurityEnforcementFilter">
		<property name="filterSecurityInterceptor"
		ref="filterSecurityInterceptor" />
		<property name="authenticationEntryPoint"
		ref="authenticationProcessingFilterEntryPoint" />
		</bean> -->

	<bean id="securityContextHolderAwareRequestFilter" class="org.acegisecurity.wrapper.SecurityContextHolderAwareRequestFilter" />

	<bean id="authenticationProcessingFilterEntryPoint" class="org.acegisecurity.ui.webapp.AuthenticationProcessingFilterEntryPoint">
		<property name="loginFormUrl" value="/login.html" />
		<property name="forceHttps" value="false" />
	</bean>

	<!-- ===================== REMEMBER ME ==================== -->
	<!-- remember me processing filter -->
	<bean id="rememberMeProcessingFilter" class="org.acegisecurity.ui.rememberme.RememberMeProcessingFilter">
		<property name="rememberMeServices" ref="rememberMeServices" />
		<property name="authenticationManager" ref="authenticationManager" />
	</bean>

	<bean id="rememberMeServices" class="org.acegisecurity.ui.rememberme.TokenBasedRememberMeServices">
        <property name="userDetailsService">
            <!--<ref local="inMemoryDaoImpl" />-->
            <ref local="authenticationDao" />
        </property>
        <property name="key" value="someTokenName" />
	</bean>

	<bean id="rememberMeAuthenticationProvider" class="org.acegisecurity.providers.rememberme.RememberMeAuthenticationProvider">
		<property name="key" value="someTokenName" />
	</bean>

	<bean id="logoutFilter" class="org.acegisecurity.ui.logout.LogoutFilter">
		<constructor-arg index="0" value="/index.html" />
		<constructor-arg index="1">
			<list>
				<ref local="securityContextLogoutHandler" />
				<ref local="rememberMeServices" />
			</list>
		</constructor-arg>
		<property name="filterProcessesUrl">
			<value>/j_acegi_logout.jsf</value>
		</property>
	</bean>

	<bean id="securityContextLogoutHandler" class="org.acegisecurity.ui.logout.SecurityContextLogoutHandler"></bean>

	<!-- Method Security -->
	<bean id="securityInterceptor" class="org.acegisecurity.intercept.method.aopalliance.MethodSecurityInterceptor">
        <property name="validateConfigAttributes">
        	<value>false</value>
        </property>
        <property name="authenticationManager">
        	<ref local="authenticationManager" />
        </property>
        <property name="accessDecisionManager">
        	<ref local="accessDecisionManager" />
        </property>
        <property name="objectDefinitionSource">
        	<ref local="objectDefinitionSource" />
        </property>
     </bean>

	<bean id="objectDefinitionSource" class="org.acegisecurity.intercept.method.MethodDefinitionAttributes">
        <property name="attributes">
        	<ref local="attributes" />
        </property>
    </bean>

	<bean id="attributes" class="org.acegisecurity.annotation.SecurityAnnotationAttributes" />
        <bean id="autoproxy" class="org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator">
	</bean>

    <bean id="methodSecurityAdvisor" class="org.acegisecurity.intercept.method.aopalliance.MethodDefinitionSourceAdvisor"
        autowire="constructor">
	</bean>

	<!-- ===================== SSL SWITCHING ==================== -->
	<bean id="channelProcessingFilter" class="org.acegisecurity.securechannel.ChannelProcessingFilter">
		<property name="channelDecisionManager" ref="channelDecisionManager" />
		<property name="filterInvocationDefinitionSource">
			<value>
				PATTERN_TYPE_APACHE_ANT
				/admin/**=REQUIRES_SECURE_CHANNEL
				/login*=REQUIRES_SECURE_CHANNEL
				/j_security_check*=REQUIRES_SECURE_CHANNEL
				/editProfile.html*=REQUIRES_SECURE_CHANNEL
				/signup.html*=REQUIRES_SECURE_CHANNEL
				/saveUser.html*=REQUIRES_SECURE_CHANNEL
				/**=REQUIRES_INSECURE_CHANNEL
			</value>
		</property>
	</bean>

	<bean id="channelDecisionManager" class="org.acegisecurity.securechannel.ChannelDecisionManagerImpl">
		<property name="channelProcessors">
			<list>
				<bean
					class="org.acegisecurity.securechannel.SecureChannelProcessor" />
				<bean
					class="org.acegisecurity.securechannel.InsecureChannelProcessor" />
			</list>
		</property>
	</bean>

	<bean id="inMemoryDaoImpl" class="org.acegisecurity.userdetails.memory.InMemoryDaoImpl">
		<property name="userMap">
			<value>
				jdolinski=jdolinski,ROLE_SUPER
                amiller=amiller,ROLE_MTNCE
                cguinn=cguinn,ROLE_USER
                disable=disable,disabled,ROLE_USER
			</value>
		</property>
	</bean>

    <bean id="authenticationDao" class="org.acegisecurity.userdetails.jdbc.JdbcDaoImpl">
        <property name="dataSource">
            <ref bean="dataSource"/>
        </property>
        <property name="usersByUsernameQuery">
            <value>select USERNAME,
                          PASSWORD,
                          ENABLED as ENABLED
                   from mtn001.users
                   where USERNAME=?</value>
        </property>
        <property name="authoritiesByUsernameQuery">
            <value>select USERNAME,
                          ROLE_NAME as authority
                   from mtn001.users myuser,
                   mtn001.roles myroles
                   where myuser.ROLE_ID=myroles.ROLE_ID
                   and myuser.USERNAME = ?</value>
        </property>
    </bean>
</beans>
