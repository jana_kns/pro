package org.dotcomm.services;

import org.dotcomm.pagecode.rows.*;
import java.util.List;

/**
 * Defines the business services for administrating security for the
 * application.
 *
 * @author JDolinski
 *
 */
public interface AdminSecurityService {

    /**
     * Get all the departments in the system.
     *
     * @return list of departments
     */
    List<DeptRow> getAllDepartments();

    /**
     * Get all the active departments in the system.
     *
     * @return list of departments
     */
    List<DeptRow> getAllActiveDepartments();

    /**
     * Add a new message.
     * @param deptRow object
     */
    void addNewDept(DeptRow deptRow);

    /**
     * Update an existing department.
     * @param deptRow object
     */
    void updateDept(DeptRow deptRow);

    /**
     * Get a specific department in the system.
     * @param id of the record
     * @return department A single message object
     */
    DeptRow getDept(Integer id);

    /**
     * Get all the locations in the system.
     *
     * @return list of locations
     */
    List<LocationRow> getAllLocations();

    /**
     * Get all the active locations in the system. A user with the "user" role
     * is only able to see locations that belong to their department.
     *
     * @return list of locations
     */
    List<LocationRow> getAllActiveLocations();

    /**
     * Add a new location.
     * @param locationRow object
     */
    void addNewLocation(LocationRow locationRow);

    /**
     * Update an existing location.
     * @param locationRow object
     */
    void updateLocation(LocationRow locationRow);

    /**
     * Get a specific user in the system.
     * @param id of the record
     * @return location A single message object
     */
    LocationRow getLocation(Integer id);

    /**
     * Get all the users in the system.
     *
     * @return list of users
     */
    List<UserRow> getAllUsers();

    /**
     * Get all the active users that have a maintenace role in the system.
     *
     * @return list of users
     */
    List<UserRow> getAllActiveMtnceUsers();

    /**
     * Add a new user.
     * @param userRow object
     */
    void addNewUser(UserRow userRow);

    /**
     * Update an existing user.
     * @param userRow object
     */
    void updateUser(UserRow userRow);

    /**
     * Get a specific user in the system.
     * @param id of the record
     * @return user A single message object
     */
    UserRow getUser(Integer id);

    /**
     * Get a specific user in the system.
     * @param username The user name
     * @return user A single message object
     */
    UserRow getUser(String username);

    /**
     * Get the user logged in to the system.
     * @return user A single message object
     */
    UserRow getUserLoggedIn();


    /**
     * Get all the roles in the system.
     *
     * @return list of users
     */
    List<RoleRow> getAllRoles();

    /**
     * Get all the maintenance groups in the system.
     *
     * @return list of maintenance groups
     */
    List<MtnceGroupRow> getAllMtnceGroups();

    /**
     * Get all the active maintenance groups in the system.
     *
     * @return list of maintenance groups
     */
    List<MtnceGroupRow> getAllActiveMtnceGroups();

    /**
     * Add a new maintenance group.
     * @param mtnceGroupRow object
     */
    void addNewMtnceGroup(MtnceGroupRow mtnceGroupRow);

    /**
     * Update an existing maintenance group.
     * @param mtnceGroupRow object
     */
    void updateMtnceGroup(MtnceGroupRow mtnceGroupRow);

    /**
     * Get a specific maintenance group in the system.
     * @param id of the record
     * @return maintenance group A single message object
     */
    MtnceGroupRow getMtnceGroup(Integer id);

}
