package org.dotcomm.dto.propertymaintenance;

/**
 * DTO for the Location table.
 *
 * @author jdolinski
 */
class Location {

	Integer locationId
    Date createDateTime
    Collection workOrder
	Collection mtnceGroupMany
	Departments deptId
	Date lastUpdatedDateTime
	String name
    String descr
    Integer enabled

}
