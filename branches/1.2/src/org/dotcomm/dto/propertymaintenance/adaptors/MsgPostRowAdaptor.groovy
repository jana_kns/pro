package org.dotcomm.dto.propertymaintenance.adaptors

import org.dotcomm.util.Adaptor
import org.dotcomm.pagecode.rows.MsgPostRow
import org.dotcomm.dto.propertymaintenance.MsgPost
import org.dotcomm.persistence.DataAccessObject

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class MsgPostRowAdaptor implements Adaptor {

    /**
     * Adapt persistant dto for view.
     * @return object for view
     */
    public Object adapt(Object obj) {
        MsgPostRow msgPostRow = new MsgPostRow()
        MsgPost msgPost = (MsgPost) obj
        if (msgPost != null) {
            msgPostRow.msgId = msgPost.msgPostId
            msgPostRow.msgTitle = msgPost.msgTitle
            msgPostRow.msg = msgPost.msg
            msgPostRow.msgDate = msgPost.lastUpdatedDateTime
            msgPostRow.updBy = msgPost.enteredBy
        }
        else {
            return null
        }
        return msgPostRow
    }

    public Class<MsgPost> getFromType() {
        return MsgPost
    }

    public Class<MsgPostRow> getToType() {
        return MsgPostRow
    }

    /**
     * Merge object for view to persistant dto.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        MsgPost msgPost = new MsgPost()
        MsgPostRow msgPostRow = (MsgPostRow) obj
        if (msgPostRow != null) {
            msgPost.msgPostId = msgPostRow.msgId
            msgPost.msgTitle = msgPostRow.msgTitle
            msgPost.msg = msgPostRow.msg
            msgPost.lastUpdatedDateTime = msgPostRow.msgDate
            msgPost.enteredBy = msgPostRow.updBy
        }
        else {
            return null
        }
        return msgPost
    }

}