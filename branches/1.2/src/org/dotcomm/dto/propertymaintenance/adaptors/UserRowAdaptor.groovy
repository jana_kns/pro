package org.dotcomm.dto.propertymaintenance.adaptors

import org.dotcomm.dto.propertymaintenance.Users
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.dto.propertymaintenance.Departments
import org.dotcomm.dto.propertymaintenance.Roles
import org.dotcomm.util.FacesUtils
import org.dotcomm.util.Adaptor
import org.dotcomm.dto.propertymaintenance.MtnceGroup
import org.dotcomm.persistence.DataAccessObject
import org.dotcomm.pagecode.rows.MtnceGroupRow

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class UserRowAdaptor implements Adaptor {

    /** Injected Property */
    DataAccessObject dao

    /**
     * Adapt persistant dto for view.
     * @return object for view
     */
    public Object adapt(Object obj) {
        UserRow userRow = new UserRow()
        Users users = (Users) obj
        if (users != null) {
            userRow.userId = users.userId
            userRow.lastUpd = users.lastUpdatedDateTime
            userRow.fname = users.fname
            userRow.lname = users.lname
            userRow.uname = users.username
            userRow.pwd = users.password
            userRow.fullName = "$users.fname $users.lname"
            userRow.phone = users.phone
            userRow.email = users.email
            userRow.dept = users.deptId.deptName
            userRow.deptId = users.deptId.deptId
            userRow.role = users.roleId.roleName
            userRow.roleId = users.roleId.roleId
            userRow.userStatus = users.enabled.intValue() == 1 ? 'Active' : 'Inactive'
            userRow.active = users.enabled.intValue() == 1 ? new Boolean(true) : new Boolean(false)
            userRow.listMtnceGroups = new ArrayList<MtnceGroupRow>()
            for (MtnceGroup mg : users.mtnceGroupMany) {
                userRow.listMtnceGroups.add(new MtnceGroupRow(groupId:mg.mtnceGroupId, groupName:mg.name))
            }
        }
        else {
            return null
        }
        return userRow
    }

    public Class<Users> getFromType() {
        return Users
    }

    public Class<UserRow> getToType() {
        return UserRow
    }

    /**
     * Merge object for view to persistant dto.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        Users users = new Users()
        UserRow userRow = (UserRow) obj
        if (userRow != null) {
            users.userId = userRow.userId
            users.lastUpdatedDateTime = userRow.lastUpd
            users.fname = userRow.fname
            users.lname = userRow.lname
            users.username = userRow.uname
            users.password = userRow.pwd
            users.phone = userRow.phone
            users.email = userRow.email
            users.deptId = new Departments(deptId:userRow.deptId)
            users.roleId = new Roles(roleId:userRow.roleId)
            users.enabled = userRow.active ? new Integer(1) : new Integer(0)
            users.mtnceGroupMany = new ArrayList<MtnceGroup>()
            for (MtnceGroupRow row : userRow.listMtnceGroups) {
                //load() method always tries to return a proxy and only returns
                //an initialized instance if it is already managed in the current persistence context.
                //A get will always hit the database. Since we are just adding a item to a collection
                //we do not need to hit the db and get the value, the proxy will work just fine.
                users.mtnceGroupMany.add(dao.load(MtnceGroup.class, row.groupId))
            }
        }
        else {
            return null
        }
        return users
    }

}