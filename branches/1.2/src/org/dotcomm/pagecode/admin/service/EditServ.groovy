package org.dotcomm.pagecode.admin.service

import org.dotcomm.pagecode.rows.ServiceGroupRow
import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.rows.ServiceGroupItemRow
import org.dotcomm.pagecode.compositions.ServForm
import org.dotcomm.pagecode.admin.AdminMain;

/**
 * Jsf managed bean for the editserv.xhtml.
 *
 * @author jdolinski
 */
class EditServ extends ServForm {

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** Page code data */
    ServiceGroupRow serviceGroupRow

    /** Constructor */
    EditServ(AdminWorkOrderService adminWorkOrderService) {
        this.adminWorkOrderService = adminWorkOrderService
        Integer servId = (Integer)FacesUtils.getPageFlowScopeParameter("sergroupid")
        serviceGroupRow = this.adminWorkOrderService.getServiceGroup(servId)
        if (FacesUtils.getPageFlowScopeParameter("ServFormListItems") == null) {
            FacesUtils.putPageFlowScopeParameter "ServFormListItems", serviceGroupRow.listItems
        }
    }

    /**
     * Action method to edit an existing service group in the system.
     * @return Faces navigation text.
     */
    String updateServ() {
        List<ServiceGroupItemRow> listItems = FacesUtils.getPageFlowScopeParameter ("ServFormListItems")
        serviceGroupRow.listItems = listItems
        if (!validate()) {
            adminWorkOrderService.updateServiceGroup (serviceGroupRow)
        }
        else {
            return null
        }
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

}
