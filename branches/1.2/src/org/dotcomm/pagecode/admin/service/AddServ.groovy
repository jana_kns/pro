package org.dotcomm.pagecode.admin.service

import org.dotcomm.pagecode.rows.ServiceGroupRow
import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.pagecode.rows.ServiceGroupItemRow
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.compositions.ServForm
import org.dotcomm.pagecode.admin.AdminMain;

/**
 * Jsf managed bean for the AddServ.xhtml.
 *
 * @author jdolinski
 */
class AddServ extends ServForm {

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** Page code data */
    ServiceGroupRow serviceGroupRow

    /** Constructor */
    AddServ() {
        super()
        serviceGroupRow = new ServiceGroupRow()
        serviceGroupRow.active = new Boolean(true)
    }

    /**
     * Action method to add a new service group to the system.
     * @return Faces navigation text.
     */
    String newServGroup() {
        List<ServiceGroupItemRow> listItems = FacesUtils.getPageFlowScopeParameter ("ServFormListItems")
        serviceGroupRow.listItems = listItems
        if (!validate()) {
            adminWorkOrderService.addNewServiceGroup(serviceGroupRow)
        }
        else {
            return null
        }
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

}
