package org.dotcomm.pagecode.admin.user

import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.admin.AdminMain

/**
 * Jsf managed bean for the adduser.xhtml.
 *
 * @author jdolinski
 */
class AddUser {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Page code data */
    UserRow userRow

    /** Constructor */
    AddUser() {
        userRow = new UserRow()
        userRow.active = new Boolean(true)
        //if we are adding a new user from the dept page, default the department.
        if (FacesUtils.getPageFlowScopeParameter('addUserFromDept') != null) {
            userRow.deptId = (Integer) FacesUtils.getPageFlowScopeParameter('addUserFromDept')
            FacesUtils.removePageFlowScopeParameter 'addUserFromDept'
        }
    }

    /**
     * Action method to add a new user to the system.
     * @return Faces navigation text.
     */
    String newUser() {
        adminSecurityService.addNewUser (userRow)
        String navText = (String)FacesUtils.getPageFlowScopeParameter("addusernav")
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return navText
    }

    /**
     * Action method for the cancel button.
     * @return Faces navigation text.
     */
    String cancel() {
        String navText = (String)FacesUtils.getPageFlowScopeParameter("addusernav")
        return navText
    }
}
