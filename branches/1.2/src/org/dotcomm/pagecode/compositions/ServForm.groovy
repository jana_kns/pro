package org.dotcomm.pagecode.compositions

import org.dotcomm.pagecode.rows.ServiceGroupItemRow
import javax.faces.event.ActionEvent
import org.dotcomm.util.FacesUtils
import org.apache.myfaces.trinidad.component.core.output.CoreOutputText
import org.apache.myfaces.trinidad.component.UIXInput

/**
 * Facelet compostion backing bean for the servform.xhtml.
 *
 * @author JDolinski
 */
class ServForm {

    /** Binding for removing the message row after delete */
    CoreOutputText bindItemRow    

    /**
     * Action event to add a new item to the service group.
     * The previous rows are marked as read only as trinidad does not correctly
     * delete the row unless the content is output fields only. 
     * @param Action event
     */
    void addItemRow(ActionEvent event) {
        List<ServiceGroupItemRow> listItems = FacesUtils.getPageFlowScopeParameter ("ServFormListItems")
        if (!validateItems()) {
            if (listItems == null || listItems.size() == 0) {
                listItems = new ArrayList<ServiceGroupItemRow>()
                listItems.add(new ServiceGroupItemRow(readOnly:false,active:new Boolean(true)))
            }
            else {
                ServiceGroupItemRow previous = (ServiceGroupItemRow) listItems.get(listItems.size() - 1)
                if (!previous.dbStored) {
                    previous.readOnly = true
                }
                listItems.add(new ServiceGroupItemRow(readOnly:false,active:new Boolean(true)))
            }
        }
        FacesUtils.putPageFlowScopeParameter "ServFormListItems", listItems
    }

    /**
     * Action method to delete a service item that has yet to be added to the database.
     * @return Faces navigation text.
     */
    void deleteItem() {
        List<ServiceGroupItemRow> listItems = FacesUtils.getPageFlowScopeParameter ("ServFormListItems")
        listItems.remove((ServiceGroupItemRow)bindItemRow.value)
        FacesUtils.putPageFlowScopeParameter "ServFormListItems", listItems
    }

    /**
     * Server side required validation used to allow adding a service item without being prompted
     * to fill in the name of the service group. This is caused by using required=true on the name.
     * The problem was not easily fixed by using immediate because jsf does not update the model
     * when using immediate. Thus, data is lost in the table between validations.
     * @return true if error, else false
     */
    boolean validate() {
        boolean validationError = false
        UIXInput servName = (UIXInput)FacesUtils.findComponentInRoot("name")
        if (servName == null || servName.getValue() == null || ((String)servName.getValue()).trim().length() < 1) {
            validationError = true
            FacesUtils.addErrorMessage 'form:name', FacesUtils.getStringFromBundle('valuereq')
        }
        if (validateItems()) {
            validationError = true
        }
        return validationError
    }

    /**
     * Server side validation for the required fields within the service item table.
     * See above for further details.
     * @return true if error, else false
     */
    boolean validateItems() {
        boolean validationError = false
        List<ServiceGroupItemRow> listItems = FacesUtils.getPageFlowScopeParameter ("ServFormListItems")
        int idx = 0
        for (ServiceGroupItemRow row : listItems) {
            if (row.name == null || row.name.trim().length() < 1) {
                validationError = true
                FacesUtils.addErrorMessage "form:servItemTable:$idx:servItem", FacesUtils.getStringFromBundle('valuereq')
            }
            idx++
        }
        return validationError
    }

}