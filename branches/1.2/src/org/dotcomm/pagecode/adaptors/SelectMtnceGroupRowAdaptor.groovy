package org.dotcomm.pagecode.adaptors

import org.dotcomm.util.Adaptor
import org.dotcomm.pagecode.rows.MtnceGroupRow
import javax.faces.model.SelectItem

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class SelectMtnceGroupRowAdaptor implements Adaptor {

    /**
     * Adapt a list of view rows to selection items.
     * @return list of select items
     */
    public Object adapt(Object obj) {
        List<SelectItem> listSelectItems = new ArrayList<SelectItem>()
        List<MtnceGroupRow> listMtnceGroupRows = (ArrayList<MtnceGroupRow>) obj
        for (MtnceGroupRow r : listMtnceGroupRows) {
            listSelectItems.add(new SelectItem(r.groupId, r.groupName, r.groupDesc))
        }
        return listSelectItems.sort {SelectItem it -> it.label}
    }

    public Class<ArrayList<MtnceGroupRow>> getFromType() {
        return ArrayList
    }

    public Class<ArrayList<SelectItem>> getToType() {
        return ArrayList
    }

    /**
     * Merge object from list of select items to list of view rows.
     * @return list view row
     */
    public Object merge(Object obj) {
        List<MtnceGroupRow> listWoStatusRows = new ArrayList<MtnceGroupRow>()
        List<SelectItem> listSelectItems = (ArrayList<SelectItem>) obj
        for (SelectItem i : listSelectItems) {
            listWoStatusRows.add(new MtnceGroupRow(groupId:(Integer)i.value, groupName:i.getLabel(), groupDescr:i.description))
        }
        return listWoStatusRows
    }

}