package org.dotcomm.pagecode.adaptors

import org.dotcomm.util.Adaptor
import javax.faces.model.SelectItem
import org.dotcomm.pagecode.rows.UserRow

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class SelectUserRowAdaptor implements Adaptor {

    /**
     * Adapt a list of view rows to selection items.
     * @return list of select items
     */
    public Object adapt(Object obj) {
        List<SelectItem> listSelectItems = new ArrayList<SelectItem>()
        List<UserRow> listUserRows = (ArrayList<UserRow>) obj
        for (UserRow u : listUserRows) {
            listSelectItems.add(new SelectItem(u.userId, u.fullName))
        }
        return listSelectItems.sort{SelectItem it -> it.label}  
    }

    public Class<ArrayList<UserRow>> getFromType() {
        return ArrayList
    }

    public Class<ArrayList<SelectItem>> getToType() {
        return ArrayList
    }

    /**
     * Merge object from list of select items to list of view rows.
     * @return list view row
     */
    public Object merge(Object obj) {
        List<UserRow> listUserRows = new ArrayList<UserRow>()
        List<SelectItem> listSelectItems = (ArrayList<SelectItem>) obj
        for (SelectItem i : listSelectItems) {
            listUserRows.add(new UserRow(userId:(Integer)i.value, fullName:i.label))
        }
        return listUserRows
    }
    
}