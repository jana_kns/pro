package org.dotcomm.pagecode.rows

/**
 * Page code bean for displaying location information.
 *
 * @author jdolinski
 */
class LocationRow {

    Integer locId
    Date lastUpd
    String locName
    String locDesc
    String locStatus
    String locDept
    Integer locDeptId
    Boolean locActive
    List<MtnceGroupRow> listMtnceGroups
    
}