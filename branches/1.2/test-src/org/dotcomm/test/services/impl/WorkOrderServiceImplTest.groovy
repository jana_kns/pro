package org.dotcomm.test.services.impl

import org.dotcomm.services.impl.WorkOrderServiceImpl
import org.junit.Test
import org.dotcomm.pagecode.rows.ChartRow
import org.dotcomm.dao.WorkOrderDao
import org.dotcomm.pagecode.rows.WoStatusRow
import org.dotcomm.persistence.DataAccessObject
import org.dotcomm.dto.propertymaintenance.WoStatus
import org.dotcomm.dto.propertymaintenance.adaptors.WoStatusRowAdaptor
import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.dto.propertymaintenance.adaptors.WorkOrderRowAdaptor
import org.dotcomm.dto.propertymaintenance.Users
import org.dotcomm.dto.propertymaintenance.WorkOrder
import org.dotcomm.dto.propertymaintenance.Archive
import org.dotcomm.dto.propertymaintenance.WoType
import org.dotcomm.dto.propertymaintenance.Location
import org.dotcomm.dto.propertymaintenance.Departments
import org.dotcomm.util.SecurityUtils
import org.dotcomm.dto.propertymaintenance.ServiceItem
import org.dotcomm.dto.propertymaintenance.ServiceGroup

/**
 * Junit test cases for the WorkOrderServiceImpl
 * @author jdolinski
 */
class WorkOrderServiceImplTest extends GroovyTestCase {

    WorkOrderServiceImpl workOrderService
    Departments mockDept
    Location mockLocation
    WorkOrder mockWorkOrder
    WoStatus mockWoStatus
    Archive mockArchive
    WoType mockWoType
    Users mockUser

    protected void setUp() {
        workOrderService = new WorkOrderServiceImpl()
        workOrderService.workOrderRowAdaptor = new WorkOrderRowAdaptor()
        workOrderService.woStatusRowAdaptor = new WoStatusRowAdaptor()
        mockWoStatus = new WoStatus(statusId:1,name:"test active",descr:"test descr",enabled:1)
        mockArchive = new Archive(archiveId:1)
        mockWoType = new WoType(woTypeId:1)
        mockDept = new Departments(deptId:1)
        mockLocation = new Location(locationId:1,deptId:mockDept)
        mockUser = new Users(userId:1)
        mockWorkOrder = new WorkOrder(workOrderId:1,archiveId:mockArchive,createDateTime:new Date(),statusId:mockWoStatus,locationId:mockLocation,woTypeId:mockWoType,userId:mockUser,lastUpdatedDateTime:new Date())
    }

    @Test
    void testGetAllActiveWorkOrderStatuses() {
        List data = [mockWoStatus]
        def dao = [
                getObjectsByExample:{o1 ->
                    data}] as DataAccessObject
        workOrderService.dao = dao
        List<WoStatusRow> list = workOrderService.getAllActiveWorkOrderStatuses()
        list.eachWithIndex {WoStatusRow w, int count ->
            WoStatus woStatus = (WoStatus)data[count]
            assert w.woStatusName == woStatus.name
            assert w.woStatusId == woStatus.statusId
            assert w.woStatusDescr == woStatus.descr
            assert w.woStatusEnabledDescr == 'Active'
            assert w.woStatusEnabled == true
        }
        assert list.size() == 1
    }

    @Test
    void testGetAllWoStatusItems() {
        List data = [mockWoStatus, mockWoStatus]
        def dao = [
                getAllObjects:{
                    data}] as DataAccessObject
        workOrderService.dao = dao
        List<WoStatusRow> list = workOrderService.getAllWoStatusItems()
        list.eachWithIndex {WoStatusRow w, int count ->
            WoStatus woStatus = (WoStatus)data[count]
            assert w.woStatusName == woStatus.name
            assert w.woStatusId == woStatus.statusId
            assert w.woStatusDescr == woStatus.descr
        }
        assert list.size() == 2
    }

    @Test
    void testAddNewWorkOrder() {
        Users user = new Users(username:'junit')
        def dao = [
                getObjectById:{o1, o2 ->
                    return user
                },
                load:{o1, o2 ->
                    if (o1 == WoType) {
                        return new WoType(woTypeId:1)
                    }
                    else {
                        return new Location(locationId:1)
                    }
                },
                saveAndReturn:{o1 ->
                    o1}] as DataAccessObject
        def securityUtils = [
                getUserIdLoggedIn:{
                    1
                }] as SecurityUtils
        WorkOrderRow workOrderRow = new WorkOrderRow(woId:1,woReqComments:'Testing New Work Order')
        workOrderService.dao = dao
        workOrderService.securityUtils = securityUtils
        assert 1 == workOrderService.addNewWorkOrder(workOrderRow)
    }

    @Test
    void testGetWorkOrder() {
        WorkOrder data = mockWorkOrder
        def dao = [
                getObjectById:{o1,o2 ->
                    data}] as DataAccessObject
        workOrderService.dao = dao
        WorkOrderRow value = workOrderService.getWorkOrder(new Integer(1))
        assert data.workOrderId == value.woId
        assert data.createDateTime == value.createDateTime
     }

    @Test
    void testUpdateWorkOrder() {
        def dao = [
                merge:{},
                load:{o1, o2 ->
                    if (o1 == WoType) {
                        mockWoType
                    }
                    else if (o1 == Location) {
                        mockLocation
                    }
                    else if (o1 == Users) {
                        mockUser
                    }
                },
                getObjectById:{o1, o2 ->
                    mockUser
                }] as DataAccessObject
        def securityUtils = [
                getUserIdLoggedIn:{
                    mockUser.userId
                }] as SecurityUtils
        WorkOrderRow workOrderRow = new WorkOrderRow(woId:1,woStatusId:mockWoStatus.statusId)
        workOrderService.dao = dao
        workOrderService.securityUtils = securityUtils
        workOrderService.updateWorkOrder(workOrderRow)
        assert true
    }

      @Test
      void testGetWorkOrderReportData() {
          List<WorkOrder> data = new ArrayList<WorkOrder>()
          data.add(mockWorkOrder)
          def dao = [
                  getObjectsByExample:{o1 ->
                      data}] as DataAccessObject
          workOrderService.dao = dao
          List list = workOrderService.getWorkOrderReportData()
          assertEquals "List should have one element!",1,list.size()
      }

      @Test
      void testGetServiceItemReportData() {
        List<WorkOrder> data = new ArrayList<WorkOrder>()
        data.add(mockWorkOrder)
        def dao = [
                getObjectsByExample:{o1 ->
                    data}] as DataAccessObject
        workOrderService.dao = dao
        List list = workOrderService.getServiceItemReportData()
        assertTrue "List should be empty, no service items!",list.isEmpty()
        mockWorkOrder.serviceItemMany = new ArrayList<ServiceItem>()
        mockWorkOrder.serviceItemMany.add(new ServiceItem(name:'junit item',serviceItemId:1,serviceGroupId:new ServiceGroup(name:"junit group")))
        list = workOrderService.getServiceItemReportData()
        assertEquals "List should have one element!",1,list.size()
        assertEquals "Service item names should match!",'junit group - junit item',((WorkOrderRow)list.iterator().next()).serviceItems
      }

    @Test
    void testGetWorkOrderStatusesInSystem() {
        List data = [[100, "status1"].toArray(), [99, "status2"].toArray()]
        def dao = [
                getWoStatusCounts:{
                    data}] as WorkOrderDao
        workOrderService.workOrderDao = dao
        ChartRow c = workOrderService.getWorkOrderStatusesInSystem()
        c.groupLabels.eachWithIndex { String label, int count ->
            assert label == data[count][1]
        }
        c.yValues.eachWithIndex {List values, int count ->
            assert values.get(0) == data[count][0]
        }
    }

    @Test
    void testGetNumberOfWorkOrdersForEachDept() {
        List data = [[6, "dept1"].toArray(), [10, "dept2"].toArray()]
        def dao = [
                getDeptCounts:{
                    data}] as WorkOrderDao
        workOrderService.workOrderDao = dao
        ChartRow c = workOrderService.getNumberOfWorkOrdersForEachDept()
        c.groupLabels.eachWithIndex { String label, int count ->
            assert label == data[count][1]
        }
        c.yValues.eachWithIndex {List values, int count ->
            assert values.get(0) == data[count][0]
        }
    }

    @Test
    void testGetNumberOfWorkOrdersServiceGroups() {
        List data = [[6, "serv1"].toArray(), [10, "serv2"].toArray()]
        def dao = [
                getServiceGroupCounts:{
                    data}] as WorkOrderDao
        workOrderService.workOrderDao = dao
        ChartRow c = workOrderService.getNumberOfWorkOrdersServiceGroups()
        c.groupLabels.eachWithIndex { String label, int count ->
            assert label == data[count][1]
        }
        c.yValues.eachWithIndex {List values, int count ->
            assert values.get(0) == data[count][0]
        }
    }

    @Test
    void testGetNumberOfWorkByType() {
        List data = [[3, "name"].toArray(), [6, "wathasdf"].toArray()]
        def dao = [
                getWoTypesCounts:{
                    data}] as WorkOrderDao
        workOrderService.workOrderDao = dao
        ChartRow c = workOrderService.getNumberOfWorkByType()
        c.groupLabels.eachWithIndex { String label, int count ->
            assert label == data[count][1]
        }
        c.yValues.eachWithIndex {List values, int count ->
            assert values.get(0) == data[count][0]
        }
    }

    @Test
    void testUpdateArchiveIndicators() {
        int numRecsUpdated = 5
        def dao = [
                updateArchiveInd:{o1 ->
                    numRecsUpdated}] as WorkOrderDao
        workOrderService.workOrderDao = dao
        workOrderService.updateArchiveIndicators()
        assert true
    }

    @Test
    void testPurgeWorkOrders() {
        List<WorkOrder> purgeList = new ArrayList<WorkOrder>()
        def dao = [
                completedWo:{o1 ->
                    purgeList}] as WorkOrderDao
        workOrderService.workOrderDao = dao
        workOrderService.purgeWorkOrders()
        assert true
    }

}