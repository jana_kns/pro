package org.dotcomm.test.base;

import java.io.FileInputStream;
import java.sql.Connection;
import javax.sql.DataSource;
import org.dotcomm.test.util.SpringContextUtil;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

public class BaseTestCase extends AbstractTransactionalDataSourceSpringContextTests {

	/** Location of the dbunit data file */
	//private static String TEST_DATA_FILE = "database/dbunit-test-data.xml";

	protected SessionFactory sessionFactory = null;

	protected String[] getConfigLocations() {
		return new String[] { SpringContextUtil
				.getApplicationContextDirectory()
				+ "applicationContext.xml" };
	}

	/**
	 * Spring will automatically inject the Hibernate session factory on startup
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * Load data for testing on setup. This data will be loaded on each
	 * test case accessed and removed when exiting the test case.
	 */
	/**protected void onSetUpInTransaction() throws Exception {
		logger.info("*** Inserting test data ***");
		// Use spring to get the datasource
		DataSource ds = this.jdbcTemplate.getDataSource();
		Connection conn = ds.getConnection();
		try {
			IDatabaseConnection connection = new DatabaseConnection(conn, "PRJ001");
			DatabaseOperation.INSERT.execute(connection, new FlatXmlDataSet(
					new FileInputStream(TEST_DATA_FILE)));
		} finally {
			DataSourceUtils.releaseConnection(conn, ds);
			logger.info("*** Finished inserting test data ***");
		}
	}

	protected void onTearDownInTransaction() throws Exception {
		// Commit or rollback the transaction
		endTransaction();
		
		// Delete the data
		DataSource ds = this.jdbcTemplate.getDataSource();
		Connection conn = ds.getConnection();
		try {
			IDatabaseConnection connection = new DatabaseConnection(conn, "PRJ001");
			DatabaseOperation.DELETE.execute(connection, new FlatXmlDataSet(
					new FileInputStream(TEST_DATA_FILE)));
		} finally {
			DataSourceUtils.releaseConnection(conn, ds);
			logger.info("*** Finished removing test data ***");
		}
	}*/

}
