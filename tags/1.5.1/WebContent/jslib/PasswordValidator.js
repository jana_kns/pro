/*
 * Apache Trinidad client side implementation for validating a phone number.
 * The implementation is used by org.dotcomm.pagecode.validators.PasswordValidator
 *
 * password must be 8 digits, one upper character and one lower character and at least one digit
 */

PasswordValidator = function() {}

PasswordValidator.prototype = new TrValidator();

PasswordValidator.prototype.validate = function (value, label, converter) {
    var facesMessage = new TrFacesMessage("Validation Error",
                                          "Password must be at least 8 digits, have at least one uppercase character, at least one lower case character, and at least one digit.",
                                          TrFacesMessage.SEVERITY_ERROR);

    var re = new RegExp("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$");

    if (!re.test(value)) {
        //todo get message from resource bundle, or inject
        throw new TrValidatorException(facesMessage);
    }
}