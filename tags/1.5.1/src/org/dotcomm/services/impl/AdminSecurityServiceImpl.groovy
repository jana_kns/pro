package org.dotcomm.services.impl

import org.dotcomm.persistence.DataAccessObject
import org.dotcomm.pagecode.rows.DeptRow
import org.dotcomm.dto.propertymaintenance.Departments
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.rows.LocationRow
import org.dotcomm.dto.propertymaintenance.Location
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.dto.propertymaintenance.Users
import org.dotcomm.pagecode.rows.MtnceGroupRow
import org.dotcomm.dto.propertymaintenance.MtnceGroup
import org.dotcomm.dto.propertymaintenance.adaptors.DeptRowAdaptor
import org.dotcomm.dto.propertymaintenance.adaptors.UserRowAdaptor
import org.dotcomm.pagecode.rows.RoleRow
import org.dotcomm.dto.propertymaintenance.Roles
import org.dotcomm.dto.propertymaintenance.adaptors.RoleRowAdaptor
import org.dotcomm.dto.propertymaintenance.adaptors.LocationRowAdaptor
import org.dotcomm.dto.propertymaintenance.adaptors.MtnceGroupRowAdaptor
import org.springframework.security.userdetails.UserDetails
import org.springframework.security.userdetails.UserDetailsService
import org.springframework.security.userdetails.UsernameNotFoundException
import org.springframework.security.GrantedAuthorityImpl
import org.springframework.security.GrantedAuthority
import org.dotcomm.util.SecurityUtils
import org.dotcomm.util.CustomUser

/**
 * The implementation for {@link AdminSecurityService}.
 *
 * @author JDolinski
 *
 */
class AdminSecurityServiceImpl implements AdminSecurityService, UserDetailsService {

    /** Injected Property */
    DataAccessObject dao

    /** Injected Property */
    SecurityUtils securityUtils

    /** Injected Property */
    DeptRowAdaptor deptRowAdaptor

    /** Injected Property */
    UserRowAdaptor userRowAdaptor

    /** Injected Property */
    RoleRowAdaptor roleRowAdaptor

    /** Injected Property */
    LocationRowAdaptor locationRowAdaptor

    /** Injected Property */
    MtnceGroupRowAdaptor mtnceGroupRowAdaptor

    /** {@inheritDoc} */
	public List<DeptRow> getAllDepartments() {
		List<DeptRow> list = new ArrayList<DeptRow>()
        List<Departments> listDepts = dao.getAllObjects(Departments.class)
        for (Departments d : listDepts) {
			list.add(deptRowAdaptor.adapt(d))
		}
        //remove inactive locations from the department's location list
        for (DeptRow deptRow : list) {
            for (Iterator it = deptRow.listLocations.iterator();it.hasNext();) {
                LocationRow locationRow = (LocationRow) it.next()
                if (!locationRow.locActive) {
                    it.remove()    
                }
            }
        }
        return list.sort {DeptRow it -> it.deptName}
	}

    /** {@inheritDoc} */
	public List<DeptRow> getAllActiveDepartments() {
		List<DeptRow> list = new ArrayList<DeptRow>()
        List<Departments> listDepts = dao.getObjectsByExample(new Departments(enabled:new Integer(1)))
        for (Departments d : listDepts) {
			list.add(deptRowAdaptor.adapt(d))
		}
		return list.sort {DeptRow it -> it.deptName}
	}

    /** {@inheritDoc} */
    void addNewDept(DeptRow deptRow) {
        Departments departments = deptRowAdaptor.merge(deptRow)
        Date theDate = new Date()
        departments.lastUpdatedDateTime = theDate
        departments.createDateTime = theDate
        dao.save(departments)
    }

    /** {@inheritDoc} */
    void updateDept(DeptRow deptRow) {
        Departments departments = deptRowAdaptor.merge(deptRow)
        for (Users u : departments.users) {
            //if the department is disabled, disable all users within the department
            //and if they belong to any maintenance groups, remove them.
            if (departments.enabled.intValue() == 0) {
                u.enabled = new Integer(0)
                u.mtnceGroupMany.clear()
            }
            u.deptId = departments
        }
        for (Location l : departments.location) {
            //if the department is disabled, disable all locations
            //and if they belong to any maintenance groups, remove them.
            if (departments.enabled.intValue() == 0) {
                l.enabled = new Integer(0)
                l.mtnceGroupMany.clear()
            }
            l.deptId = departments
        }
        if (departments.enabled.intValue() == 0) {
            //update all mtnce groups
            Date theDate = new Date()
            for (MtnceGroup mg : dao.getAllObjects(MtnceGroup.class)) {
                mg.lastUpdatedDateTime = theDate
            }
        }
        dao.merge(departments)
    }

    /** {@inheritDoc} */
    DeptRow getDept(Integer id) {
        Departments departments = (Departments)dao.getObjectById(id, Departments.class)
        DeptRow deptRow = deptRowAdaptor.adapt(departments)
        return deptRow
    }


    /** {@inheritDoc} */
    public List<LocationRow> getAllLocations() {
        List<LocationRow> list = new ArrayList<LocationRow>()
        List<Location> listLocs = dao.getAllObjects(Location.class)
        for (Location l : listLocs) {
            list.add(locationRowAdaptor.adapt(l))
        }
        return list.sort {LocationRow it -> it.locName}
    }

    /** {@inheritDoc} */
    public List<LocationRow> getAllActiveLocations() {
        List<LocationRow> list = new ArrayList<LocationRow>()
        Users user = (Users)dao.getObjectById(securityUtils.getUserIdLoggedIn(), Users.class)
        List<Location> listLocs
        if (user.roleId.roleId == 1) {
            if (user.locationId == null) { //location dropdown contains all locations for a dept, when no location specified
                listLocs = dao.getObjectsByExample(new Location(enabled:new Integer(1),deptId:user.deptId))
            } else { //location dropdown only contains default location
                listLocs = dao.getObjectsByExample(new Location(enabled:new Integer(1),locationId:user.locationId.locationId))
            }
        } else if (user.roleId.roleId == 2) { //location dropdown contains all locations for a dept
            listLocs = dao.getObjectsByExample(new Location(enabled:new Integer(1),deptId:user.deptId))
        } else { //location dropdown contains all active locations
           listLocs = dao.getObjectsByExample(new Location(enabled:new Integer(1)))
        }
        for (Location l : listLocs) {
            list.add(locationRowAdaptor.adapt(l))
        }
        return list.sort {LocationRow it -> it.locName}
    }

    /** {@inheritDoc} */
    public List<LocationRow> getAllActiveDeptLocations(Integer deptId) {
        List<LocationRow> list = new ArrayList<LocationRow>()
        Departments dept = (Departments)dao.getObjectById(deptId,Departments.class)
        List<Location> listLocs = dao.getObjectsByExample(new Location(enabled:new Integer(1),deptId:dept))
        for (Location l : listLocs) {
            list.add(locationRowAdaptor.adapt(l))
        }
        return list.sort {LocationRow it -> it.locName}
    }

    /** {@inheritDoc} */
    public List<UserRow> getAllUsers() {
        List<UserRow> list = new ArrayList<UserRow>()
        List<Users> listUsers = dao.getAllObjects(Users.class)
        for (Users u : listUsers) {
            list.add(userRowAdaptor.adapt(u))
        }
        return list.sort {UserRow it -> it.dept}
    }

    /** {@inheritDoc} */
    public List<UserRow> getAllActiveMtnceUsers() {
        List<UserRow> list = new ArrayList<UserRow>()
        List<Users> listUsers = dao.getObjectsByExample(new Users(enabled:new Integer(1)))
        for (Users u : listUsers) {
            if (u.roleId.roleId.intValue() == 3) { //Mtnce role
                list.add(userRowAdaptor.adapt(u))
            }
        }
        return list.sort {UserRow it -> it.fname + it.lname}
    }

    /** {@inheritDoc} */
    void addNewLocation(LocationRow locationRow) {
        Location location = locationRowAdaptor.merge(locationRow)
        location.deptId = (Departments)dao.load(Departments.class, location.deptId.deptId)
        Date theDate = new Date()
        location.lastUpdatedDateTime = theDate
        location.createDateTime = theDate
        dao.save(location)
    }

    /** {@inheritDoc} */
    void updateLocation(LocationRow locationRow) {
        Location location = locationRowAdaptor.merge(locationRow)
        //if location is inactivated, remove all ties to maintenance groups
        if (location.enabled.intValue() != 1) {
            location.mtnceGroupMany = new ArrayList<MtnceGroup>()
            //update all mtnce groups
            Date theDate = new Date()
            for (MtnceGroup mg : dao.getAllObjects(MtnceGroup.class)) {
                mg.lastUpdatedDateTime = theDate
            }
        }
        //Associate the object with the session so hibernate does not throw transient object exception.
        location.deptId = (Departments)dao.getObjectById(location.deptId.deptId, Departments.class)
        dao.merge(location)
    }

    /** {@inheritDoc} */
    LocationRow getLocation(Integer id) {
        LocationRow locationRow = locationRowAdaptor.adapt(dao.getObjectById(id, Location.class))
        return locationRow
    }

    void addNewUser(UserRow userRow) {
        Users users = userRowAdaptor.merge(userRow)
        users.deptId = (Departments)dao.load(Departments.class, users.deptId.deptId)
        Date theDate = new Date()
        users.lastUpdatedDateTime = theDate
        users.createDateTime = theDate
        dao.save(users)
    }

    /** {@inheritDoc} */
    void updateUser(UserRow userRow) {
        Users users = userRowAdaptor.merge(userRow)
        //get the department so we do not get a transient object exception with a detached instance.
        users.deptId = (Departments)dao.load(Departments.class, users.deptId.deptId)
        users.roleId = (Roles)dao.load(Roles.class, users.roleId.roleId)
        //if user is inactivated or role is not mtnce user, remove all ties to maintenance groups
        if (users.enabled.intValue() != 1 || users.roleId.roleId != 3) { //Mtnce role
            users.mtnceGroupMany = new ArrayList<MtnceGroup>()
        }
        users = dao.merge(users)
    }

    /** {@inheritDoc} */
    UserRow getUser(Integer id) {
        UserRow userRow = userRowAdaptor.adapt(dao.getObjectById(id, Users.class))
        return userRow
    }

    /** {@inheritDoc} */
    UserRow getUser(String username) {
        UserRow userRow = userRowAdaptor.adapt(dao.getUniqueObjectByExample(new Users(username:username)))
        return userRow
    }

    /** {@inheritDoc} */
    UserRow getUserLoggedIn() {
        Integer userId = securityUtils.getUserIdLoggedIn()
        Users users = (Users)dao.getObjectById(userId, Users.class)
        UserRow userRow = userRowAdaptor.adapt(users)
        return userRow
    }

    /** {@inheritDoc} */
    List<RoleRow> getAllRoles() {
        List<RoleRow> list = new ArrayList<RoleRow>()
        List<Roles> listRoles = dao.getAllObjects(Roles.class)
        for (Roles r : listRoles) {
            list.add(roleRowAdaptor.adapt(r))
        }
        return list
    }


    /** {@inheritDoc} */
    public List<MtnceGroupRow> getAllMtnceGroups() {
        List<MtnceGroupRow> list = new ArrayList<MtnceGroupRow>()
        List<MtnceGroup> listMtnceGroups = dao.getAllObjects(MtnceGroup.class)
        for (MtnceGroup g : listMtnceGroups) {
            list.add(mtnceGroupRowAdaptor.adapt(g))
        }
        return list.sort {MtnceGroupRow it -> it.groupName}
    }

    /** {@inheritDoc} */
	public List<MtnceGroupRow> getAllActiveMtnceGroups() {
		List<MtnceGroupRow> list = new ArrayList<MtnceGroupRow>()
        List<MtnceGroup> listMtnceGroups = dao.getObjectsByExample(new MtnceGroup(enabled:new Integer(1)))
        for (MtnceGroup g : listMtnceGroups) {
			list.add(mtnceGroupRowAdaptor.adapt(g))
		}
		return list.sort {MtnceGroupRow it -> it.groupName}
	}

    /** {@inheritDoc} */
    void addNewMtnceGroup(MtnceGroupRow mtnceGroupRow) {
        MtnceGroup mtnceGroup = mtnceGroupRowAdaptor.merge(mtnceGroupRow)
        Date theDate = new Date()
        mtnceGroup.lastUpdatedDateTime = theDate
        mtnceGroup.createDateTime = theDate
        dao.save(mtnceGroup)
    }

    /** {@inheritDoc} */
    void updateMtnceGroup(MtnceGroupRow mtnceGroupRow) {
        MtnceGroup mtnceGroup = mtnceGroupRowAdaptor.merge(mtnceGroupRow)
        if (mtnceGroup.enabled.intValue() == 0) { //if a mtnce group is inactivated, remove all users
            mtnceGroup.usersMany.clear()
        }
        dao.merge(mtnceGroup)
    }

    /** {@inheritDoc} */
    MtnceGroupRow getMtnceGroup(Integer id) {
        MtnceGroupRow mtnceGroupRow = mtnceGroupRowAdaptor.adapt(dao.getObjectById(id, MtnceGroup.class))
        return mtnceGroupRow
    }

    /**
     * Spring security authentication/authorization service.
     * @param username The user's name.
     * @return The user details object.
     */
    UserDetails loadUserByUsername(String username) {
        Users u = (Users)dao.getUniqueObjectByExample(new Users(username:username))
        UserDetails d
        if (u == null) {
            throw new UsernameNotFoundException('User not found!')
        }
        else {
            GrantedAuthority[] auths = new GrantedAuthority[1]
            auths[0] = new GrantedAuthorityImpl(u.roleId.role)
            boolean enabled = u.enabled.intValue() == 1 ? true : false
            d = new CustomUser(u.userId, u.username, u.password, enabled, auths)
        }
        return d
    }

}