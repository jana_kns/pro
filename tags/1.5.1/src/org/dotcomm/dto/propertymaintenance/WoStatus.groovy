package org.dotcomm.dto.propertymaintenance;

/**
 * DTO for the WoStatus table.
 *
 * @author jdolinski
 */
class WoStatus {

	Integer statusId
	Collection workOrder
	String name
	String descr
	Integer enabled

}
