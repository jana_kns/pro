package org.dotcomm.dto.propertymaintenance;

/**
 * DTO for the Users table.
 *
 * @author jdolinski
 */
class Users {

	Integer userId
    Date createDateTime
    Roles roleId
	Departments deptId
	Date lastUpdatedDateTime
	String fname
	String lname
	String username
	String password
	String phone
	String email
	Integer enabled
    Location locationId
    Collection workOrder
	Collection mtnceGroupMany

}
