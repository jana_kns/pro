package org.dotcomm.dto.propertymaintenance;

/**
 * DTO for the WoType table.
 *
 * @author jdolinski
 */
class WoType {

	Integer woTypeId
    Date createDateTime
    Collection workOrder
	Date lastUpdatedDateTime
	String name
	String descr
	Integer enabled
	Collection mtnceGroupMany

}
