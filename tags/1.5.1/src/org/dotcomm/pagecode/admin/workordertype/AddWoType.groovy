package org.dotcomm.pagecode.admin.workordertype

import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.pagecode.rows.WoTypeRow
import org.dotcomm.pagecode.admin.AdminMain
import org.dotcomm.util.FacesUtils

/**
 * Jsf managed bean for the addwotype.xhtml.
 *
 * @author jdolinski
 */
class AddWoType {

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** Page code data */
    WoTypeRow woTypeRow

    /** Constructor */
    AddWoType() {
        woTypeRow = new WoTypeRow()
        woTypeRow.woActive = new Boolean(true)
    }

   /**
     * Action method to add a new work order type to the system.
     * @return Faces navigation text.
     */
    String newWoType() {
        adminWorkOrderService.addNewWoType(woTypeRow)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

}
