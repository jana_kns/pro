package org.dotcomm.pagecode.workorder

import javax.faces.model.SelectItem
import org.apache.myfaces.trinidad.event.RangeChangeEvent
import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.services.WorkOrderService
import org.dotcomm.util.CsvBean
import org.dotcomm.util.FacesUtils
import org.dotcomm.util.PagedListDataModel
import org.dotcomm.pagecode.adaptors.*
import javax.faces.event.ActionEvent

/**
 * Jsf managed bean for the searchwo.xhtml.
 *
 * @author jdolinski
 */
class SearchWo {

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Injected Property */
    WorkOrderService workOrderService

    /** Page code data */
    WorkOrderRow workOrderRow

    /** Page code data */
    List<WorkOrderRow> pcListWorkOrders

    /** Page code data */
    List<SelectItem> listLocations

    /** Page code data */
    List<SelectItem> listDepts

    /** Page code data */
    List<SelectItem> listStatus

    /** Page code data */
    List<SelectItem> listWoTypes

    /** Page code data */
    List<SelectItem> listMtnceGroups    

    /** Page code data */
    List<SelectItem> listUsers

    /** Page code data */
    List<SelectItem> listServiceItems

    /** Page code data */
    Integer pcServiceItemId

    /** Page code data, database pagination */
    PagedListDataModel dataModel

    /** Bean to download data from table into csv format. */
    CsvBean csvBean

    /** Constructor */
    SearchWo(AdminSecurityService adminSecurityService,
                    AdminWorkOrderService adminWorkOrderService,
                    WorkOrderService workOrderService) {
        this.adminSecurityService = adminSecurityService
        this.adminWorkOrderService = adminWorkOrderService
        this.workOrderService = workOrderService
        csvBean = new CsvBean()
        workOrderRow = new WorkOrderRow()
        populateDropdowns()
        this.workOrderRow.sortOrder = (String)FacesUtils.getPageFlowScopeParameter('sortOrder')
    }

    /**
     * Get data available for selections.
     */
    private void populateDropdowns() {
        SelectLocationRowAdaptor selectLocationRowAdaptor = new SelectLocationRowAdaptor()
        listLocations = selectLocationRowAdaptor.adapt(adminSecurityService.getAllActiveLocations())
        SelectDeptAdaptor selectDeptAdaptor = new SelectDeptAdaptor()
        listDepts = selectDeptAdaptor.adapt(adminSecurityService.getAllActiveDepartments())
        SelectStatusRowAdaptor selectStatusRowAdaptor = new SelectStatusRowAdaptor()
        listStatus = selectStatusRowAdaptor.adapt(workOrderService.getAllWoStatusItems())
        SelectWoTypeAdaptor selectWoTypeAdaptor = new SelectWoTypeAdaptor()
        listWoTypes = selectWoTypeAdaptor.adapt(adminWorkOrderService.getAllActiveWorkOrderTypeItems())
        SelectMtnceGroupRowAdaptor selectMtnceGroupRowAdaptor = new SelectMtnceGroupRowAdaptor()
        listMtnceGroups = selectMtnceGroupRowAdaptor.adapt(adminSecurityService.getAllActiveMtnceGroups())
        SelectUserRowAdaptor selectUserRowAdaptor = new SelectUserRowAdaptor()
        listUsers = selectUserRowAdaptor.adapt(adminSecurityService.getAllUsers())
        SelectServiceGroupItemRowAdaptor itemRowAdaptor = new SelectServiceGroupItemRowAdaptor()
        listServiceItems = itemRowAdaptor.adapt(adminWorkOrderService.getAllActiveServiceItems())
        //page flow scope is used to maintain values across pages, without using a session scoped bean.
        this.workOrderRow.woStatusId = (Integer)FacesUtils.getPageFlowScopeParameter('woStatusId')
        this.workOrderRow.woLocationId = (Integer)FacesUtils.getPageFlowScopeParameter('woLocationId')
        this.workOrderRow.woDepartmentId = (Integer)FacesUtils.getPageFlowScopeParameter('woDepartmentId')
        this.workOrderRow.woNumber = (Long)FacesUtils.getPageFlowScopeParameter('woNumber')
        this.pcServiceItemId = (Integer)FacesUtils.getPageFlowScopeParameter("pcServiceItemId")
        this.workOrderRow.woTypeId = (Integer)FacesUtils.getPageFlowScopeParameter('woTypeId')
        this.workOrderRow.mtnceGroup = (Integer)FacesUtils.getPageFlowScopeParameter('mtnceGroup')
        this.workOrderRow.woRequestorUserId = (Integer)FacesUtils.getPageFlowScopeParameter('woRequestorUserId')
        this.workOrderRow.archived = (Boolean)FacesUtils.getPageFlowScopeParameter('archived')
        this.workOrderRow.startCreateDateTime = (Date)FacesUtils.getPageFlowScopeParameter('fromCreateDateTime')
        this.workOrderRow.endCreateDateTime = (Date)FacesUtils.getPageFlowScopeParameter('toCreateDateTime')
        this.workOrderRow.woReqComments = (String)FacesUtils.getPageFlowScopeParameter('woReqComments')
    }

    /**
     * Invoke search for work orders.
     */
    public void doSearch() {
        if (pcServiceItemId != null) {
            List<Integer> selectedServiceItem = new ArrayList<Integer>()
            selectedServiceItem.add (pcServiceItemId)
            this.workOrderRow.listServiceItems = selectedServiceItem
        }
        //validate from date does not occur after the end date
        if (this.workOrderRow.endCreateDateTime != null
        &&  this.workOrderRow.startCreateDateTime > this.workOrderRow.endCreateDateTime) {
            FacesUtils.addErrorMessage FacesUtils.getStringFromBundle('fromdatebeforetodate')
        }
        else {
            Integer totalSize = workOrderService.getCountWorkOrders(this.workOrderRow);
            csvBean.table.first = 0
            pcListWorkOrders = workOrderService.getFilteredWorkOrders(this.workOrderRow, 0, csvBean.table.rows)
            dataModel = new PagedListDataModel(pcListWorkOrders, totalSize, csvBean.table.rows);
            FacesUtils.putPageFlowScopeParameter 'searchlist', dataModel
        }
        FacesUtils.putPageFlowScopeParameter 'woStatusId', this.workOrderRow.woStatusId
        FacesUtils.putPageFlowScopeParameter 'woLocationId', this.workOrderRow.woLocationId
        FacesUtils.putPageFlowScopeParameter 'woDepartmentId', this.workOrderRow.woDepartmentId
        FacesUtils.putPageFlowScopeParameter 'woNumber', this.workOrderRow.woNumber
        FacesUtils.putPageFlowScopeParameter 'pcServiceItemId', this.pcServiceItemId
        FacesUtils.putPageFlowScopeParameter 'woTypeId', this.workOrderRow.woTypeId
        FacesUtils.putPageFlowScopeParameter 'mtnceGroup', this.workOrderRow.mtnceGroup
        FacesUtils.putPageFlowScopeParameter 'woRequestorUserId', this.workOrderRow.woRequestorUserId
        FacesUtils.putPageFlowScopeParameter 'archived', this.workOrderRow.archived
        FacesUtils.putPageFlowScopeParameter 'fromCreateDateTime', this.workOrderRow.startCreateDateTime
        FacesUtils.putPageFlowScopeParameter 'toCreateDateTime', this.workOrderRow.endCreateDateTime
        FacesUtils.putPageFlowScopeParameter 'woReqComments', this.workOrderRow.woReqComments
    }

    /**
     * View the work order
     * @return The navigation rule
     */
    public String doView() {
        Integer woId = (Integer)FacesUtils.getPageFlowScopeParameter("woid")
        this.workOrderService.updateStatusMtnceViewed(woId)
        return "viewwo"
    }

    /**
     * Edit the work order
     * @return The navigation rule
     */
    public String doEdit() {
        Integer woId = (Integer)FacesUtils.getPageFlowScopeParameter("woid")
        this.workOrderService.updateStatusMtnceViewed(woId)
        return "editwo"
    }

    /**
     * Clear the search filters.
     */
    public void clear() {
        this.workOrderRow.woStatusId = null
        this.workOrderRow.woLocationId = null
        this.workOrderRow.woDepartmentId = null
        this.workOrderRow.woNumber = null
        this.pcServiceItemId = null
        this.workOrderRow.woTypeId = null
        this.workOrderRow.mtnceGroup = null
        this.workOrderRow.woRequestorUserId = null
        this.workOrderRow.archived = null
        this.workOrderRow.startCreateDateTime = null
        this.workOrderRow.endCreateDateTime = null
        this.workOrderRow.woReqComments = null
        FacesUtils.removePageFlowScopeParameter 'searchlist'
        FacesUtils.removePageFlowScopeParameter 'woStatusId'
        FacesUtils.removePageFlowScopeParameter 'woLocationId'
        FacesUtils.removePageFlowScopeParameter 'woDepartmentId'
        FacesUtils.removePageFlowScopeParameter 'woNumber'
        FacesUtils.removePageFlowScopeParameter 'pcServiceItemId'
        FacesUtils.removePageFlowScopeParameter 'woTypeId'
        FacesUtils.removePageFlowScopeParameter 'mtnceGroup'
        FacesUtils.removePageFlowScopeParameter 'woRequestorUserId'
        FacesUtils.removePageFlowScopeParameter 'archived'
        FacesUtils.removePageFlowScopeParameter 'fromCreateDateTime'
        FacesUtils.removePageFlowScopeParameter 'toCreateDateTime'
        FacesUtils.removePageFlowScopeParameter 'woReqComments'
        FacesUtils.removePageFlowScopeParameter 'sortOrder'
    }

    /**
     * Used to implement database pagination.
     * @param e The event when the range changes.
     */
    void rangeChange(RangeChangeEvent e) {
        if (csvBean.table.showAll) { //we are showing all records
            pcListWorkOrders = workOrderService.getFilteredWorkOrders(this.workOrderRow, null, null)
            dataModel = new PagedListDataModel(pcListWorkOrders, csvBean.table.rowCount, pcListWorkOrders.size());
        }
        else { //Only display a page of data at a time, based on the tables rows attribute
            pcListWorkOrders = workOrderService.getFilteredWorkOrders(this.workOrderRow, csvBean.table.first, csvBean.table.rows)
            dataModel = new PagedListDataModel(pcListWorkOrders, csvBean.table.rowCount, csvBean.table.rows);
            dataModel.first = csvBean.table.first
        }
        FacesUtils.putPageFlowScopeParameter 'searchlist', dataModel
    }


   /**
    * Sort the selected column on the search results table.
    * @param e The selected column.
    */
    void sortColumn(ActionEvent e) {
        this.workOrderRow.sortOrder = (String)FacesUtils.getPageFlowScopeParameter('sortOrder')
        String sortDeptValue = "locationId.${e.component.id}"
        if (this.workOrderRow.sortOrder.equals(e.component.id) || this.workOrderRow.sortOrder.equals(sortDeptValue)) {
            this.workOrderRow.sortOrder = "${e.component.id} desc"
        } else {
            this.workOrderRow.sortOrder = e.component.id
        }
        this.workOrderRow.sortOrder = this.workOrderRow.sortOrder.replaceAll("deptId","locationId.deptId")
        FacesUtils.putPageFlowScopeParameter 'sortOrder', this.workOrderRow.sortOrder
        doSearch()
    }
}
