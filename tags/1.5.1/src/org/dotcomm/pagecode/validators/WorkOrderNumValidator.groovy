package org.dotcomm.pagecode.validators

import javax.faces.validator.ValidatorException
import javax.faces.component.UIComponent
import java.util.regex.Pattern
import javax.faces.context.FacesContext
import javax.faces.validator.Validator
import javax.faces.application.FacesMessage
import java.util.regex.Matcher
import org.apache.myfaces.trinidad.validator.ClientValidator

/**
 * Jsf custom validator for record identifiers using a date and id.
 *
 * @author jdolinski
 */
class WorkOrderNumValidator implements Validator, ClientValidator {

    /**
     * First 4 characters (year) must be between 2000-2100
     *  Characters 5-6 (month) must be between 01-12, currently checking it is a between 00-19
     *                      need to enhance.
     *  Charaters 7-8 (day) is not being validated against month's day, currently checking it is a between 00-39
     *                      need to enhance.
     *  Characters 9-# (id) must be numeric
     */
    private static final String DATE_ID = '^[2][0][0-9][0-9][0-1][0-9][0-3][0-9]\\d+$'

    /** Error message to send back to user */
    String errMsg = 'Invalid Work Order Number.'

    void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        /** create a mask */
        Pattern mask = Pattern.compile(DATE_ID)

        /** retrieve the string value of the field */
        String dateid = (String)value

        /** check to ensure that the value is valid */
        Matcher matcher = mask.matcher(dateid)

        if (!matcher.matches()) {
            FacesMessage msg = new FacesMessage()
            msg.setDetail(errMsg)
            msg.setSummary(errMsg)
            msg.setSeverity(FacesMessage.SEVERITY_ERROR)
            //todo get message from resource bundle, or inject
            throw new ValidatorException(msg)
        }
    }

    public String getClientLibrarySource(FacesContext facesContext) {
        return facesContext.getExternalContext().getRequestContextPath() + '/jslib/WorkOrderNumValidator.js'
    }

    public Collection<String> getClientImportNames() {
        return null
    }

    public String getClientScript(FacesContext facesContext, UIComponent uiComponent) {
        return null
    }

    public String getClientValidation(FacesContext facesContext, UIComponent uiComponent) {
        return "new WorkOrderNumValidator()"
    }
}