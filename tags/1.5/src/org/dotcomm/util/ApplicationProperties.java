package org.dotcomm.util;

/**
 * Spring bean for setting properties used by the application.
 *
 * @author jdolinski
 */
public class ApplicationProperties {

    /** Is this the test application. */
    private boolean test;

    /** Url to the help application. */
    private String helpUrl;
    
    /** Url to the project tracking application. */
    private String ptsUrl;
    
    /** Url to the ca service desk for chg orders. */
    private String caChgUrl;
    
    /** Url to the ca service desk for req orders. */
    private String caReqUrl;

    /** Location of the file share for the files. */
    private String fileServerShareDir;

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    public String getHelpUrl() {
        return helpUrl;
    }

    public void setHelpUrl(String helpUrl) {
        this.helpUrl = helpUrl;
    }

    public String getFileServerShareDir() {
        return fileServerShareDir;
    }

    public void setFileServerShareDir(String fileServerShareDir) {
        this.fileServerShareDir = fileServerShareDir;
    }

	public String getCaChgUrl() {
		return caChgUrl;
	}

	public void setCaChgUrl(String caChgUrl) {
		this.caChgUrl = caChgUrl;
	}

	public String getCaReqUrl() {
		return caReqUrl;
	}

	public void setCaReqUrl(String caReqUrl) {
		this.caReqUrl = caReqUrl;
	}

	public String getPtsUrl() {
		return ptsUrl;
	}

	public void setPtsUrl(String ptsUrl) {
		this.ptsUrl = ptsUrl;
	}

}
