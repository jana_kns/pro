package org.dotcomm.pagecode.rows

/**
 * Page code bean for displaying material information.
 *
 * @author jdolinski
 */
class MaterialRow {

    Integer matId
    Integer woId
    String matPartName
    Integer matQuantity
    String matPoNumber
    boolean readOnly
    
}