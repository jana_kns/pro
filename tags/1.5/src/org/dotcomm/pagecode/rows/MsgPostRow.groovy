package org.dotcomm.pagecode.rows

/**
 * Page code bean for displaying message posting information.
 *
 * @author jdolinski
 */
class MsgPostRow {

    Integer msgId
    String msgTitle
    String msg
    Date msgDate
    String updBy
   
}