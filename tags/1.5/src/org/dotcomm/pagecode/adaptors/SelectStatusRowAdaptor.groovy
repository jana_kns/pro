package org.dotcomm.pagecode.adaptors

import org.dotcomm.util.Adaptor
import javax.faces.model.SelectItem
import org.dotcomm.pagecode.rows.WoStatusRow

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class SelectStatusRowAdaptor implements Adaptor {

    /**
     * Adapt a list of view rows to selection items.
     * @return list of select items
     */
    public Object adapt(Object obj) {
        List<SelectItem> listSelectItems = new ArrayList<SelectItem>()
        List<WoStatusRow> listWoStatusRows = (ArrayList<WoStatusRow>) obj
        for (WoStatusRow s : listWoStatusRows) {
            listSelectItems.add(new SelectItem(s.woStatusId, s.woStatusName, s.woStatusDescr, false))
        }
        return listSelectItems
    }

    public Class<ArrayList<WoStatusRow>> getFromType() {
        return ArrayList
    }

    public Class<ArrayList<SelectItem>> getToType() {
        return ArrayList
    }

    /**
     * Merge object from list of select items to list of view rows.
     * @return list view row
     */
    public Object merge(Object obj) {
        List<WoStatusRow> listWoStatusRows = new ArrayList<WoStatusRow>()
        List<SelectItem> listSelectItems = (ArrayList<SelectItem>) obj
        for (SelectItem i : listSelectItems) {
            listWoStatusRows.add(new WoStatusRow(woStatusId:(Integer)i.value, woStatusName:i.label, woStatusDescr:i.description))
        }
        return listWoStatusRows
    }
}