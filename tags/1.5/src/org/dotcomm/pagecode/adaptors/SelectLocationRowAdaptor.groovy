package org.dotcomm.pagecode.adaptors

import org.dotcomm.util.Adaptor
import org.dotcomm.pagecode.rows.LocationRow
import javax.faces.model.SelectItem

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class SelectLocationRowAdaptor implements Adaptor {

    /**
     * Adapt a list of view rows to selection items.
     * @return list of select items
     */
    public Object adapt(Object obj) {
        List<SelectItem> listSelectItems = new ArrayList<SelectItem>()
        List<LocationRow> listLocationRows = (ArrayList<LocationRow>) obj
        for (LocationRow l : listLocationRows) {
            listSelectItems.add(new SelectItem(l.locId, l.locName, l.locDesc))
        }
        return listSelectItems.sort {SelectItem it -> it.label}
    }

    public Class<ArrayList<LocationRow>> getFromType() {
        return ArrayList
    }

    public Class<ArrayList<SelectItem>> getToType() {
        return ArrayList
    }

    /**
     * Merge object from list of select items to list of view rows.
     * @return list view row
     */
    public Object merge(Object obj) {
        List<LocationRow> listLocationRows = new ArrayList<LocationRow>()
        List<SelectItem> listSelectItems = (ArrayList<SelectItem>) obj
        for (SelectItem i : listSelectItems) {
            listLocationRows.add(new LocationRow(locId:i.value, locName:i.label, locDesc:i.description))
        }
        return listLocationRows
    }


}