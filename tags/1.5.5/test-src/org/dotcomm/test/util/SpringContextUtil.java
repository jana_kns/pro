package org.dotcomm.test.util;

public class SpringContextUtil {

    public static String getApplicationContextDirectory() {
        String appLocation = SpringContextUtil.class.getResource("").getPath()
                .split("classes")[0];
        appLocation = appLocation.replace("%20", " ");
        return "file://" + appLocation;
    }

    /**public static String getUnitTestApplicationContextFile() {
        String appLocation = SpringContextUtil.class.getResource("").getPath()
                .split("WebContent")[0];
        appLocation = appLocation.replace("%20", " ");
        appLocation += "test-src/testApplicationContext.xml";
        return "file://" + appLocation;
    }*/
}
