package org.dotcomm.pagecode.validators

import javax.faces.validator.Validator
import javax.faces.application.FacesMessage
import javax.faces.validator.ValidatorException
import java.util.regex.Matcher
import javax.faces.context.FacesContext
import java.util.regex.Pattern
import javax.faces.component.UIComponent
import org.apache.myfaces.trinidad.validator.ClientValidator

/**
 * Jsf custom validator for phone numbers.
 *
 * @author jdolinski
 */
class PhoneNumValidator implements Validator, ClientValidator {

    /** phone number in form of xxx-xxxx or xxx-xxx-xxxx */
    private static final String PHONE_NUM = '^([0-9]{3}?[-])?[0-9]{3}[-]{1}[0-9]{4}$'

    /** Error message to send back to user */
    String errMsg = "Invalid Format. Please use format: 123-456-7890 or 456-7890."

    void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        /** create a mask */
        Pattern mask = Pattern.compile(PHONE_NUM)

        /** retrieve the string value of the field */
        String phoneNumber = (String)value

        /** check to ensure that the value is valid */
        Matcher matcher = mask.matcher(phoneNumber)

        if (!matcher.matches()) {
            FacesMessage msg = new FacesMessage()
            msg.setDetail(errMsg)
            msg.setSummary(errMsg)
            msg.setSeverity(FacesMessage.SEVERITY_ERROR)
            //todo get message from resource bundle, or inject
            throw new ValidatorException(msg)
        }
    }

    Collection<String> getClientImportNames() {
        return null
    }

    String getClientLibrarySource(FacesContext facesContext) {
        return facesContext.getExternalContext().getRequestContextPath() + '/jslib/PhoneNumValidator.js'
    }

    String getClientScript(FacesContext facesContext, UIComponent uiComponent) {
        return null
    }

    String getClientValidation(FacesContext facesContext, UIComponent uiComponent) {
        return "new PhoneNumValidator()" 
    }

}