package org.dotcomm.pagecode.validators

import javax.faces.validator.ValidatorException
import javax.faces.context.FacesContext
import javax.faces.validator.Validator
import java.util.regex.Matcher
import javax.faces.component.UIComponent
import java.util.regex.Pattern
import javax.faces.application.FacesMessage
import org.apache.myfaces.trinidad.validator.ClientValidator

/**
 * Jsf custom validator for passwords.
 *
 * @author jdolinski
 */
class PasswordValidator implements Validator, ClientValidator {

    /** password must be 8 digits, one upper character and one lower character and at least one digit */
    private static final String PASSWORD = '^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$'

    /** Error message to send back to user */
    String errMsg = "Password must be at least 8 digits, have at least one uppercase character, at least one lower case character, and at least one digit."

    void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        /** create a mask */
        Pattern mask = Pattern.compile(PASSWORD)

        /** retrieve the string value of the field */
        String phoneNumber = (String)value

        /** check to ensure that the value is valid */
        Matcher matcher = mask.matcher(phoneNumber)

        if (!matcher.matches()) {
            FacesMessage msg = new FacesMessage()
            msg.setDetail(errMsg)
            msg.setSummary(errMsg)
            msg.setSeverity(FacesMessage.SEVERITY_ERROR)
            //todo get message from resource bundle, or inject
            throw new ValidatorException(msg)
        }
    }

    public String getClientLibrarySource(FacesContext facesContext) {
        return facesContext.getExternalContext().getRequestContextPath() + '/jslib/PasswordValidator.js'
    }

    public Collection<String> getClientImportNames() {
        return null
    }

    public String getClientScript(FacesContext facesContext, UIComponent uiComponent) {
        return null
    }

    public String getClientValidation(FacesContext facesContext, UIComponent uiComponent) {
        return "new PasswordValidator()"
    }
    
}