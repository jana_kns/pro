package org.dotcomm.pagecode.admin;

import org.dotcomm.pagecode.rows.MsgPostRow
import org.dotcomm.pagecode.rows.WoTypeRow
import org.dotcomm.pagecode.rows.ServiceGroupRow
import org.dotcomm.pagecode.rows.DeptRow
import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.rows.LocationRow
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.pagecode.rows.MtnceGroupRow
import org.dotcomm.util.FacesUtils

/**
 * Jsf managed bean for the adminmain.xhtml.
 *
 * @author jdolinski
 */
class AdminMain {

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** List of announcements/messages displayed on the page */
    List<MsgPostRow> listMessages

    /** List of work order types displayed on the page */
    List<WoTypeRow> listWoTypes

    /** List of work order types displayed on the page */
    List<ServiceGroupRow> listServiceGroups

    /** List of departments displayed on the page */
    List<DeptRow> listDepts

    /** List of locations displayed on the page */
    List<LocationRow> listLocs

    /** List of users displayed on the page */
    List<UserRow> listUsers

    /** List of maintenance groups displayed on the page */
    List<MtnceGroupRow> listMtnceGroups

    /** Constructor */
    AdminMain(AdminWorkOrderService adminWorkOrderService, AdminSecurityService adminSecurityService) {
        this.adminWorkOrderService = adminWorkOrderService
        this.adminSecurityService = adminSecurityService
    }

    /**
     *  Populate page with admin records.
     */
    void doSearch() {
        listMessages = this.adminWorkOrderService.getAllMsgPostItems()
        listWoTypes = this.adminWorkOrderService.getAllWorkOrderTypeItems()
        listServiceGroups = this.adminWorkOrderService.getAllServiceGroupItems()
        listDepts = this.adminSecurityService.getAllDepartments()
        listLocs = this.adminSecurityService.getAllLocations()
        listUsers = this.adminSecurityService.getAllUsers()
        listMtnceGroups = this.adminSecurityService.getAllMtnceGroups()
        FacesUtils.putPageFlowScopeParameter 'adminbean', this
    }

    /**
     * Action method to delete an existing message in the system.
     * @return Faces navigation text.
     */
    String deleteMsg() {
        Integer msgId = (Integer)FacesUtils.getPageFlowScopeParameter("msgid")
        adminWorkOrderService.deleteMsgPost(msgId)
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return "admin"
    }

    /**
     * Action method to navigate to add a new service group.
     * @return Faces navigation text.
     */
    String addServGroup() {
        FacesUtils.removePageFlowScopeParameter("ServFormListItems")
        return "addservgroup"
    }

    /**
     * Action method to navigate to add a new service group.
     * @return Faces navigation text.
     */
    String editServGroup() {
        FacesUtils.removePageFlowScopeParameter("ServFormListItems")
        return "editservgroup"
    }

}
