package org.dotcomm.pagecode.admin.user

import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.admin.AdminMain
import org.dotcomm.pagecode.compositions.UserForm
import org.dotcomm.pagecode.adaptors.SelectLocationRowAdaptor

/**
 * Jsf managed bean for the adduser.xhtml.
 *
 * @author jdolinski
 */
class AddUser extends UserForm {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Constructor */
    AddUser(AdminSecurityService adminSecurityService) {
        super(adminSecurityService)
        this.adminSecurityService = adminSecurityService
        userRow = new UserRow()
        userRow.active = new Boolean(true)
        userRow.roleId = 1 //default new user to user role
        SelectLocationRowAdaptor selectLocationRowAdaptor = new SelectLocationRowAdaptor()
        //if we are adding a new user from the dept page, default the department.
        if (FacesUtils.getPageFlowScopeParameter('addUserFromDept') != null) {
            userRow.deptId = (Integer) FacesUtils.getPageFlowScopeParameter('addUserFromDept')
            FacesUtils.removePageFlowScopeParameter 'addUserFromDept'
            pcListLocs = selectLocationRowAdaptor.adapt(this.adminSecurityService.getAllActiveDeptLocations(userRow.deptId))
        } 
    }

    /**
     * Action method to add a new user to the system.
     * @return Faces navigation text.
     */
    String newUser() {
        if (validate()) {
            return
        }
        adminSecurityService.addNewUser (userRow)
        String navText = (String)FacesUtils.getPageFlowScopeParameter("addusernav")
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return navText
    }

    /**
     * Action method for the cancel button.
     * @return Faces navigation text.
     */
    String cancel() {
        String navText = (String)FacesUtils.getPageFlowScopeParameter("addusernav")
        return navText
    }
}
