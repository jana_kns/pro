package org.dotcomm.pagecode.compositions

import javax.faces.event.ValueChangeEvent
import javax.faces.model.SelectItem
import org.dotcomm.pagecode.adaptors.SelectDeptAdaptor
import org.dotcomm.pagecode.adaptors.SelectLocationRowAdaptor
import org.dotcomm.pagecode.adaptors.SelectMtnceGroupRowAdaptor
import org.dotcomm.pagecode.adaptors.SelectRoleAdaptor
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.util.FacesUtils

/**
 * Facelet compostion backing bean for the userform.xhtml.
 *
 * @author JDolinski
 */
class UserForm {

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Page code data */
    List<SelectItem> pcListDepts
    List<SelectItem> pcListRoles
    List<SelectItem> pcListLocs
    List<SelectItem> pcListMtnceGroups
    UserRow userRow

    /** Constructor */
    UserForm(AdminSecurityService adminSecurityService) {
        this.adminSecurityService = adminSecurityService
        SelectDeptAdaptor selectDeptAdaptor = new SelectDeptAdaptor()
        SelectRoleAdaptor selectRoleAdaptor = new SelectRoleAdaptor()
        SelectMtnceGroupRowAdaptor selectMtnceGroupRowAdaptor = new SelectMtnceGroupRowAdaptor()
        pcListDepts = selectDeptAdaptor.adapt(adminSecurityService.getAllActiveDepartments())
        pcListRoles = selectRoleAdaptor.adapt(adminSecurityService.getAllRoles())
        pcListMtnceGroups = selectMtnceGroupRowAdaptor.adapt(adminSecurityService.getAllActiveMtnceGroups())
    }


    /**
     * Update the location list with locations from the select department.
     * @param event The change event.
     */
    void deptSelection(ValueChangeEvent event) {
      Integer deptId = (Integer)event.newValue
      SelectLocationRowAdaptor selectLocationRowAdaptor = new SelectLocationRowAdaptor()
      pcListLocs = selectLocationRowAdaptor.adapt(adminSecurityService.getAllActiveDeptLocations(deptId))
    }

    /**
     * Validate user form object with custom validations.
     * @return True if error found.
     */
    boolean validate() {
        boolean validationError = false
        if (userRow.roleId == 5 && userRow.locationId == null) {
            validationError = true
            FacesUtils.addErrorMessage 'form:defaultLocation', FacesUtils.getStringFromBundle('deflocReq')
        }
        return validationError
    }
}
