package org.dotcomm.pagecode.workorder

import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.services.WorkOrderService
import org.dotcomm.util.FacesUtils
import javax.faces.model.SelectItem
import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.pagecode.adaptors.SelectLocationRowAdaptor
import org.dotcomm.pagecode.adaptors.SelectWoTypeAdaptor
import org.dotcomm.pagecode.adaptors.SelectStatusRowAdaptorUnviewDisabled
import org.dotcomm.pagecode.adaptors.SelectServiceGroupItemRowAdaptor
import javax.faces.event.ActionEvent
import org.dotcomm.pagecode.rows.MaterialRow
import javax.faces.event.ValueChangeEvent
import org.apache.myfaces.trinidad.component.core.layout.CorePanelLabelAndMessage
import org.apache.myfaces.trinidad.component.core.data.CoreTable
import org.apache.myfaces.trinidad.component.UIXOutput
import org.apache.myfaces.trinidad.component.UIXInput
import javax.faces.application.FacesMessage
import javax.faces.validator.ValidatorException
import javax.faces.component.UIComponent
import javax.faces.context.FacesContext
import org.richfaces.component.html.HtmlCalendar
import org.dotcomm.pagecode.rows.UserRow

/**
 * Jsf managed bean for the editwo.xhtml.
 *
 * @author jdolinski
 */
class EditWo {

    /** Injected Property */
    WorkOrderService workOrderService

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** Injected Property */
    AdminSecurityService adminSecurityService

    /** Page code data */
    WorkOrderRow workOrderRow

    /** Page code data */
    List<SelectItem> listLocations

    /** Page code data */
    List<SelectItem> listWoTypes

    /** Page code data */
    List<SelectItem> listWoStatuses

    /** Page code data */
    List<SelectItem> pcListServiceItems

    /** Binding for removing the material row after delete */
    CoreTable bindTable
    UIXOutput bindMaterialRow
    UIXInput bindPart

    /** Default time for date pop up */
    String defaultTime

    /** Binding for setting required fields */
    CorePanelLabelAndMessage bindStartDate
    CorePanelLabelAndMessage bindCompletionDate
    CorePanelLabelAndMessage bindCompletionCommments
    CorePanelLabelAndMessage bindServiceAreas
    CorePanelLabelAndMessage bindLaborHrs
    CorePanelLabelAndMessage bindCompletedBy

    /** Constructor */
    EditWo(WorkOrderService workOrderService,
                  AdminSecurityService adminSecurityService,
                  AdminWorkOrderService adminWorkOrderService) {
        this.workOrderService = workOrderService
        this.adminSecurityService = adminSecurityService
        this.adminWorkOrderService = adminWorkOrderService
        SelectLocationRowAdaptor locationRowAdaptor = new SelectLocationRowAdaptor()
        listLocations = locationRowAdaptor.adapt(adminSecurityService.getAllActiveLocations())
        SelectWoTypeAdaptor woTypeAdaptor = new SelectWoTypeAdaptor()
        listWoTypes = woTypeAdaptor.adapt(adminWorkOrderService.getAllActiveWorkOrderTypeItems())
        SelectStatusRowAdaptorUnviewDisabled statusRowAdaptor = new SelectStatusRowAdaptorUnviewDisabled()
        listWoStatuses = statusRowAdaptor.adapt(workOrderService.getAllActiveWorkOrderStatuses())
        SelectServiceGroupItemRowAdaptor itemRowAdaptor = new SelectServiceGroupItemRowAdaptor()
        pcListServiceItems = itemRowAdaptor.adapt(adminWorkOrderService.getAllActiveServiceItems())
        Integer woId = (Integer)FacesUtils.getPageFlowScopeParameter("woid")
        workOrderRow = this.workOrderService.getWorkOrderForEdit(woId)
        if (FacesUtils.getPageFlowScopeParameter("editWoMaterials") == null) {
            FacesUtils.putPageFlowScopeParameter "editWoMaterials", workOrderRow.listMaterials
        }
    }

    /**
     * Action method to edit an existing work order in the system.
     * @return Faces navigation text.
     */
    String updWorkOrder() {
        List<MaterialRow> list = (List)FacesUtils.getPageFlowScopeParameter ("editWoMaterials")
        workOrderRow.listMaterials = list
        if (validate()) {
            workOrderService.updateWorkOrder(workOrderRow)
        }
        else {
            return null
        }
        return "viewwo"
    }

    /**
     * Action event to add a new material to the workorder.
     * @param Action event
     */
    void addMaterialRow(ActionEvent event) {
        List<MaterialRow> materials = (ArrayList<MaterialRow>)FacesUtils.getPageFlowScopeParameter ("editWoMaterials")
        if (!validateParts()) {
            if (materials == null || materials.size() == 0) {
                materials = new ArrayList<MaterialRow>()
                materials.add(new MaterialRow(readOnly:false))
            }
            else {
                MaterialRow previous = (MaterialRow) materials.get(materials.size() - 1)
                previous.readOnly = true
                materials.add(new MaterialRow(readOnly:false))
            }
        }
        FacesUtils.putPageFlowScopeParameter "editWoMaterials", materials
    }

    /**
     * Action method to delete a material row.
     * @return Faces navigation text.
     */
    void deleteMaterial() {
        List<MaterialRow> listMaterials = (ArrayList<MaterialRow>)FacesUtils.getPageFlowScopeParameter ("editWoMaterials")
        listMaterials.remove((MaterialRow)bindMaterialRow.getValue())
        FacesUtils.putPageFlowScopeParameter "editWoMaterials", listMaterials
    }

    /**
     * Method to validate the work order.
     * @return boolean True if there are no errors, false if errors exist.
     */
    //todo: Move this out to jsf's process validations phase using a validator - Having problem registering more than 1 validator on a component.
    private boolean validate() {
        boolean noErrors = true
        //if the work order is marked as completed, we have required fields.
        if (workOrderRow.woStatusId.intValue() == 3) {
            if (workOrderRow.woStartDate == null) {
                FacesUtils.addErrorMessage 'form:startDate', FacesUtils.getStringFromBundle('valuereq')
                noErrors = false
            }
            if (workOrderRow.woCompletionDate == null) {
                //default the completed date, if not entered
                workOrderRow.woCompletionDate = new Date()
            }
            if (workOrderRow.woCompletionComments == null || workOrderRow.woCompletionComments.length() == 0) {
                //default the complete comments, if not entered
                String usrName = ((UserRow)adminSecurityService.getUserLoggedIn()).uname
                workOrderRow.woCompletionComments = "Completed - $usrName"
            }
            if (workOrderRow.listServiceItems == null || workOrderRow.listServiceItems.isEmpty()) {
                FacesUtils.addErrorMessage 'form:servdone', FacesUtils.getStringFromBundle('selreq')
                noErrors = false
            }
            if (workOrderRow.woCompletedBy == null || workOrderRow.woCompletedBy.length() == 0) {
                FacesUtils.addErrorMessage 'form:compby', FacesUtils.getStringFromBundle('valuereq')
                noErrors = false
            }
            if (workOrderRow.woManHrs == null) {
                FacesUtils.addErrorMessage 'form:laborhrs', FacesUtils.getStringFromBundle('valuereq')
                noErrors = false
            }
        }
        if (validateParts()) {
            noErrors = false
        }
        //output a global error message
        if (!noErrors) {
            FacesUtils.addErrorMessage FacesUtils.getStringFromBundle('statuscomplreq')
        }
        return noErrors
    }

    /**
     * Validate the required fields for the parts/materials table.
     * See ServForm.groovy for further details.
     * @return true if validation error exists, else false
     */
    boolean validateParts() {
        boolean validationError = false
        List<MaterialRow> listMaterials = (ArrayList<MaterialRow>)FacesUtils.getPageFlowScopeParameter ("editWoMaterials")
        int idx = 0
        for (MaterialRow row : listMaterials) {
            if (row.matPartName == null || row.matPartName.trim().length() < 1) {
                validationError = true
                FacesUtils.addErrorMessage "form:partresults:$idx:partname", FacesUtils.getStringFromBundle('valuereq')
            }
            if (row.matQuantity == null) {
                validationError = true
                FacesUtils.addErrorMessage "form:partresults:$idx:qty", FacesUtils.getStringFromBundle('valuereq')
            }
            else if (row.matQuantity <= 0) {
                validationError = true
                FacesUtils.addErrorMessage "form:partresults:$idx:qty", FacesUtils.getStringFromBundle('greaterzero')
            }
            idx++
        }
        return validationError
    }

    /**
     * If the status of the work order changes, check to see if we have required fields.
     * @param event
     */
    void statusChangeListener(ValueChangeEvent e) {
        determineRequiredFields((Integer)e.getNewValue())
    }

    /**
     * Determine the required fields for the work order.
     * @param statuId The id of the work order status.
     */
    private void determineRequiredFields(Integer statusId) {
        if (statusId.intValue() == 3) { //completed
            bindStartDate.setShowRequired true
            bindCompletionDate.setShowRequired true
            bindCompletionCommments.setShowRequired true
            bindServiceAreas.setShowRequired true
            bindLaborHrs.setShowRequired true
            bindCompletedBy.setShowRequired true
        }
        else {
            bindStartDate.setShowRequired false
            bindCompletionDate.setShowRequired false
            bindCompletionCommments.setShowRequired false
            bindServiceAreas.setShowRequired false
            bindLaborHrs.setShowRequired false
            bindCompletedBy.setShowRequired false
        }
    }

    /**
     * Set required fields when the work order is initially opened.
     */
    void setBindStartDate(CorePanelLabelAndMessage corePanelLabelAndMessage) {
        bindStartDate = corePanelLabelAndMessage
        if (workOrderRow.woStatusId.intValue() == 3) {
            bindStartDate.setShowRequired true
        }
        else {
            bindStartDate.setShowRequired false
        }
    }

    /**
     * Set required fields when the work order is initially opened.
     */
    void setBindCompletionDate(CorePanelLabelAndMessage corePanelLabelAndMessage) {
        bindCompletionDate = corePanelLabelAndMessage
        if (workOrderRow.woStatusId.intValue() == 3) {
            bindCompletionDate.setShowRequired true
        }
        else {
            bindCompletionDate.setShowRequired false
        }
    }

    /**
     * Set required fields when the work order is initially opened.
     */
    void setBindCompletionComments(CorePanelLabelAndMessage corePanelLabelAndMessage) {
        bindCompletionComments = corePanelLabelAndMessage
        if (workOrderRow.woStatusId.intValue() == 3) {
            bindCompletionCommments.setShowRequired true
        }
        else {
            bindCompletionCommments.setShowRequired false
        }
    }

    /**
     * Set required fields when the work order is initially opened.
     */
    void setBindServiceAreas(CorePanelLabelAndMessage corePanelLabelAndMessage) {
        bindServiceAreas = corePanelLabelAndMessage
        if (workOrderRow.woStatusId.intValue() == 3) {
            bindServiceAreas.setShowRequired true
        }
        else {
            bindServiceAreas.setShowRequired false
        }
    }

    /**
     * Set required fields when the work order is initially opened.
     */
    void setBindLaborHrs(CorePanelLabelAndMessage corePanelLabelAndMessage) {
        bindLaborHrs = corePanelLabelAndMessage
        if (workOrderRow.woStatusId.intValue() == 3) {
            bindLaborHrs.setShowRequired true
        }
        else {
            bindLaborHrs.setShowRequired false
        }
    }

    /**
     * Set required fields when the work order is initially opened.
     */
    void setBindCompletedBy(CorePanelLabelAndMessage corePanelLabelAndMessage) {
        bindCompletedBy = corePanelLabelAndMessage
        if (workOrderRow.woStatusId.intValue() == 3) {
            bindCompletedBy.setShowRequired true
        }
        else {
            bindCompletedBy.setShowRequired false
        }
    }

    /**
     * Validator to check the start date is before the completion date.
     * @param context The current faces context.
     * @param component The component the validator is registered on.
     * @param value The form value submitted.
     */
    void completionDateTimeValidator(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        /** retrieve the string value of the field */
        Date complDate = (Date)value

        /** Get the error msg from the message bundle */
        String errorMsg = FacesUtils.getStringFromBundle('compldatebeforestartdate')

        /**
         * If we are editing a user, this variable will be populated with user's database id we are working with.
         *  This is the component id on the edituser.xhtml page.
         */
        HtmlCalendar startDateCalendar = (HtmlCalendar)FacesUtils.findComponentInRoot("startDate")
        Date startDate = (Date)startDateCalendar?.value
        //get rid of seconds, milliseconds for comparison
        Calendar cal = Calendar.getInstance()
        try {
            cal.setTime startDate
        } catch (NullPointerException e) {
            //start date not required for all status, if null completion date is filled in but no start date
            FacesMessage msg = new FacesMessage()
            msg.setDetail(errorMsg)
            msg.setSummary(errorMsg)
            msg.setSeverity(FacesMessage.SEVERITY_ERROR)
            throw new ValidatorException(msg)
        }
        cal.set(Calendar.MILLISECOND, 0)
        cal.set(Calendar.SECOND, 0)
        startDate = cal.getTime()

        if (startDate > complDate) {
            FacesMessage msg = new FacesMessage()
            msg.setDetail(errorMsg)
            msg.setSummary(errorMsg)
            msg.setSeverity(FacesMessage.SEVERITY_ERROR)
            throw new ValidatorException(msg)
        }
    }

    /**
     * Validator to check the start date is after the creation date of the work order.
     * 7/1/10 - Removed per Marv Olson C33493
     * @param context The current faces context.
     * @param component The component the validator is registered on.
     * @param value The form value submitted.
     */
    void startDateTimeValidator(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        /** retrieve the string value of the field */
        Date startDate = (Date)value

        /**
         * If we are editing a user, this variable will be populated with user's database id we are working with.
         *  This is the component id on the edituser.xhtml page.
         */
        UIXInput createDateComponent = (UIXInput)FacesUtils.findComponentInRoot("datereq")
        Date createDate = (Date)createDateComponent?.value
        //get rid of seconds, milliseconds for comparison
        Calendar cal = Calendar.getInstance()
        cal.setTime createDate
        cal.set(Calendar.MILLISECOND, 0)
        cal.set(Calendar.SECOND, 0)
        createDate = cal.getTime()

        /** Get the error msg from the message bundle */
        String errorMsg = FacesUtils.getStringFromBundle('startdatebeforecreatedate')

        if (createDate > startDate) {  //check if user name is in the system
            FacesMessage msg = new FacesMessage()
            msg.setDetail(errorMsg)
            msg.setSummary(errorMsg)
            msg.setSeverity(FacesMessage.SEVERITY_ERROR)
            throw new ValidatorException(msg)
        }
    }

    String getDefaultTime() {
        Calendar cal = Calendar.getInstance()
        cal.time = new Date()
        return "${cal.get(cal.HOUR_OF_DAY)}:${cal.get(cal.MINUTE)}"
    }
}
