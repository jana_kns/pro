package org.dotcomm.util

import org.springframework.security.userdetails.User
import org.springframework.security.GrantedAuthority

/**
 * Custom spring security user to hold the database user id for the application to do a database user
 * lookup.
 *
 * @author jdolinski
 */
class CustomUser extends User {

    Integer userId

    /** Constructor */
    CustomUser(Integer userId, String userName, String password, boolean enabled, GrantedAuthority[] auth) {
        super(userName, password, enabled, true, true, true, auth)
        boolean accountNonExpired = true
        boolean credentialsNonExpired = true
        boolean accountNonLocked = true
        this.userId = userId
    }

}