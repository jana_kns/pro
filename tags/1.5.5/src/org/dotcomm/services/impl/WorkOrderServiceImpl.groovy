package org.dotcomm.services.impl

import org.dotcomm.services.WorkOrderService
import org.dotcomm.dto.propertymaintenance.WorkOrder
import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.dto.propertymaintenance.adaptors.WorkOrderRowAdaptor
import org.dotcomm.persistence.DataAccessObject
import org.dotcomm.pagecode.rows.WoStatusRow
import org.dotcomm.dto.propertymaintenance.WoStatus
import org.dotcomm.dto.propertymaintenance.adaptors.WoStatusRowAdaptor
import org.dotcomm.dto.propertymaintenance.Users
import org.dotcomm.dao.WorkOrderDao
import java.text.SimpleDateFormat
import java.text.DateFormat
import org.dotcomm.dto.propertymaintenance.Archive
import org.springframework.security.AccessDeniedException
import org.dotcomm.dto.propertymaintenance.WoType
import org.dotcomm.dto.propertymaintenance.Location
import org.dotcomm.util.SecurityUtils
import org.apache.log4j.Logger
import org.dotcomm.dto.propertymaintenance.WoPurge
import org.dotcomm.dto.propertymaintenance.adaptors.WoPurgeAdaptor
import org.dotcomm.dto.propertymaintenance.ServiceItem
import org.dotcomm.dto.propertymaintenance.adaptors.ServiceGroupItemRowAdaptor

/**
 * The implementation for {@link WorkOrderService}.
 *
 * @author JDolinski
 *
 */
class WorkOrderServiceImpl implements WorkOrderService {

  /** Logging for the class */
    Logger logger = Logger.getLogger(this.getClass())

    /** The number of days to archive back from the current date */
    private final int NUMBER_OF_DAYS_FROM_CURRENT_DATE = -14

    /** The number of years to purge back from the current date */
    private final int NUMBER_OF_YEARS_FROM_CURRENT_DATE = -5

    /** Injected Property */
    DataAccessObject dao

    /** Injected Property */
    SecurityUtils securityUtils

    /** Injected Property */
    WorkOrderDao workOrderDao

    /** Injected Property */
    WorkOrderRowAdaptor workOrderRowAdaptor

    /** Injected Property */
    WoStatusRowAdaptor woStatusRowAdaptor

    /** Injected Property */
    WoPurgeAdaptor woPurgeAdaptor

    /** Injected Property */
    ServiceGroupItemRowAdaptor serviceGroupItemRowAdaptor

    /** {@inheritDoc} */
    List<WoStatusRow> getAllActiveWorkOrderStatuses() {
        List<WoStatusRow> list = new ArrayList<WoStatusRow>()
        List<WoStatus> listStatuses = dao.getObjectsByExample(new WoStatus(enabled:new Integer(1)))
        for (WoStatus w : listStatuses) {
            list.add(woStatusRowAdaptor.adapt(w))
        }
        return list
    }

    /** {@inheritDoc} */
    List<WoStatusRow> getAllWoStatusItems() {
        List<WoStatusRow> list = new ArrayList<WoStatusRow>()
        List<WoStatus> listWoStatuses = dao.getAllObjects(WoStatus.class)
        for (WoStatus s : listWoStatuses) {
            list.add(woStatusRowAdaptor.adapt(s))
        }
        return list
    }

    /** {@inheritDoc} */
    List<WorkOrderRow> getFilteredWorkOrders(WorkOrderRow workOrderRow, Integer firstResult, Integer maxRows) {
        List<WorkOrderRow> list = new ArrayList<WorkOrderRow>()
        WorkOrder workOrder = workOrderRowAdaptor.merge(workOrderRow)
        //extract the database id and creation date from to form the work order number
        workOrder.workOrderId = workOrderRow?.woNumber?.toString()?.substring(8)?.toInteger()
        DateFormat df = new SimpleDateFormat("yyyyMMdd")
        String date = workOrderRow?.woNumber?.toString()?.substring(0,8)
        if (date != null) {
            Date createDate = df.parse(date)
            workOrder.createDateTime = createDate
        }
        //determine what the user's role can view
        Users user = (Users)dao.getObjectById(securityUtils.getUserIdLoggedIn(), Users.class)
        List<WorkOrder> listWorkOrders = null
        switch (user.roleId.role) {
            case 'ROLE_USER':
                workOrder.userId = user //can only see work orders they submit
                listWorkOrders = workOrderDao.filterWorkOrdersForUser(workOrder, firstResult, maxRows)
                break
            case 'ROLE_LOC_HEAD':
                workOrder.locationId.locationId = user.locationId.locationId  //can see all work orders in location
                listWorkOrders = workOrderDao.filterWorkOrdersForLocHead(workOrder, firstResult, maxRows)
                break
            case 'ROLE_DEPT_HEAD':
                workOrder.locationId.deptId.deptId = user.deptId.deptId  //can see all work orders in dept
                listWorkOrders = workOrderDao.filterWorkOrdersForDeptHead(workOrder, workOrderRow.startCreateDateTime, workOrderRow.endCreateDateTime, firstResult, maxRows)
                break
            case 'ROLE_MTNCE':
                workOrder.mtnceGroup = user.userId //can only see work order in their maintenance group
                listWorkOrders = workOrderDao.filterWorkOrdersForMtnce(workOrder, workOrderRow.startCreateDateTime, workOrderRow.endCreateDateTime, firstResult, maxRows)
                break
            case 'ROLE_SUPER':
                //can see all work orders
                listWorkOrders = workOrderDao.filterWorkOrdersForSuper(workOrder, workOrderRow.startCreateDateTime, workOrderRow.endCreateDateTime, firstResult, maxRows)
                break
        }
        for (WorkOrder w : listWorkOrders) {
            list.add(workOrderRowAdaptor.adapt(w))
        }
        return list
    }

    /** {@inheritDoc} */
    Integer getCountWorkOrders(WorkOrderRow workOrderRow) {
        Integer count = 0
        WorkOrder workOrder = workOrderRowAdaptor.merge(workOrderRow)
        //extract the database id and creation date from to form the work order number
        workOrder.workOrderId = workOrderRow?.woNumber?.toString()?.substring(8)?.toInteger()
        DateFormat df = new SimpleDateFormat("yyyyMMdd")
        String date = workOrderRow?.woNumber?.toString()?.substring(0,8)
        if (date != null) {
            Date createDate = df.parse(date)
            workOrder.createDateTime = createDate
        }
        //determine what the user's role can view
        Users user = (Users)dao.getObjectById(securityUtils.getUserIdLoggedIn(), Users.class)
        switch (user.roleId.role) {
            case 'ROLE_USER':
                workOrder.userId = user //can only see work orders they submit
                count = workOrderDao.countWorkOrdersForUser(workOrder)
                break
            case 'ROLE_LOC_HEAD':
                workOrder.locationId.locationId = user.locationId.locationId //can only see work orders in their location
                count = workOrderDao.countWorkOrdersForLocHead(workOrder)
                break
            case 'ROLE_DEPT_HEAD':
                workOrder.locationId.deptId.deptId = user.deptId.deptId  //can see all work orders in dept
                count = workOrderDao.countWorkOrdersForDeptHead(workOrder, workOrderRow.startCreateDateTime, workOrderRow.endCreateDateTime)
                break
            case 'ROLE_MTNCE':
                workOrder.mtnceGroup = user.userId //can only see work order in their maintenance group
                count = workOrderDao.countWorkOrdersForMtnce(workOrder, workOrderRow.startCreateDateTime, workOrderRow.endCreateDateTime)
                break
            case 'ROLE_SUPER': //Role Super
                //can see all work orders
                count = workOrderDao.countWorkOrdersForSuper(workOrder, workOrderRow.startCreateDateTime, workOrderRow.endCreateDateTime)
                break
        }
        return count
    }

    /** {@inheritDoc} */
    Integer addNewWorkOrder(WorkOrderRow workOrderRow) {
        WorkOrder workOrder = workOrderRowAdaptor.merge(workOrderRow)
        Date theDate = new Date()
        workOrder.lastUpdatedDateTime = theDate
        workOrder.createDateTime = theDate
        workOrder.archiveId = new Archive(archiveId:new Integer(0))
        Users user = (Users)dao.getObjectById(securityUtils.getUserIdLoggedIn(), Users.class)
        workOrder.userId = user
        workOrder.lastUpdatedBy = "$workOrder.userId.fname $workOrder.userId.lname"
        workOrder.statusId = new WoStatus(statusId:new Integer(1))
        workOrder.woTypeId = (WoType)dao.load(WoType.class, workOrder.woTypeId.woTypeId)
        workOrder.locationId = (Location)dao.load(Location.class, workOrder.locationId.locationId)
        workOrder = (WorkOrder)dao.saveAndReturn(workOrder)
        return workOrder.workOrderId
    }

    /** {@inheritDoc} */
    void updateStatusMtnceViewed(Integer id) {
        WorkOrder workOrder = (WorkOrder)dao.getObjectById(id, WorkOrder.class)
        Users user = (Users)dao.getObjectById(securityUtils.getUserIdLoggedIn(), Users.class)
        if (user.roleId.roleId.intValue() == 3) { //mtnce role
            if (workOrder.statusId.statusId.intValue() == 1) { //unviewed status
                //update fields
                workOrder.statusId = (WoStatus)dao.getObjectById(new Integer(2), WoStatus.class)
                workOrder.startDateTime = new Date()
                workOrder.lastUpdatedBy = "$user.fname $user.lname"
            }
        }
    }

    /** {@inheritDoc} */
    WorkOrderRow getWorkOrder(Integer id) {
        WorkOrderRow workOrderRow = workOrderRowAdaptor.adapt(dao.getObjectById(id, WorkOrder.class))
        return workOrderRow
    }

    /** {@inheritDoc} */
    WorkOrderRow getWorkOrderForEdit(Integer id) {
        WorkOrderRow workOrderRow = workOrderRowAdaptor.adapt(dao.getObjectById(id, WorkOrder.class))
        //remove inactive service items
        for (Iterator it = workOrderRow.listServiceItems.iterator(); it.hasNext();) {
            Integer servId = (Integer) it.next()
            ServiceItem serviceItem = (ServiceItem)dao.getObjectById(servId, ServiceItem.class)
            if (serviceItem.enabled == 0 || serviceItem.serviceGroupId.enabled == 0) {
                it.remove()
            }
        }
        return workOrderRow
    }

    /** {@inheritDoc} */
    void updateWorkOrder(WorkOrderRow workOrderRow) {
        //If workorder type has been changed, update a pending status to unviewed status
        if (workOrderRow.woTypeId != workOrderRow.woTypeIdOld && workOrderRow.woStatusId == 2) {
            workOrderRow.woStatusId = 1
        }
        WorkOrder workOrder = (WorkOrder)workOrderRowAdaptor.merge(workOrderRow)
        //Associate the objects with the session so hibernate does not throw transient object exception.
        workOrder.woTypeId = (WoType)dao.load(WoType.class, workOrder.woTypeId.woTypeId)
        workOrder.locationId = (Location)dao.load(Location.class, workOrder.locationId.locationId)
        workOrder.userId = (Users)dao.load(Users.class, workOrder.userId.userId)
        Users user = (Users)dao.getObjectById(securityUtils.getUserIdLoggedIn(), Users.class)
        workOrder.lastUpdatedBy = "$user.fname $user.lname"
        dao.merge(workOrder)
    }

    /** {@inheritDoc} */
    void updateArchiveIndicators() {
        Calendar now = Calendar.getInstance()
        now.add(Calendar.DAY_OF_YEAR, NUMBER_OF_DAYS_FROM_CURRENT_DATE)
        int numRecsUpd = workOrderDao.updateArchiveInd(now.getTime())
        logger.info "The number of work order records archived: $numRecsUpd"
    }

    /** {@inheritDoc} */
    void purgeWorkOrders() {
        Calendar now = Calendar.getInstance()
        now.add(Calendar.YEAR, NUMBER_OF_YEARS_FROM_CURRENT_DATE)
        List<WorkOrder> purgeList = workOrderDao.completedWo(now.getTime())
        Date purgeDateTime = new Date()
        for (WorkOrder w : purgeList) {
            //insert to purge table
            WoPurge woPurge = woPurgeAdaptor.adapt(w)
            woPurge.purgeDateTime = purgeDateTime
            dao.save(woPurge)
            //remove from online system
            w.serviceItemMany.clear()
            dao.delete w
        }
        logger.info "The number of work order records purged: $purgeList.size"
    }

}