import groovy.sql.Sql
import java.sql.ResultSet

/**
 * Script to update email addresses from a file Marv Olson provided.
 *
 * @author jdolinski
 * 6/7/2010
 *
 * Record layout - ROLE_NAME,LNAME,FNAME,USERNAME,PASSWORD,Name,EMAIL
 *            IE - 408 S. 18TH STREET,Dept Head,HIGGINS,KEVIN,khiggins,Khiggins184,KEVIN.HIGGINS,KEVIN.HIGGINS@dotcomm.org

 */

String fileLocation = 'marv_wouseremail.csv'
Integer matchCnt = 0
//Test
//Sql sql = Sql.newInstance("jdbc:db2://martina.co.douglas.ne.us:50002/dbt202", "mtn001", "mtn001", "com.ibm.db2.jcc.DB2Driver")
//Production
Sql sql = Sql.newInstance("jdbc:db2://martina.co.douglas.ne.us:50002/dbp202", "mtn001", "mtn001", "com.ibm.db2.jcc.DB2Driver")
new File(fileLocation).splitEachLine(',') {List tokens ->
  String email = tokens[7].toLowerCase()
  String uname = tokens[4]
  sql.query ("Select user_id from mtn001.users where username = $uname", { ResultSet rs ->
      if (rs.next()) {
        if (email != null) {
          matchCnt++
          Integer userId = rs.getInt(1)
          //println "Found user: $uname id: $userId updating email address $email"
          String update = "update mtn001.users set email = '$email' where user_id = $userId"
          println update
          sql.execute(update)
        }
      } else {
          println "Unable to find user: $uname"
      }
      rs.close()
  })
}
println "\nMatched $matchCnt users."
