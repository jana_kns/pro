package org.dotcomm.pagecode.reports.adaptors

import java.text.Format
import java.text.SimpleDateFormat
import org.dotcomm.dto.propertymaintenance.WorkOrder
import org.dotcomm.pagecode.rows.MaterialRow
import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.util.Adaptor

/**
 * Adaptor pattern to convert persistant dtos and report views.
 *
 */
class ReportWorkOrderRowAdaptor implements Adaptor {

    /**
     * Adapt persistant dto for view.
     * @return object for view
     */
    public Object adapt(Object obj) {
        WorkOrderRow workOrderRow = new WorkOrderRow()
        WorkOrder workOrder = (WorkOrder) obj
        if (workOrder != null) {
            workOrderRow.woId = workOrder.workOrderId
            workOrderRow.createDateTime = workOrder.createDateTime
            workOrderRow.archived = workOrder.archiveId.archiveId.intValue() == 1 ? new Boolean(true) : new Boolean(false)
            workOrderRow.archivedString = workOrder.archiveId.archiveId.intValue() == 1 ? "Yes" : "No"
            Format fmt = new SimpleDateFormat("yyyyMMdd")
            String woNum = fmt.format(workOrder.createDateTime) + workOrder.workOrderId
            workOrderRow.woNumber = woNum.toLong()
            workOrderRow.woStatus = workOrder.statusId.name
            workOrderRow.woStatusId = workOrder.statusId.statusId
            workOrderRow.woLocation = workOrder.locationId.name
            workOrderRow.woLocationId = workOrder.locationId.locationId
            workOrderRow.woDepartment = workOrder.locationId.deptId.deptName
            workOrderRow.woDepartmentId = workOrder.locationId.deptId.deptId
            workOrderRow.woType = workOrder.woTypeId.name
            workOrderRow.woTypeId = workOrder.woTypeId.woTypeId
            workOrderRow.woTypeIdOld = workOrder.woTypeId.woTypeId
            workOrderRow.woRequestor = "$workOrder.userId.fname $workOrder.userId.lname"
            workOrderRow.woRequestorPhone = workOrder.userId?.phone
            workOrderRow.woRequestorEmail = workOrder.userId?.email
            workOrderRow.woRequestorUserId = workOrder.userId.userId
            workOrderRow.woReqComments = workOrder.requestComments
            Format formatter = new SimpleDateFormat("M/d/yyyy h:mm aaa")
            workOrderRow.woEnteredDate = workOrder.createDateTime
            workOrderRow.woLastUpdDate = workOrder.lastUpdatedDateTime
            String lastUpd = formatter.format(workOrder.lastUpdatedDateTime)
            workOrderRow.woLastUpdated = "$workOrder.lastUpdatedBy - $lastUpd"
            workOrderRow.woReqCompDate = workOrder.requestedCompDate
            workOrderRow.woStartDate = workOrder.startDateTime
            workOrderRow.woCompletionDate = workOrder.compDateTime
            workOrderRow.woInstructions = workOrder.workInstructions
            workOrderRow.woCompletionComments = workOrder.compComments
            workOrderRow.woChrgManHrsAcct = workOrder.chrgManHrsAcct
            workOrderRow.woChrgPartsAcct = workOrder.chgrPartsAcct
            workOrderRow.woNumberPersons = workOrder.numPersons
            workOrderRow.woNumberTrips = workOrder.numTrips
            workOrderRow.woManHrs = workOrder.laborManHrs
            workOrderRow.woTravelHrs = workOrder.travelManHrs
            workOrderRow.woTotalHrs = (workOrder.laborManHrs == null ? 0 : workOrder.laborManHrs) + (workOrder.travelManHrs == null ? 0 : workOrder.travelManHrs) 
            workOrderRow.woCompletedBy = workOrder.compBy
            workOrderRow.mtnceGroup = workOrder.mtnceGroup
            workOrderRow.listMaterials = new ArrayList<MaterialRow>()
            workOrderRow.listServiceItems = new ArrayList<Integer>()
        }
        else {
            return null
        }
        return workOrderRow
    }

    public Class<WorkOrder> getFromType() {
        return WorkOrder
    }

    public Class<WorkOrderRow> getToType() {
        return WorkOrderRow
    }

    /**
     * Merge object for view to persistant dto.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        throw new UnsupportedOperationException("Method not implemented.")
    }

}