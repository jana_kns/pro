package org.dotcomm.pagecode.reports

import org.dotcomm.services.AdminSecurityService

/**
 * Search criteria for reports.
 */
class ReportSearchCriteria {

    /** Injected */
    AdminSecurityService adminSecurityService

    Integer deptId
    Date createStartDate
    Date createEndDate
    Boolean archiveData

    @Override
    String toString() {
        return "${deptId==null?"":"Dept: " + adminSecurityService.getDept(deptId).deptName} ${createStartDate==null?"":"| Start: " + createStartDate.dateString} ${createEndDate==null?"":"| End: " + createEndDate.dateString}"
    }


}
