package org.dotcomm.pagecode.admin.messages

import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.util.FacesUtils
import org.dotcomm.pagecode.rows.MsgPostRow
import org.dotcomm.pagecode.admin.AdminMain

/**
 * Jsf managed bean for the editmsg.xhtml.
 *
 * @author jdolinski
 */
class EditMsg {

    /** Injected Property */
    AdminWorkOrderService adminWorkOrderService

    /** Page code data */
    MsgPostRow msgPostRow

    /** Constructor */
    EditMsg(AdminWorkOrderService adminWorkOrderService) {
        this.adminWorkOrderService = adminWorkOrderService
        Integer msgId = (Integer)FacesUtils.getPageFlowScopeParameter("msgid")
        msgPostRow = this.adminWorkOrderService.getMsgPostItem(msgId)
        if (msgPostRow == null) {
            msgPostRow = new MsgPostRow()
        }
    }

    /**
     * Action method to edit an existing message in the system.
     * @return Faces navigation text.
     */
    String updateMsg() {
        //todo fix the optimistic locking
        //this "if" is a hack to solve the problem of deleting a msg while another user is editing it.
        if (msgPostRow.msgId == null) {
            FacesUtils.addErrorMessage 'Please verify information and try again, the data was updated by another user.'
        }
        else {
            adminWorkOrderService.updateMsgPost(msgPostRow)
        }
        ((AdminMain)FacesUtils.getManagedBean('adminMain')).doSearch()
        return 'admin'
    }

}
