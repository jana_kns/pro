package org.dotcomm.pagecode.validators

import javax.faces.validator.ValidatorException
import javax.faces.context.FacesContext
import javax.faces.validator.Validator
import java.util.regex.Matcher
import javax.faces.component.UIComponent
import java.util.regex.Pattern
import javax.faces.application.FacesMessage
import org.apache.myfaces.trinidad.validator.ClientValidator
import org.apache.myfaces.trinidad.component.UIXInput
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.util.FacesUtils
import org.dotcomm.services.AdminSecurityService

/**
 * Jsf custom validator for user name.
 *
 * @author jdolinski
 */
class UserNameValidator implements Validator {

  /** Injected Property */
  AdminSecurityService adminSecurityService

  /**
   * Check to see if the username is already used in the system. Username's are unique.
   * @param context The current faces context.
   * @param component The component the validator is registered on.
   * @param value The form value submitted.
   */
  void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

      /** retrieve the string value of the field */
      String userName = (String)value

      /**
       * If we are editing a user, this variable will be populated with user's database id we are working with.
       *  This is the component id on the edituser.xhtml page.
       */
      UIXInput editPageUserId = (UIXInput)FacesUtils.findComponentInRoot("userid")
      Integer userId = (Integer)editPageUserId?.value

      /** Get the error msg from the message bundle */
      String errorMsg = FacesUtils.getStringFromBundle('usernameused')

      /** Look up the username in the database */
      UserRow userRow = adminSecurityService.getUser(userName)

      if (userRow != null) {  //check if user name is in the system
          if (userId != userRow.userId) { //check to see if the username belongs to another user
              FacesMessage msg = new FacesMessage()
              msg.setDetail(errorMsg)
              msg.setSummary(errorMsg)
              msg.setSeverity(FacesMessage.SEVERITY_ERROR)
              throw new ValidatorException(msg)
          }
      }
  }

}