package org.dotcomm.pagecode.rows

/**
 * Page code bean for displaying service group item information.
 *
 * @author jdolinski
 */
class ServiceGroupItemRow {

    Integer itemId
    Date lastUpd
    Integer groupId
    String name
    String groupName
    String groupDescr
    String status
    Boolean active
    Boolean dbStored
    boolean readOnly

}