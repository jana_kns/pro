package org.dotcomm.dto.propertymaintenance;

import java.util.Collection;

/**
 * DTO for the Archive table.
 *
 * @author jdolinski
 */
class Archive {

	Integer archiveId
	Collection workOrder
	String name
	String descr

}
