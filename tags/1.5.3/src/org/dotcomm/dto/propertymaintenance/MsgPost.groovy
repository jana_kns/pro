package org.dotcomm.dto.propertymaintenance

/**
 * DTO for the MsgPost table.
 *
 * @author jdolinski
 */
class MsgPost {

    Integer msgPostId
    Date lastUpdatedDateTime
    String msg
    String enteredBy
    String msgTitle

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MsgPost msgPost = (MsgPost) o;

        if (msgPostId != msgPost.msgPostId) return false;
        if (enteredBy != null ? !enteredBy.equals(msgPost.enteredBy) : msgPost.enteredBy != null) return false;
        if (lastUpdatedDateTime != null ? !lastUpdatedDateTime.equals(msgPost.lastUpdatedDateTime) : msgPost.lastUpdatedDateTime != null)
            return false;
        if (msg != null ? !msg.equals(msgPost.msg) : msgPost.msg != null) return false;
        if (msgTitle != null ? !msgTitle.equals(msgPost.msgTitle) : msgPost.msgTitle != null) return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = msgPostId;
        result = 31 * result + (lastUpdatedDateTime != null ? lastUpdatedDateTime.hashCode() : 0);
        result = 31 * result + (msg != null ? msg.hashCode() : 0);
        result = 31 * result + (enteredBy != null ? enteredBy.hashCode() : 0);
        result = 31 * result + (msgTitle != null ? msgTitle.hashCode() : 0);
        return result;
    }
}