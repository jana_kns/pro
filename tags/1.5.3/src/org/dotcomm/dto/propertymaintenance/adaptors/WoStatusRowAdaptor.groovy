package org.dotcomm.dto.propertymaintenance.adaptors

import org.dotcomm.dto.propertymaintenance.WoStatus
import org.dotcomm.pagecode.rows.WoStatusRow
import org.dotcomm.util.FacesUtils
import org.dotcomm.util.Adaptor

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class WoStatusRowAdaptor implements Adaptor {

    /**
     * Adapt persistant dto for view.
     * @return object for view
     */
    public Object adapt(Object obj) {
        WoStatusRow woStatusRow = new WoStatusRow()
        WoStatus woStatus = (WoStatus) obj
        if (woStatus != null) {
            woStatusRow.woStatusId = woStatus.statusId
            woStatusRow.woStatusName = woStatus.name
            woStatusRow.woStatusDescr = woStatus.descr
            woStatusRow.woStatusEnabledDescr = woStatus.enabled.intValue() == 1 ? 'Active' : 'Inactive'
            woStatusRow.woStatusEnabled = woStatus.enabled.intValue() == 1 ? new Boolean(true) : new Boolean(false)
        }
        else {
            return null
        }
        return woStatusRow
    }

    public Class<WoStatus> getFromType() {
        return WoStatus
    }

    public Class<WoStatusRow> getToType() {
        return WoStatusRow
    }

    /**
     * Merge object for view to persistant dto.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        WoStatus woStatus = new WoStatus()
        WoStatusRow woStatusRow = (WoStatusRow) obj
        if (woStatusRow != null) {
            woStatus.statusId = woStatusRow.woStatusId
            woStatus.name = woStatusRow.woStatusName
            woStatus.descr = woStatusRow.woStatusDescr
            woStatus.enabled = woStatusRow.woStatusEnabled ? new Integer(1) : new Integer(0)
        }
        else {
            return null
        }
        return woStatus
    }

}