package org.dotcomm.jobs

import org.springframework.scheduling.quartz.QuartzJobBean
import org.quartz.JobExecutionException
import org.quartz.JobExecutionContext
import org.dotcomm.services.WorkOrderService

/**
 * The following job will update workorder's archive indicator.
 * 
 * @author JDolinski
 */
class ArchiveJob extends QuartzJobBean {

    private WorkOrderService workOrderService

    protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
        workOrderService.updateArchiveIndicators()
    }
}