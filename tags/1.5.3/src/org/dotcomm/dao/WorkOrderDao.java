package org.dotcomm.dao;

import org.dotcomm.dto.propertymaintenance.WorkOrder;
import org.dotcomm.pagecode.reports.ReportSearchCriteria;
import org.springframework.security.annotation.Secured;
import org.springframework.security.AccessDeniedException;

import java.util.List;
import java.util.Date;

/**
 * Defines data access to the work_order table.
 *
 * @author JDolinski
 */
public interface WorkOrderDao {

    /**
     * Filter all work order for super roles.
     * @param workOrder A work order object
     * @param fromCreateDate The begining value for create date range
     * @param toCreateDate The ending value from create date range
     * @param firstResult The database pagination page to display.
     * @param maxRows The number of results to return from query.
     * @return List of Work Orders
     * @throws AccessDeniedException Thrown when the user is not authorized to invoke the method.
     */
    @Secured({"ROLE_SUPER"})
    List<WorkOrder> filterWorkOrdersForSuper(WorkOrder workOrder, Date fromCreateDate, Date toCreateDate, Integer firstResult, Integer maxRows) throws AccessDeniedException;

    /**
     * Count the number of work order records the filter will return for super roles.
     * @param workOrder A work order object
     * @param fromCreateDate The begining value for create date range
     * @param toCreateDate The ending value from create date range
     * @return List of Work Orders
     * @throws AccessDeniedException Thrown when the user is not authorized to invoke the method.
     */
    @Secured({"ROLE_SUPER"})
    Integer countWorkOrdersForSuper(WorkOrder workOrder, Date fromCreateDate, Date toCreateDate) throws AccessDeniedException;

    /**
     * Filter all work order for maintenance roles.
     * @param workOrder A work order object
     * @param fromCreateDate The begining value for create date range
     * @param toCreateDate The ending value from create date range
     * @param firstResult The database pagination page to display.
     * @param maxRows The number of results to return from query.
     * @return List of Work Orders
     * @throws AccessDeniedException Thrown when the user is not authorized to invoke the method.
     */
    @Secured({"ROLE_MTNCE"})
    List<WorkOrder> filterWorkOrdersForMtnce(WorkOrder workOrder, Date fromCreateDate, Date toCreateDate, Integer firstResult, Integer maxRows) throws AccessDeniedException;

    /**
     * Count the number of work order records the filter will return for maintenance roles.
     * @param workOrder A work order object
     * @param fromCreateDate The begining value for create date range
     * @param toCreateDate The ending value from create date range
     * @return List of Work Orders
     * @throws AccessDeniedException Thrown when the user is not authorized to invoke the method.
     */
    @Secured({"ROLE_MTNCE"})
    Integer countWorkOrdersForMtnce(WorkOrder workOrder, Date fromCreateDate, Date toCreateDate) throws AccessDeniedException;

    /**
     * Filter all work order for the department head, they can see all wo in their dept.
     * @param workOrder A work order object
     * @param fromCreateDate The begining value for create date range
     * @param toCreateDate The ending value from create date range
     * @param firstResult The database pagination page to display.
     * @param maxRows The number of results to return from query.
     * @return List of Work Orders
     * @throws AccessDeniedException Thrown when the user is not authorized to invoke the method.
     */
    @Secured({"ROLE_DEPT_HEAD"})
    List<WorkOrder> filterWorkOrdersForDeptHead(WorkOrder workOrder, Date fromCreateDate, Date toCreateDate, Integer firstResult, Integer maxRows) throws AccessDeniedException;

    /**
     * Count the number of work order records the filter will return for dept head roles.
     * @param workOrder A work order object
     * @param fromCreateDate The begining value for create date range
     * @param toCreateDate The ending value from create date range
     * @return List of Work Orders
     * @throws AccessDeniedException Thrown when the user is not authorized to invoke the method.
     */
    @Secured({"ROLE_DEPT_HEAD"})
    Integer countWorkOrdersForDeptHead(WorkOrder workOrder, Date fromCreateDate, Date toCreateDate) throws AccessDeniedException;

    /**
     * Filter all work order for user, they can only see wo they created.
     * @param workOrder A work order object
     * @param firstResult The database pagination page to display.
     * @param maxRows The number of results to return from query.
     * @return List of Work Orders
     * @throws AccessDeniedException Thrown when the user is not authorized to invoke the method.
     */
    @Secured({"ROLE_USER"})
    List<WorkOrder> filterWorkOrdersForUser(WorkOrder workOrder, Integer firstResult, Integer maxRows) throws AccessDeniedException;

    /**
     * Count the number of work order records the filter will return for user roles.
     * @param workOrder A work order object
     * @return List of Work Orders
     * @throws AccessDeniedException Thrown when the user is not authorized to invoke the method.
     */
    @Secured({"ROLE_USER"})
    Integer countWorkOrdersForUser(WorkOrder workOrder) throws AccessDeniedException;

    /**
     * Count the number of active work orders in a specific work order type.
     * @return List The work order type name and count.
     */
    List<Object[]> getWoTypesCounts(ReportSearchCriteria criteria);

    /**
     * Count the number of active work orders in a specific service group.
     * @param criteria The work order filter criteria.
     * @return List The service group name and count.
     */
    List<Object[]> getServiceGroupCounts(ReportSearchCriteria criteria);

    /**
     * Count the number of active work orders in a specific department.
     * @param criteria The work order filter criteria.
     * @return List The department names and count.
     */
    List<Object[]> getDeptCounts(ReportSearchCriteria criteria);

    /**
     * Count the number of active work orders created in the last 30 days.
     * @return List The date the work order was created and count for that day.
     */
    List<Object[]> getLastThirtyDayCreatedWorkOrders();

    /**
     * Count the active work order's statuses.
     * @return List The status and count.
     */
    List<Object[]> getWoStatusCounts(ReportSearchCriteria criteria);

    /**
     * Set the archive indicator to archived when the work order's last updated
     * date is less/equal to the date passed in.
     * @param archiveOlderThanThisDate Update records that are older/equal to this date.
     * @return int The number of records updated.
     */
    int updateArchiveInd(Date archiveOlderThanThisDate);

    /**
     * Get completed work orders where the work order's last updated date is less/equal to the date passed in.
     * @param olderThanThisDate Get records that are older/equal to this date.
     * @return list List of workorders.
     */
    List<WorkOrder> completedWo(Date olderThanThisDate);

    List<WorkOrder> woReportData(ReportSearchCriteria criteria);

    List<WorkOrder> servItemReportData(ReportSearchCriteria criteria);

}
