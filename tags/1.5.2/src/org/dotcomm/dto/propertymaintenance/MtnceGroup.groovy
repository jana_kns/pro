package org.dotcomm.dto.propertymaintenance;

/**
 * DTO for the MtnceGroup table.
 *
 * @author jdolinski
 */
class MtnceGroup {

	Integer mtnceGroupId
    Date createDateTime
    Collection usersMany
	Date lastUpdatedDateTime
	String name
	String descr
	Integer enabled
	Collection woTypeMany
	Collection locationMany

}
