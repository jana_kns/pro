package org.dotcomm.dto.propertymaintenance.adaptors

import org.dotcomm.util.Adaptor
import org.dotcomm.pagecode.rows.MtnceGroupRow
import org.dotcomm.dto.propertymaintenance.MtnceGroup
import org.dotcomm.util.FacesUtils
import org.dotcomm.dto.propertymaintenance.WoType
import org.dotcomm.persistence.DataAccessObject
import org.dotcomm.dto.propertymaintenance.Location
import org.dotcomm.dto.propertymaintenance.Users
import org.dotcomm.pagecode.rows.UserRow

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class MtnceGroupRowAdaptor implements Adaptor {

    /** Injected Property */
    DataAccessObject dao

    /**
     * Adapt persistant dto for view.
     * @return object for view
     */
    public Object adapt(Object obj) {
        MtnceGroupRow mtnceGroupRow = new MtnceGroupRow()
        MtnceGroup mtnceGroup = (MtnceGroup) obj
        if (mtnceGroup != null) {
            mtnceGroupRow.groupId = mtnceGroup.mtnceGroupId
            mtnceGroupRow.lastUpd = mtnceGroup.lastUpdatedDateTime
            mtnceGroupRow.groupName = mtnceGroup.name
            mtnceGroupRow.groupDesc = mtnceGroup.descr
            mtnceGroupRow.groupStatus = mtnceGroup.enabled.intValue() == 1 ? 'Active' : 'Inactive'
            mtnceGroupRow.active = mtnceGroup.enabled.intValue() == 1 ? new Boolean(true) : new Boolean(false)
            mtnceGroupRow.listWoTypeSelections = new ArrayList<Integer>()
            //Assign the types of work orders the maintenance group handles.
            for (WoType wt : mtnceGroup.woTypeMany) {
                mtnceGroupRow.listWoTypeSelections.add(new Integer(wt.woTypeId))
            }
            mtnceGroupRow.listLocations = new ArrayList<Integer>()
            //Assign the locations the maintenance group handles.
            for (Location l : mtnceGroup.locationMany) {
                mtnceGroupRow.listLocations.add(new Integer(l.locationId))
            }
            mtnceGroupRow.listMtncePersonnelSelections = new ArrayList<Integer>()
            mtnceGroupRow.listMtncePersonnel = new ArrayList<UserRow>()
            UserRowAdaptor userRowAdaptor = new UserRowAdaptor()
            //Assign the personnel to the maintenance group.
            for (Users u : mtnceGroup.usersMany) {
                mtnceGroupRow.listMtncePersonnelSelections.add(new Integer(u.userId))
                mtnceGroupRow.listMtncePersonnel.add(userRowAdaptor.adapt(u))
            }
        }
        else {
            return null
        }
        return mtnceGroupRow
    }

    public Class<MtnceGroup> getFromType() {
        return MtnceGroup
    }

    public Class<MtnceGroupRow> getToType() {
        return MtnceGroupRow
    }

    /**
     * Merge object for view to persistant dto.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        MtnceGroup mtnceGroup = new MtnceGroup()
        MtnceGroupRow mtnceGroupRow = (MtnceGroupRow) obj
        if (mtnceGroupRow != null) {
            mtnceGroup.mtnceGroupId = mtnceGroupRow.groupId
            mtnceGroup.lastUpdatedDateTime = mtnceGroupRow.lastUpd
            mtnceGroup.name = mtnceGroupRow.groupName
            mtnceGroup.descr = mtnceGroupRow.groupDesc
            mtnceGroup.enabled = mtnceGroupRow.active ? new Integer(1) : new Integer(0)
            mtnceGroup.woTypeMany = new ArrayList<WoType>()
            //load() method always tries to return a proxy and only returns
            //an initialized instance if it is already managed in the current persistence context.
            //A get will always hit the database. Since we are just adding a item to a collection
            //we do not need to hit the db and get the value, the proxy will work just fine.
            for (Integer i : mtnceGroupRow.listWoTypeSelections) {
                mtnceGroup.woTypeMany.add(dao.load(WoType.class, i))
            }
            mtnceGroup.locationMany = new ArrayList<Location>()
            for (Integer i : mtnceGroupRow.listLocations) {
                mtnceGroup.locationMany.add(dao.load(Location.class, i))
            }
            mtnceGroup.usersMany = new ArrayList<Users>()
            for (Integer i : mtnceGroupRow.listMtncePersonnelSelections) {
                mtnceGroup.usersMany.add(dao.load(Users.class, i))
            }
        }
        else {
            return null
        }
        return mtnceGroup
    }

}