package org.dotcomm.util;

import java.util.Date;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;

public class MyLogger {

    public Object log(ProceedingJoinPoint call) throws Throwable
    {
        System.out.println("Aop is in the house");

        Object point =  call.proceed();

        return point;
    }

}