package org.dotcomm.util

import org.aspectj.lang.ProceedingJoinPoint
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException
import javax.faces.application.FacesMessage
import javax.faces.context.FacesContext

/**
 * The following catches hibernates exception for optimistic locking. 
 *
 * @author JDolinski
 */
class JsfHibernateOptimisticLockExceptionHandler extends TransactionCommitInterceptor {

    String errorMsg = 'Please verify information and try again, the data was updated by another user.'

    Object commitTransaction(ProceedingJoinPoint pjp) throws Throwable {
        try {
            return pjp.proceed()
        }
        catch (HibernateOptimisticLockingFailureException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, errorMsg))
        }
    }

}