package org.dotcomm.util;

import java.util.*;

import javax.faces.FactoryFinder;
import javax.faces.application.Application;
import javax.faces.application.ApplicationFactory;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIViewRoot;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;
import javax.faces.webapp.UIComponentTag;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.trinidad.context.RequestContext;

/**
 * Class used to expose jsf utilities.
 *
 */
public class FacesUtils {

    private static final String NO_RESOURCE_FOUND = "Missing resource: ";

    public static ServletContext getServletContext() {
        return (ServletContext) FacesContext.getCurrentInstance()
                .getExternalContext().getContext();
    }

    public static Object getManagedBean(String beanName) {
        Object o = getValueBinding(getJsfEl(beanName)).getValue(
                FacesContext.getCurrentInstance());

        return o;
    }

    public static void resetManagedBean(String beanName) {
        getValueBinding(getJsfEl(beanName)).setValue(
                FacesContext.getCurrentInstance(), null);
    }

    public static void setManagedBeanInSession(String beanName,
            Object managedBean) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
                .put(beanName, managedBean);
    }

    public static String getRequestParameter(String name) {
        return (String) FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get(name);
    }

    public static Object getPageFlowScopeParameter(String name) {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        return requestContext.getPageFlowScope().get(name);
    }

    public static void putPageFlowScopeParameter(String name, Object obj) {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.getPageFlowScope().put(name, obj);
    }

    public static void removePageFlowScopeParameter(Object obj) {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.getPageFlowScope().remove(obj);
    }

    public static void addInfoMessage(String msg) {
        addInfoMessage(null, msg);
    }

    public static void addInfoMessage(String clientId, String msg) {
        FacesContext.getCurrentInstance().addMessage(clientId,
                new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg));
    }

    public static void addErrorMessage(String msg) {
        addErrorMessage(null, msg);
    }

    public static void addErrorMessage(String clientId, String msg) {
        FacesContext.getCurrentInstance().addMessage(clientId,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
    }

    public static void addWarnMessage(String clientId, String msg) {
        FacesContext.getCurrentInstance().addMessage(clientId,
                new FacesMessage(FacesMessage.SEVERITY_WARN, msg, msg));
    }

    public static void addWarnMessage(String msg) {
        addWarnMessage(null, msg);
    }

    private static Application getApplication() {
        ApplicationFactory appFactory = (ApplicationFactory) FactoryFinder
                .getFactory(FactoryFinder.APPLICATION_FACTORY);
        return appFactory.getApplication();
    }

    private static ValueBinding getValueBinding(String el) {
        return getApplication().createValueBinding(el);
    }

    private static HttpServletRequest getServletRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance()
                .getExternalContext().getRequest();
    }

    private static Object getElValue(String el) {
        return getValueBinding(el).getValue(FacesContext.getCurrentInstance());
    }

    private static String getJsfEl(String value) {
        return "#{" + value + "}";
    }

    /**
     * Method for taking a reference to a JSF binding expression and returning
     * the matching object (or creating it).
     * 
     * @param expression
     *            EL expression
     * @return Managed object
     */
    public static Object resolveExpression(String expression) {
        FacesContext ctx = getFacesContext();
        Application app = ctx.getApplication();
        ValueBinding bind = app.createValueBinding(expression);
        return bind.getValue(ctx);
    }

    /**
     * Method for taking a reference to a JSF binding expression and returning
     * the matching Boolean.
     * 
     * @param expression
     *            EL expression
     * @return Managed object
     */
    public static Boolean resolveExpressionAsBoolean(String expression) {
        return (Boolean) resolveExpression(expression);
    }

    /**
     * Method for taking a reference to a JSF binding expression and returning
     * the matching String (or creating it).
     * 
     * @param expression
     *            EL expression
     * @return Managed object
     */
    public static String resolveExpressionAsString(String expression) {
        return (String) resolveExpression(expression);
    }

    /**
     * Convenience method for resolving a reference to a managed bean by
     * userName rather than by expression.
     * 
     * @param beanName
     *            name of managed bean
     * @return Managed object
     */
    public static Object getManagedBeanValue(String beanName) {
        StringBuffer buff = new StringBuffer("#{");
        buff.append(beanName);
        buff.append("}");
        return resolveExpression(buff.toString());
    }

    /**
     * Method for setting a new object into a JSF managed bean Note: will fail
     * silently if the supplied object does not match the type of the managed
     * bean.
     * 
     * @param expression
     *            EL expression
     * @param newValue
     *            new value to set
     */
    public static void setExpressionValue(String expression, Object newValue) {
        FacesContext ctx = getFacesContext();
        Application app = ctx.getApplication();
        ValueBinding bind = app.createValueBinding(expression);

        // Check that the input newValue can be cast to the property type
        // expected by the managed bean.
        // If the managed Bean expects a primitive we rely on Auto-Unboxing
        // I could do a more comprehensive check and conversion from the object
        // to the equivilent primitive but life is too short
        Class bindClass = bind.getType(ctx);
        if (bindClass.isPrimitive() || bindClass.isInstance(newValue)) {
            bind.setValue(ctx, newValue);
        }
    }

    /**
     * Convenience method for setting the value of a managed bean by userName
     * rather than by expression.
     * 
     * @param beanName
     *            name of managed bean
     * @param newValue
     *            new value to set
     */
    public static void setManagedBeanValue(String beanName, Object newValue) {
        StringBuffer buff = new StringBuffer("#{");
        buff.append(beanName);
        buff.append("}");
        setExpressionValue(buff.toString(), newValue);
    }

    /**
     * Convenience method for setting Session variables.
     * 
     * @param key
     *            object key
     * @param object
     *            value to store
     */
    public static void storeOnSession(String key, Object object) {
        FacesContext ctx = getFacesContext();
        Map sessionState = ctx.getExternalContext().getSessionMap();
        sessionState.put(key, object);
    }

    /**
     * Convenience method for getting Session variables.
     * 
     * @param key
     *            object key
     * @return session object for key
     */
    public static Object getFromSession(String key) {
        FacesContext ctx = getFacesContext();
        Map sessionState = ctx.getExternalContext().getSessionMap();
        return sessionState.get(key);
    }

    /**
     * Pulls a String resource from the property bundle that is defined under
     * the application &lt;message-bundle&gt; element in the faces config.
     * Respects Locale
     * 
     * @param key
     *            string message key
     * @return Resource value or placeholder error String
     */
    public static String getStringFromBundle(String key) {
        ResourceBundle bundle = getBundle();
        return getStringSafely(bundle, key, null);
    }

    /**
     * Convenience method to construct a <code>FacesMesssage</code> from a
     * defined error key and severity This assumes that the error keys follow
     * the convention of using <b>_detail</b> for the detailed part of the
     * message, otherwise the main message is returned for the detail as well.
     * 
     * @param key
     *            for the error message in the resource bundle
     * @param severity
     *            severity of message
     * @return Faces Message object
     */
    public static FacesMessage getMessageFromBundle(String key,
            FacesMessage.Severity severity) {
        ResourceBundle bundle = getBundle();
        String summary = getStringSafely(bundle, key, null);
        String detail = getStringSafely(bundle, key + "_detail", summary);
        FacesMessage message = new FacesMessage(summary, detail);
        message.setSeverity(severity);
        return message;
    }

    /**
     * Add JSF error message.
     * 
     * @param msg
     *            error message string
     */
    public static void addFacesErrorMessage(String msg) {
        FacesContext ctx = getFacesContext();
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, "");
        ctx.addMessage(getRootViewComponentId(), fm);
    }

    /**
     * Add JSF error message for a specific attribute.
     * 
     * @param attrName
     *            name of attribute
     * @param msg
     *            error message string
     */
    public static void addFacesErrorMessage(String attrName, String msg) {
        FacesContext ctx = getFacesContext();
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                attrName, msg);
        ctx.addMessage(getRootViewComponentId(), fm);
    }

    // Informational getters

    /**
     * Get view id of the view root.
     * 
     * @return view id of the view root
     */
    public static String getRootViewId() {
        return getFacesContext().getViewRoot().getViewId();
    }

    /**
     * Get component id of the view root.
     * 
     * @return component id of the view root
     */
    public static String getRootViewComponentId() {
        return getFacesContext().getViewRoot().getId();
    }

    /**
     * Get FacesContext.
     * 
     * @return FacesContext
     */
    public static FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    /*
     * Internal method to pull out the correct local message bundle
     */
    private static ResourceBundle getBundle() {
        FacesContext ctx = getFacesContext();
        UIViewRoot uiRoot = ctx.getViewRoot();
        Locale locale = uiRoot.getLocale();
        ClassLoader ldr = Thread.currentThread().getContextClassLoader();
        return ResourceBundle.getBundle(
                ctx.getApplication().getMessageBundle(), locale, ldr);
    }

    /**
     * Get an HTTP Request attribute.
     * 
     * @param name
     *            attribute userName
     * @return attribute value
     */
    public static Object getRequestAttribute(String name) {
        return getFacesContext().getExternalContext().getRequestMap().get(name);
    }

    /**
     * Set an HTTP Request attribute.
     * 
     * @param name
     *            attribute userName
     * @param value
     *            attribute value
     */
    public static void setRequestAttribute(String name, Object value) {
        getFacesContext().getExternalContext().getRequestMap().put(name, value);
    }

    /*
     * Internal method to proxy for resource keys that don't exist
     */

    private static String getStringSafely(ResourceBundle bundle, String key,
            String defaultValue) {
        String resource = null;
        try {
            resource = bundle.getString(key);
        } catch (MissingResourceException mrex) {
            if (defaultValue != null) {
                resource = defaultValue;
            } else {
                resource = NO_RESOURCE_FOUND + key;
            }
        }
        return resource;
    }

    public static UIComponent findComponentInRoot(String id) {
        UIComponent ret = null;

        FacesContext context = FacesContext.getCurrentInstance();
        if (context != null) {
            UIComponent root = context.getViewRoot();
            ret = findComponent(root, id);
        }

        return ret;
    }

    public static UIComponent findComponent(UIComponent base, String id) {
    // Is the "base" component itself the match we are looking for?
        if (id.equals(base.getId())) {
            return base;
        }

        // Search through our facets and children
        UIComponent kid = null;
        UIComponent result = null;
        Iterator kids = base.getFacetsAndChildren();
        while (kids.hasNext() && (result == null)) {
            kid = (UIComponent) kids.next();
            if (id.equals(kid.getId())) {
                result = kid;
                break;
            }
            result = findComponent(kid, id);
            if (result != null) {
                break;
            }
        }
        return result;
    }



}
