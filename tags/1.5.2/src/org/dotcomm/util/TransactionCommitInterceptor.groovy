package org.dotcomm.util

import org.aspectj.lang.ProceedingJoinPoint
import org.springframework.core.Ordered

/**
 * Abstract class that can be used as an aspect with AOP to wrap around a transaction.
 * 
 * @author JDolinski
 */
abstract class TransactionCommitInterceptor implements Ordered {

    int order

    abstract Object commitTransaction(ProceedingJoinPoint pjp) throws Throwable
}