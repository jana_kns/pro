package org.dotcomm.pagecode.rows

/**
 * Page code bean for displaying role information.
 *
 * @author jdolinski
 */
class RoleRow {

    Integer roleId
    String roleName
    String roleDesc
    String role

}