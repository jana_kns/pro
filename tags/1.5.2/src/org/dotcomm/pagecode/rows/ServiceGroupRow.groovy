package org.dotcomm.pagecode.rows

/**
 * Page code bean for displaying service group information.
 *
 * @author jdolinski
 */
class ServiceGroupRow {

    Integer servGroupId
    Date lastUpd
    String servGroup
    String servGroupDesc
    String servGroupStatus
    Boolean active
    List<ServiceGroupItemRow> listItems

}