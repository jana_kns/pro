package org.dotcomm.pagecode.rows

/**
 * Page code bean for displaying department information.
 *
 * @author jdolinski
 */
class DeptRow {

    Integer deptId
    Date lastUpd
    String deptName
    String deptDesc
    String deptStatus
    String deptPhone
    Boolean deptActive
    List<LocationRow> listLocations
    List<UserRow> listUsers
    
}