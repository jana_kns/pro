package org.dotcomm.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXmlExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * Utility to generate jasper reports from a jsf application.
 * 
 * @author jdolinski
 *
 */
public class JasperJsfReportUtil {
	
	/** Available jasper report formats */
	public enum Format {
		csv, html, pdf, rtf, xml
	}

	/**
	 * Runs the specified jasper report.
     * 
	 * @param format Type of format to export.
	 * @param reportNameLoc The name and location of the jrxml file.
	 * @param data The list of pojos used to populate the report.
     * @throws JRException Rethrow the jasper exception.
     * @throws IOException Rethrow the io exception.
	 */
	public void runReport(Format format, String criteria, String reportNameLoc, List data) throws JRException, IOException {
		Map<String,String> parameters = new HashMap<String,String>();
        parameters.put("parms",criteria);
		parameters.put("format", format.name());
		parameters.put("WEBDIR", FacesUtils.getServletContext().getRealPath("/"));
        // build report
        JasperPrint jasperPrint = buildReport(reportNameLoc, data, parameters);
        // export to format
        ByteArrayOutputStream byteStream = formatExport(format, jasperPrint);
        // place report in output stream
        writeToOutputStream(format, byteStream);
        // end faces lifecycle
        FacesUtils.getFacesContext().getApplication().getStateManager().saveSerializedView(FacesUtils.getFacesContext());
        FacesUtils.getFacesContext().responseComplete();
	}

	/**
	 * @param format The format.
	 * @param byteStream The bytestream.
	 * @throws IOException The exception.
	 */
	private void writeToOutputStream(Format format, ByteArrayOutputStream byteStream) throws IOException {
		byte[] bytes = byteStream.toByteArray();
		byteStream.close();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
		response.setContentType("application/" + format);
		response.addHeader("Content-Disposition", "attachment; filename=report." + format);
		response.setContentLength(bytes.length);
		ServletOutputStream ouputStream = response.getOutputStream();
		ouputStream.write(bytes, 0, bytes.length);
		ouputStream.flush();
		ouputStream.close();
	}

	/**
	 * Create the jasper report.
	 * 
	 * @param reportNameLoc The location of the report.
	 * @param data The data for the report.
	 * @param parameters Any parms.
	 * @return JasperPrint
	 * @throws JRException Jasper reports exception.
	 */
	private JasperPrint buildReport(String reportNameLoc, List data, Map parameters) throws JRException {
		InputStream is = FacesUtils.getServletContext().getResourceAsStream(reportNameLoc);
		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(data);
		JasperDesign jasperDesign = JRXmlLoader.load(is);
		JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
		return JasperFillManager.fillReport(jasperReport, parameters, ds);
	}

	/**
	 * Exports the jasper report to a specified format.
	 * 
	 * @param format The format.
	 * @param jasperPrint JasperPrint object.
	 * @return A byte array output stream
	 * @throws JRException Jasper reports exception.
	 */
	private ByteArrayOutputStream formatExport(Format format, JasperPrint jasperPrint) throws JRException {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		switch (format) {
		case csv:
			JRCsvExporter csvExporter = new JRCsvExporter();
			csvExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			csvExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, jasperPrint.getName() + format);
			csvExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, byteStream);
			csvExporter.exportReport();
			break;
		case html:
			JRHtmlExporter htmlExporter = new JRHtmlExporter();
			htmlExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			htmlExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, jasperPrint.getName() + format);
			htmlExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, byteStream);
			htmlExporter.exportReport();
			break;
		case pdf:
			JRPdfExporter pdfExporter = new JRPdfExporter();
			pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			pdfExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, jasperPrint.getName() + format);
			pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, byteStream);
			pdfExporter.exportReport();
			break;
		case rtf:
			JRRtfExporter rtfExporter = new JRRtfExporter();
			rtfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			rtfExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, jasperPrint.getName() + format);
			rtfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, byteStream);
			rtfExporter.exportReport();
			break;
		case xml:
			JRXmlExporter xmlExporter = new JRXmlExporter();
			xmlExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			xmlExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, jasperPrint.getName() + format);
			xmlExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, byteStream);
			xmlExporter.exportReport();
			break;
		}
		return byteStream;
	}

}
