package org.dotcomm.dto.propertymaintenance.adaptors

import org.dotcomm.util.Adaptor
import org.dotcomm.dto.propertymaintenance.WoPurge
import org.dotcomm.dto.propertymaintenance.WorkOrder
import java.lang.UnsupportedOperationException
import java.lang.UnsupportedOperationException
import org.dotcomm.dto.propertymaintenance.ServiceItem
import org.dotcomm.dto.propertymaintenance.Materials

/**
 * Adaptor pattern to convert Work Order to the Purged Work Order.
 *
 * @author jdolinski
 */
class WoPurgeAdaptor implements Adaptor {

    /**
     * Adapt work order to purge object.
     * @return object for purge
     */
    public Object adapt(Object obj) {
        WoPurge woPurge = new WoPurge()
        WorkOrder workOrder = (WorkOrder) obj
        if (workOrder != null) {
            woPurge.workOrderId = workOrder.workOrderId
            woPurge.createDateTime = workOrder.createDateTime
            woPurge.lastUpdatedDateTime = workOrder.lastUpdatedDateTime
            woPurge.lastUpdatedBy = workOrder.lastUpdatedBy
            woPurge.woType = workOrder.woTypeId.name
            woPurge.location = workOrder.locationId.name
            woPurge.status = workOrder.statusId.name
            woPurge.requestorName = "$workOrder.userId.fname $workOrder.userId.lname"
            woPurge.requestedCompDate = workOrder.requestedCompDate
            woPurge.requestComments = workOrder.requestComments
            woPurge.startDateTime = workOrder.startDateTime
            woPurge.compDateTime = workOrder.compDateTime
            woPurge.compBy = workOrder.compBy
            woPurge.workInstructions = workOrder.workInstructions
            woPurge.compComments = workOrder.compComments
            woPurge.estLaborManHrs = workOrder.estLaborManHrs
            woPurge.numTrips = workOrder.numTrips
            woPurge.numPersons = workOrder.numPersons
            woPurge.laborManHrs = workOrder.laborManHrs
            woPurge.travelManHrs = workOrder.travelManHrs
            woPurge.chrgManHrsAcct = workOrder.chrgManHrsAcct
            woPurge.chrgPartsAcct = workOrder.chgrPartsAcct
            String services = ''
            for (ServiceItem s : workOrder.serviceItemMany) {
                services = "$services $s.serviceGroupId.name - $s.name\n"
            }
            woPurge.services = services
            String materials = ''
            for (Materials m : workOrder.materials) {
                materials = "$materials $m.partMaterial $m.quantity $m.poNumber"
            }
            woPurge.materials = materials
        }
        else {
            return null
        }
        return woPurge
    }

    public Class<WorkOrder> getFromType() {
        return WorkOrder
    }

    public Class<WoPurge> getToType() {
        return WoPurge
    }

    /**
     * Merge purged object to work order.
     * @return persistant dto
     */
    public Object merge(Object obj) {
        throw new UnsupportedOperationException ('Merge is not implemented.')
    }

}