package org.dotcomm.pagecode.rows

/**
 * Page code bean for displaying a work order's status rows.
 *
 * @author jdolinski
 */
class WoStatusRow {

    Integer woStatusId
    String woStatusName
    String woStatusDescr
    String woStatusEnabledDescr
    Boolean woStatusEnabled

}