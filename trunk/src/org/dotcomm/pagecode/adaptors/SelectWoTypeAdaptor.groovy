package org.dotcomm.pagecode.adaptors

import org.dotcomm.util.Adaptor
import javax.faces.model.SelectItem
import org.dotcomm.pagecode.rows.WoTypeRow

/**
 * Adaptor pattern to convert persistant dtos and presentation views.
 *
 * @author jdolinski
 */
class SelectWoTypeAdaptor implements Adaptor {

    /**
     * Adapt a list of view rows to selection items.
     * @return list of select items
     */
    public Object adapt(Object obj) {
        List<SelectItem> listSelectItems = new ArrayList<SelectItem>()
        List<WoTypeRow> listWoTypeRows = (ArrayList<WoTypeRow>) obj
        for (WoTypeRow w : listWoTypeRows) {
            listSelectItems.add(new SelectItem(w.woTypeId, w.woTypeName, w.woTypeDesc))
        }
        return listSelectItems.sort {SelectItem it -> it.label}
    }

    public Class<ArrayList<WoTypeRow>> getFromType() {
        return ArrayList
    }

    public Class<ArrayList<SelectItem>> getToType() {
        return ArrayList
    }

    /**
     * Merge object from list of select items to list of view rows.
     * @return list view row
     */
    public Object merge(Object obj) {
        List<WoTypeRow> listWoTypeRows = new ArrayList<WoTypeRow>()
        List<SelectItem> listSelectItems = (ArrayList<SelectItem>) obj
        for (SelectItem i : listSelectItems) {
            listWoTypeRows.add(new WoTypeRow(woTypeId:i.value, woTypeName:i.label, woTypeDesc:i.description))
        }
        return listWoTypeRows
    }

}