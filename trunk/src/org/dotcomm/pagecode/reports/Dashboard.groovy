package org.dotcomm.pagecode.reports

import org.apache.myfaces.trinidad.model.ChartModel
import org.apache.myfaces.trinidad.event.ChartDrillDownEvent
import org.dotcomm.pagecode.reports.adaptors.ChartModelAdaptor
import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.util.JasperJsfReportUtil
import org.dotcomm.util.JasperJsfReportUtil.Format
import javax.faces.model.SelectItem
import org.dotcomm.pagecode.adaptors.SelectDeptAdaptor
import org.dotcomm.services.AdminSecurityService
import org.dotcomm.services.ReportService
import javax.faces.validator.ValidatorException
import javax.faces.component.UIComponent
import javax.faces.context.FacesContext
import org.dotcomm.util.FacesUtils
import org.richfaces.component.html.HtmlCalendar
import javax.faces.application.FacesMessage;

/**
 * Jsf managed bean for the dashboard.xhtml.
 *
 * @author jdolinski
 */
class Dashboard {

    /** Injected Property */
    ReportSearchCriteria criteria

    /** Injected Property */
    ReportService reportService

    /** Page code data */
    ChartModel statusChart

    /** Page code data */
    ChartModel deptChart

    /** Page code data */
    ChartModel servChart

    /** Page code data */
    ChartModel typeChart

    /** Page code data */
    List<SelectItem> listDepts

    /** Constructor. */
    Dashboard(ReportService reportService, AdminSecurityService adminSecurityService, ReportSearchCriteria criteria) {
        this.reportService = reportService
        this.criteria = criteria
        SelectDeptAdaptor selectDeptAdaptor = new SelectDeptAdaptor()
        listDepts = (List<SelectItem>)selectDeptAdaptor.adapt(adminSecurityService.getAllActiveDepartments())
        reset()
    }

  void drillDown(ChartDrillDownEvent e) {
        println 'In the chart listener!!!!!!!!!!!'
    }

  /** Create pdf of the details for the current work load. */
  void printDetailCurrentStatus() {
      criteria.archiveData = false
      List<WorkOrderRow> results = reportService.getWorkOrderReportData(criteria) //.sort() {WorkOrderRow it -> it.woLocation}
      JasperJsfReportUtil jasperJsfReportUtil = new JasperJsfReportUtil()
      jasperJsfReportUtil.runReport(Format.pdf, criteria.toString(), "/WEB-INF/reports/currentStatus.jrxml", results.sort {WorkOrderRow it -> it.woStatus}.reverse())
  }

  /** Create pdf of the details for the type of work orders. */
  void printDetailTypeWo() {
      criteria.archiveData = true
      List<WorkOrderRow> results = reportService.getWorkOrderReportData(criteria) //.sort() {WorkOrderRow it -> it.woLocation}
      JasperJsfReportUtil jasperJsfReportUtil = new JasperJsfReportUtil()
      jasperJsfReportUtil.runReport(Format.pdf, criteria.toString(), "/WEB-INF/reports/typeWorkOrders.jrxml", results.sort {WorkOrderRow it -> it.woType})
  }

  /** Create pdf of the details for department activity. */
  void printDetailDeptActivity() {
      criteria.archiveData = true
      List<WorkOrderRow> results = reportService.getWorkOrderReportData(criteria) //.sort() {WorkOrderRow it -> it.woLocation}
      JasperJsfReportUtil jasperJsfReportUtil = new JasperJsfReportUtil()
      jasperJsfReportUtil.runReport(Format.pdf, criteria.toString(), "/WEB-INF/reports/departmentActivity.jrxml", results.sort {WorkOrderRow it -> it.woDepartment})
  }

  /** Create pdf of the details for service areas. */
  void printDetailServArea() {
      criteria.archiveData = true
      List<WorkOrderRow> results = reportService.getServiceItemReportData(criteria)
      JasperJsfReportUtil jasperJsfReportUtil = new JasperJsfReportUtil()
      jasperJsfReportUtil.runReport(Format.pdf, criteria.toString(), "/WEB-INF/reports/serviceAreas.jrxml", results.sort {WorkOrderRow it -> it.serviceItems})
  }

  /**
   * Clear the search filters.
   */
  void reset() {
      Calendar now = Calendar.getInstance()
      now.add(Calendar.YEAR, -1)
      this.criteria.deptId = null
      this.criteria.createStartDate = now.getTime()
      this.criteria.createEndDate = new Date()
      doRefresh()
  }

  /**
   * Invoke search for work orders.
   */
  void doRefresh() {
      ChartModelAdaptor chartModelAdaptor = new ChartModelAdaptor()
      servChart = (ChartModel)chartModelAdaptor.adapt(reportService.getNumberOfWorkOrdersServiceGroups(criteria))
      deptChart = (ChartModel)chartModelAdaptor.adapt(reportService.getNumberOfWorkOrdersForEachDept(criteria))
      typeChart = (ChartModel)chartModelAdaptor.adapt(reportService.getNumberOfWorkByType(criteria))
      statusChart = (ChartModel)chartModelAdaptor.adapt(reportService.getWorkOrderStatusesInSystem(criteria))
  }

  /**
   * Validator to check the start date is before the completion date.
   * @param context The current faces context.
   * @param component The component the validator is registered on.
   * @param value The form value submitted.
   */
  void endDateTimeValidator(FacesContext context, UIComponent component, Object value) throws ValidatorException {
      /** retrieve the string value of the field */
      Date endDate = (Date)value

      /** Get the error msg from the message bundle */
      String errorMsg = "Create date to must be after from date."

      HtmlCalendar startDateCalendar = (HtmlCalendar)FacesUtils.findComponentInRoot("begincreatedate")
      Date beginDate = (Date)startDateCalendar?.value
      //get rid of seconds, milliseconds for comparison
      Calendar cal = Calendar.getInstance()
      try {
          cal.setTime beginDate
      } catch (NullPointerException e) {
          //start date not required for all status, if null completion date is filled in but no start date
          FacesMessage msg = new FacesMessage()
          msg.setDetail(errorMsg)
          msg.setSummary(errorMsg)
          msg.setSeverity(FacesMessage.SEVERITY_ERROR)
          throw new ValidatorException(msg)
      }
      cal.set(Calendar.MILLISECOND, 0)
      cal.set(Calendar.SECOND, 0)
      beginDate = cal.getTime()

      if (beginDate > endDate) {
          FacesMessage msg = new FacesMessage()
          msg.setDetail(errorMsg)
          msg.setSummary(errorMsg)
          msg.setSeverity(FacesMessage.SEVERITY_ERROR)
          throw new ValidatorException(msg)
      }

      //check we only have 1 year for range
      Calendar calendar = Calendar.getInstance()
      calendar.setTime(beginDate)
      calendar.add(Calendar.YEAR, 1)
      calendar.set(Calendar.HOUR,23)
      calendar.set(Calendar.MINUTE,59)
      calendar.set(Calendar.SECOND,59)
      if (calendar.getTime() < endDate) {
          String errorMessage = "Date range is limited to one year."
          FacesMessage msg = new FacesMessage()
          msg.setDetail(errorMessage)
          msg.setSummary(errorMessage)
          msg.setSeverity(FacesMessage.SEVERITY_ERROR)
          throw new ValidatorException(msg)
      }

  }


}
