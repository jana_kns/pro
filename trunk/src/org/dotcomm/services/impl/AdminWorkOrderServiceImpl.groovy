package org.dotcomm.services.impl

import org.dotcomm.dto.propertymaintenance.adaptors.MsgPostRowAdaptor
import org.dotcomm.dto.propertymaintenance.adaptors.ServiceGroupItemRowAdaptor
import org.dotcomm.dto.propertymaintenance.adaptors.ServiceGroupRowAdaptor
import org.dotcomm.dto.propertymaintenance.adaptors.WorkOrderTypeRowAdaptor
import org.dotcomm.pagecode.rows.MsgPostRow
import org.dotcomm.pagecode.rows.ServiceGroupItemRow
import org.dotcomm.pagecode.rows.ServiceGroupRow
import org.dotcomm.pagecode.rows.WoTypeRow
import org.dotcomm.persistence.DataAccessObject
import org.dotcomm.services.AdminWorkOrderService
import org.dotcomm.util.SecurityUtils
import org.dotcomm.dto.propertymaintenance.*

/**
 * The implementation for {@link AdminWorkOrderService}.
 *
 * @author JDolinski
 *
 */
class AdminWorkOrderServiceImpl implements AdminWorkOrderService {

    /** Injected Property */
    DataAccessObject dao

    /** Injected Property */
    SecurityUtils securityUtils

    /** Injected Property */
    MsgPostRowAdaptor msgPostRowAdaptor

    /** Injected Property */
    WorkOrderTypeRowAdaptor workOrderTypeRowAdaptor

    /** Injected Property */
    ServiceGroupRowAdaptor serviceGroupRowAdaptor

    /** Injected property */
    ServiceGroupItemRowAdaptor serviceGroupItemRowAdaptor

    /** {@inheritDoc} */
    List<MsgPostRow> getAllMsgPostItems() {
        List<MsgPostRow> list = new ArrayList<MsgPostRow>()
        for (MsgPost msgPost : dao.getAllObjects(MsgPost.class)) {
            list.add(msgPostRowAdaptor.adapt(msgPost))
        }
        return list.sort {MsgPostRow it -> it.msgDate}.reverse()
    }

    /** {@inheritDoc} */
    MsgPostRow getMsgPostItem(Integer id) {
        MsgPost msgPost = dao.getObjectById(id, MsgPost.class)
        MsgPostRow msgPostRow = msgPostRowAdaptor.adapt(msgPost)
        return msgPostRow
    }


    /** {@inheritDoc} */
    void addNewMsgPost(MsgPostRow msgPostRow) {
        MsgPost msgPost = msgPostRowAdaptor.merge(msgPostRow)
        msgPost.lastUpdatedDateTime = new Date()
        Users user = dao.getObjectById(securityUtils.getUserIdLoggedIn(), Users.class)
        msgPost.enteredBy = "$user.fname $user.lname"
        dao.save(msgPost)
    }

    /** {@inheritDoc} */
    void updateMsgPost(MsgPostRow msgPostRow) {
        MsgPost msgPost = msgPostRowAdaptor.merge(msgPostRow)
        Users user = dao.getObjectById(securityUtils.getUserIdLoggedIn(), Users.class)
        msgPost.enteredBy = "$user.fname $user.lname"
        dao.merge(msgPost)
    }

    /** {@inheritDoc} */
    void deleteMsgPost(Integer id) {
        MsgPost msgPost = dao.getObjectById(id, MsgPost.class)
        dao.delete(msgPost)
    }

    /** {@inheritDoc} */
    public List<ServiceGroupRow> getAllServiceGroupItems() {
        List<ServiceGroupRow> list = new ArrayList<ServiceGroupRow>()
        List<ServiceGroup> listServiceGroups = dao.getAllObjects(ServiceGroup.class)
        for (ServiceGroup s : listServiceGroups) {
            List<ServiceItem> i = dao.getObjectsByExample(new ServiceItem(serviceGroupId:s, enabled:new Integer(1)))
            s.serviceItem = i
            list.add(serviceGroupRowAdaptor.adapt(s))
        }
        return list.sort {ServiceGroupRow it -> it.lastUpd}.reverse()
    }

    /** {@inheritDoc} */
    public List<ServiceGroupRow> getAllActiveServiceGroupItems() {
        List<ServiceGroupRow> list = new ArrayList<ServiceGroupRow>()
        List<ServiceGroup> listServiceGroups = dao.getObjectsByExample(new ServiceGroup(enabled:new Integer(1)))
        for (ServiceGroup s : listServiceGroups) {
            list.add(serviceGroupRowAdaptor.adapt(s))
        }
        return list
    }

    /** {@inheritDoc} */
    public List<ServiceGroupItemRow> getAllActiveServiceItems() {
        List<ServiceGroupItemRow> list = new ArrayList<ServiceGroupItemRow>()
        List<ServiceItem> listServiceItems = dao.getObjectsByExample(new ServiceItem(enabled:new Integer(1)))
        for (ServiceItem s : listServiceItems) {
            if (s.serviceGroupId.enabled.intValue() == 1) { //if the service group is inactive, do not add.
                list.add(serviceGroupItemRowAdaptor.adapt(s))
            }
        }
        return list.sort {ServiceGroupItemRow it -> it.groupName}
    }

    /** {@inheritDoc} */
    public List<ServiceGroupItemRow> getAllServiceItems() {
        List<ServiceGroupItemRow> list = new ArrayList<ServiceGroupItemRow>()
        List<ServiceItem> listServiceItems = dao.getAllObjects(ServiceItem.class)
        for (ServiceItem s : listServiceItems) {
            list.add(serviceGroupItemRowAdaptor.adapt(s))
        }
        return list
    }

    /** {@inheritDoc} */
    ServiceGroupRow getServiceGroup(Integer id) {
        ServiceGroup serviceGroup = dao.getObjectById(id, ServiceGroup.class)
        ServiceGroupRow serviceGroupRow = serviceGroupRowAdaptor.adapt(serviceGroup)
        return serviceGroupRow
    }

    /** {@inheritDoc} */
    void addNewServiceGroup(ServiceGroupRow serviceGroupRow) {
        ServiceGroup serviceGroup = serviceGroupRowAdaptor.merge(serviceGroupRow)
        Date theDate = new Date()
        serviceGroup.createDateTime = theDate
        serviceGroup.lastUpdatedDateTime = theDate
        dao.save(serviceGroup)
        for (ServiceItem i : serviceGroup.serviceItem) {
            i.lastUpdatedDateTime = theDate
            i.createDateTime = theDate
            dao.save(i)
        }
    }

    /** {@inheritDoc} */
    void updateServiceGroup(ServiceGroupRow serviceGroupRow) {
        ServiceGroup serviceGroup = serviceGroupRowAdaptor.merge(serviceGroupRow)
        dao.merge(serviceGroup)
    }

    /** {@inheritDoc} */
    public List<WoTypeRow> getAllWorkOrderTypeItems() {
        List<WoTypeRow> list = new ArrayList<WoTypeRow>()
        List<WoType> listWoTypes = dao.getAllObjects(WoType.class)
        for (WoType w : listWoTypes) {
            list.add(workOrderTypeRowAdaptor.adapt(w))
        }
        return list.sort {WoTypeRow it -> it.woLastUpd}.reverse()
    }

    /** {@inheritDoc} */
    public List<WoTypeRow> getAllActiveWorkOrderTypeItems() {
        List<WoTypeRow> list = new ArrayList<WoTypeRow>()
        List<WoType> listWoTypes = dao.getObjectsByExample(new WoType(enabled:new Integer(1)))  
        for (WoType w : listWoTypes) {
            list.add(workOrderTypeRowAdaptor.adapt(w))
        }
        return list.sort {WoTypeRow it -> it.woTypeName}
    }

    /** {@inheritDoc} */
    WoTypeRow getWorkOrderTypeItem(Integer id) {
        WoType woType = dao.getObjectById(id, WoType.class)
        WoTypeRow woTypeRow = workOrderTypeRowAdaptor.adapt(woType)
        return woTypeRow
    }

    /** {@inheritDoc} */
    void addNewWoType(WoTypeRow woTypeRow) {
        WoType woType = workOrderTypeRowAdaptor.merge(woTypeRow)
        Date theDate = new Date()
        woType.lastUpdatedDateTime = theDate
        woType.createDateTime = theDate
        dao.save(woType)
    }

    /** {@inheritDoc} */
    void updateWoType(WoTypeRow workOrderTypeRow) {
        WoType woType = workOrderTypeRowAdaptor.merge(workOrderTypeRow)
        //if work order type is inactivated, remove all ties to maintenance groups
        if (woType.enabled.intValue() != 1) {
            woType.mtnceGroupMany = new ArrayList<MtnceGroup>()
            //update all mtnce groups
            Date theDate = new Date()
            for (MtnceGroup mg : dao.getAllObjects(MtnceGroup.class)) {
                mg.lastUpdatedDateTime = theDate
            }
        }
        dao.merge(woType)
    }

}