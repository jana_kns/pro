package org.dotcomm.services;

import org.dotcomm.pagecode.reports.ReportSearchCriteria;
import org.dotcomm.pagecode.rows.ChartRow;
import org.dotcomm.pagecode.rows.WorkOrderRow;

import java.util.List;

/**
 * Defines the business services reports in the application.
 *
 * @author JDolinski
 */
public interface ReportService {

    /**
     * Get data to display the count of work order statuses.
     * @return ChartRow The information for the graph.
     */
    ChartRow getWorkOrderStatusesInSystem(ReportSearchCriteria criteria);

    /**
     * Get data to display the count of work orders for the different departments.
     * @return ChartRow The information for the graph.
     */
    ChartRow getNumberOfWorkOrdersForEachDept(ReportSearchCriteria criteria);

    /**
     * Get data to display the count of work orders for each service group.
     * @param criteria Filter criteria.
     * @return ChartRow The information for the graph.
     */
    ChartRow getNumberOfWorkOrdersServiceGroups(ReportSearchCriteria criteria);

    /**
     * Get data to display the count of work orders for each type.
     * @return ChartRow The information for the graph.
     */
    ChartRow getNumberOfWorkByType(ReportSearchCriteria criteria);

    /**
     * Get work orders in the system for reporting.
     *
     * @param criteria Report search criteria.
     * @return list List of work orders
     */
    List<WorkOrderRow> getWorkOrderReportData(ReportSearchCriteria criteria);

    /**
     * Get service items in the system for reporting.
     *
     * @return list List of service items.
     */
    List<WorkOrderRow> getServiceItemReportData(ReportSearchCriteria criteria);

}
