package org.dotcomm.test.services.impl

import org.junit.Test
import org.dotcomm.dto.propertymaintenance.Location
import org.dotcomm.dto.propertymaintenance.WoType
import org.dotcomm.dto.propertymaintenance.WoStatus
import org.dotcomm.dto.propertymaintenance.Users
import org.dotcomm.dto.propertymaintenance.Archive
import org.dotcomm.dto.propertymaintenance.WorkOrder
import org.dotcomm.dto.propertymaintenance.Departments
import org.dotcomm.services.impl.AdminSecurityServiceImpl
import org.dotcomm.persistence.DataAccessObject
import org.dotcomm.dto.propertymaintenance.adaptors.DeptRowAdaptor
import org.dotcomm.pagecode.rows.DeptRow
import org.dotcomm.dto.propertymaintenance.adaptors.LocationRowAdaptor
import org.dotcomm.pagecode.rows.LocationRow
import org.dotcomm.dto.propertymaintenance.adaptors.UserRowAdaptor
import org.dotcomm.pagecode.rows.UserRow
import org.dotcomm.dto.propertymaintenance.Roles
import org.dotcomm.dto.propertymaintenance.adaptors.RoleRowAdaptor
import org.dotcomm.pagecode.rows.RoleRow
import org.dotcomm.dto.propertymaintenance.MtnceGroup
import org.dotcomm.dto.propertymaintenance.adaptors.MtnceGroupRowAdaptor
import org.dotcomm.pagecode.rows.MtnceGroupRow
import org.dotcomm.util.CustomUser
import org.springframework.security.GrantedAuthority
import org.dotcomm.util.SecurityUtils

/**
 * Junit test cases for the AdminSecurityServiceImpl
 * @author jdolinski
 */
class AdminSecurityServiceImplTest extends GroovyTestCase {

    AdminSecurityServiceImpl adminSecurityService
    Departments mockDept
    Location mockLocation
    WorkOrder mockWorkOrder
    WoStatus mockWoStatus
    Archive mockArchive
    WoType mockWoType
    Users mockUser
    Roles mockRole
    MtnceGroup mockMtnceGroup

    protected void setUp() {
        adminSecurityService = new AdminSecurityServiceImpl()
        adminSecurityService.deptRowAdaptor = new DeptRowAdaptor()
        adminSecurityService.locationRowAdaptor = new LocationRowAdaptor()
        adminSecurityService.userRowAdaptor = new UserRowAdaptor()
        adminSecurityService.roleRowAdaptor = new RoleRowAdaptor()
        adminSecurityService.mtnceGroupRowAdaptor = new MtnceGroupRowAdaptor()
        mockWoStatus = new WoStatus(statusId:1,name:"test active",descr:"test descr",enabled:1)
        mockArchive = new Archive(archiveId:1)
        mockWoType = new WoType(woTypeId:1)
        mockDept = new Departments(deptId:1,deptName:'junit',deptDescr:'descr',enabled:1)
        mockLocation = new Location(locationId:1,deptId:mockDept,enabled:1)
        mockRole = new Roles(roleId:1,role:'test')
        mockUser = new Users(userId:1,fname:'junit',lname:'test',username:'junittest',password:'Passw0rd',enabled:1,deptId:mockDept,roleId:mockRole,locationId:mockLocation)
        mockMtnceGroup = new MtnceGroup(mtnceGroupId:1,enabled:1)
        mockWorkOrder = new WorkOrder(workOrderId:1,archiveId:mockArchive,createDateTime:new Date(),statusId:mockWoStatus,locationId:mockLocation,woTypeId:mockWoType,userId:mockUser,lastUpdatedDateTime:new Date())
    }

    @Test
    void testGetAllDepartments() {
        List data = [mockDept]
        def dao = [
                getAllObjects:{
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        List<DeptRow> list = adminSecurityService.getAllDepartments()
        list.eachWithIndex {DeptRow d, int count ->
            Departments departments = (Departments)data[count]
            assert d.deptId == departments.deptId
            assert d.deptName == departments.deptName
            assert d.deptDesc == departments.deptDescr
            assert d.deptStatus == 'Active'
            assert d.deptActive == true
        }
        assert list.size() == 1
    }

    @Test
    void testGetAllActiveDepartments() {
        List data = [mockDept, mockDept]
        def dao = [
                getObjectsByExample:{o1 ->
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        List<DeptRow> list = adminSecurityService.getAllActiveDepartments()
        list.eachWithIndex {DeptRow d, int count ->
            Departments departments = (Departments)data[count]
            assert d.deptId == departments.deptId
            assert d.deptName == departments.deptName
            assert d.deptDesc == departments.deptDescr
            assert d.deptStatus == 'Active'
            assert d.deptActive == true
        }
        assert list.size() == 2
    }

    @Test
    void testAddNewDept() {
        def dao = [
                save:{}] as DataAccessObject
        DeptRow deptRow = new DeptRow(deptId:1,deptName:'addme')
        adminSecurityService.dao = dao
        adminSecurityService.addNewDept(deptRow)
        assert true
    }

    @Test
    void testUpdateDept() {
        def dao = [
                merge:{},
                getAllObjects:{o1 ->
                    Object o2}] as DataAccessObject
        DeptRow deptRow = new DeptRow(deptId:1,deptName:'addme')
        adminSecurityService.dao = dao
        adminSecurityService.updateDept(deptRow)
        assert true
    }

    @Test
    void testGetDept() {
        Departments data = mockDept
        def dao = [
                getObjectById:{o1,o2 ->
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        DeptRow value = adminSecurityService.getDept(new Integer(1))
        assertEquals value.deptId, data.deptId  
        assertEquals value.deptName, data.deptName
        assertEquals value.deptDesc, data.deptDescr
        assertEquals 1, data.enabled
    }

    @Test
    void testGetAllLocations() {
        List data = [mockLocation]
        def dao = [
                getAllObjects:{
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        List<LocationRow> list = adminSecurityService.getAllLocations()
        list.eachWithIndex {LocationRow l, int count ->
            Location location = (Location)data[count]
            assertEquals location.locationId, l.locId
            assertEquals location.deptId.deptId, l.locDeptId
            assertEquals 'Active', l.locStatus
            assertTrue l.locActive
        }
        assertEquals 1, list.size()
    }

    @Test
    void testGetAllActiveLocations_ForDeptHeadOrUser() {
        List data = [mockLocation, mockLocation]
        def dao = [
                getObjectById:{o1, o2 ->
                    mockUser},
                getObjectsByExample:{o1 ->
                    data}] as DataAccessObject
        def securityUtils = [
                getUserIdLoggedIn:{
                    mockUser.userId
                }] as SecurityUtils
        adminSecurityService.dao = dao
        adminSecurityService.securityUtils = securityUtils
        List<LocationRow> list = adminSecurityService.getAllActiveLocations()
        list.eachWithIndex {LocationRow l, int count ->
            Location location = (Location)data[count]
            assertEquals location.locationId, l.locId  
            assertEquals location.deptId.deptId, l.locDeptId
            assertEquals 'Active', l.locStatus
            assertTrue l.locActive
        }
        assertEquals 2, list.size()
    }

    @Test
    void testGetAllActiveLocations_ForMtnceOrSuper() {
        List data = [mockLocation, mockLocation]
        mockUser.roleId = new Roles(roleId:3,role:"ROLE_SUPER")
        def dao = [
                getObjectById:{o1, o2 ->
                    mockUser},
                getObjectsByExample:{o1 ->
                    data}] as DataAccessObject
        def securityUtils = [
                getUserIdLoggedIn:{
                    mockUser.userId
                }] as SecurityUtils
        adminSecurityService.dao = dao
        adminSecurityService.securityUtils = securityUtils
        List<LocationRow> list = adminSecurityService.getAllActiveLocations()
        list.eachWithIndex {LocationRow l, int count ->
            Location location = (Location)data[count]
            assert l.locId == location.locationId
            assert l.locDeptId == location.deptId.deptId
            assert l.locStatus == 'Active'
            assert l.locActive == true
        }
        assert list.size() == 2
        //user does not belong to department with above locations
        mockUser.deptId = new Departments(deptId:99)
        list = adminSecurityService.getAllActiveLocations()
        list.eachWithIndex {LocationRow l, int count ->
            Location location = (Location)data[count]
            assert l.locId == location.locationId
            assert l.locDeptId == location.deptId.deptId
            assert l.locStatus == 'Active'
            assert l.locActive == true
        }
        assert list.size() == 2
    }

    @Test
    void testGetAllUsers() {
        List data = [mockUser]
        def dao = [
                getAllObjects:{
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        List<UserRow> list = adminSecurityService.getAllUsers()
        list.eachWithIndex {UserRow u, int count ->
            Users user = (Users)data[count]
            assert u.userId == user.userId
            assert u.fname == user.fname
            assert u.lname == user.lname
            assert u.uname == user.username
            assert u.pwd == user.password
            assert u.userStatus == 'Active'
            assert u.active == true
        }
        assert list.size() == 1
    }

    @Test
    void testGetAllActiveMtnceUsers() {
        List data = [mockUser,mockUser]
        def dao = [
                getObjectsByExample:{o1 ->
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        List<UserRow> list = adminSecurityService.getAllActiveMtnceUsers()
        assert list.size() == 0
        mockUser.roleId = new Roles(roleId:3)
        list = adminSecurityService.getAllActiveMtnceUsers()
        list.eachWithIndex {UserRow u, int count ->
            Users user = (Users)data[count]
            assert u.userId == user.userId
            assert u.fname == user.fname
            assert u.lname == user.lname
            assert u.uname == user.username
            assert u.pwd == user.password
            assert u.userStatus == 'Active'
            assert u.active == true
        }
        assert list.size() == 2
    }

    @Test
    void testAddNewLocation() {
        def dao = [
                save:{},
                load:{o1, o2 ->
                    mockDept}] as DataAccessObject
        LocationRow locationRow = new LocationRow(locId:1)
        adminSecurityService.dao = dao
        adminSecurityService.addNewLocation(locationRow)
        assert true
    }

    @Test
    void testUpdateLocation() {
        def dao = [
                merge:{},
                getAllObjects:{o1 ->
                    Object o2},
                getObjectById:{o1, o2 ->
                    mockDept}] as DataAccessObject
        LocationRow locationRow = new LocationRow(locId:1)
        adminSecurityService.dao = dao
        adminSecurityService.updateLocation(locationRow)
        assert true
    }

    @Test
    void testGetLocation() {
        Location data = mockLocation
        def dao = [
                getObjectById:{o1,o2 ->
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        LocationRow value = adminSecurityService.getLocation(new Integer(1))
        assert data.locationId == value.locId
        assert data.name == value.locName
        assert data.descr == value.locDesc
        assert value.locStatus == 'Active'
        assert value.locActive == true
        assert data.deptId.deptId == value.locDeptId
    }

    @Test
    void addNewUser() {
        def dao = [
                save:{},
                load:{o1, o2 ->
                    mockDept}] as DataAccessObject
        UserRow userRow = new UserRow(userId:1,uname:'addme')
        adminSecurityService.dao = dao
        adminSecurityService.addNewUser(userRow)
        assert true
    }

    @Test
    void updateUser() {
        def dao = [
                merge:{},
                load:{o1, o2 ->
                    if (o1 == Roles) {
                        mockRole
                    }
                    else {
                        mockDept
                    }
                }] as DataAccessObject
        UserRow userRow = new UserRow(userId:1,uname:'addme')
        adminSecurityService.dao = dao
        adminSecurityService.updateUser(userRow)
        assert true
    }

    @Test
    void getUser_ByUsername() {
        Users data = mockUser
        def dao = [
                getObjectById:{o1,o2 ->
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        UserRow value = adminSecurityService.getUser(mockUser.username)
        assert data.userId == value.userId
        assert data.username == value.uname
        assert data.fname == value.fname
        assert data.lname == value.lname
        assert data.password == value.pwd
        assert value.userStatus == 'Active'
        assert value.active == true
        assert data.deptId.deptId == value.deptId
        assert data.roleId.roleId == value.roleId
    }

    @Test
    void getUser_ById() {
        Users data = mockUser
        def dao = [
                getObjectById:{o1,o2 ->
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        UserRow value = adminSecurityService.getUser(mockUser.userId)
        assert data.userId == value.userId
        assert data.username == value.uname
        assert data.fname == value.fname
        assert data.lname == value.lname
        assert data.password == value.pwd
        assert value.userStatus == 'Active'
        assert value.active == true
        assert data.deptId.deptId == value.deptId
        assert data.roleId.roleId == value.roleId
    }

    @Test
    void testGetUserLoggedIn() {
        Users data = mockUser
        def dao = [
                getObjectById:{o1,o2 ->
                    data}] as DataAccessObject
        def securityUtils = [
                getUserIdLoggedIn:{
                    mockUser.userId    
                }] as SecurityUtils
        adminSecurityService.dao = dao
        adminSecurityService.securityUtils = securityUtils
        UserRow value = adminSecurityService.getUserLoggedIn()
        assert data.userId == value.userId
        assert data.username == value.uname
        assert data.fname == value.fname
        assert data.lname == value.lname
        assert data.password == value.pwd
        assert value.userStatus == 'Active'
        assert value.active == true
        assert data.deptId.deptId == value.deptId
        assert data.roleId.roleId == value.roleId
    }

    @Test
    void testGetAllRoles() {
        List data = [mockRole]
        def dao = [
                getAllObjects:{
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        List<RoleRow> list = adminSecurityService.getAllRoles()
        list.eachWithIndex {RoleRow r, int count ->
            Roles roles = (Roles)data[count]
            assert r.roleId == roles.roleId
            assert r.roleName == roles.roleName
            assert r.role == roles.role
            assert r.roleDesc == roles.roleDescr
        }
        assert list.size() == 1
    }

    @Test
    void testGetAllMtnceGroups() {
        List data = [mockMtnceGroup]
        def dao = [
                getAllObjects:{
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        List<RoleRow> list = adminSecurityService.getAllMtnceGroups()
        list.eachWithIndex {MtnceGroupRow row, int count ->
            MtnceGroup mtnceGroup = (MtnceGroup)data[count]
            assert row.groupId == mtnceGroup.mtnceGroupId
            assert row.active == true
            assert row.groupStatus == 'Active'
        }
        assert list.size() == 1
    }

    @Test
    void testGetAllActiveMtnceGroups() {
        List data = [mockMtnceGroup]
        def dao = [
                getObjectsByExample:{o1 ->
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        List<RoleRow> list = adminSecurityService.getAllActiveMtnceGroups()
        list.eachWithIndex {MtnceGroupRow row, int count ->
            MtnceGroup mtnceGroup = (MtnceGroup)data[count]
            assert row.groupId == mtnceGroup.mtnceGroupId
            assert row.active == true
            assert row.groupStatus == 'Active'
        }
        assert list.size() == 1
    }

    @Test
    void testAddNewMtnceGroup() {
        def dao = [
                save:{}] as DataAccessObject
        MtnceGroupRow mtnceGroupRow = new MtnceGroupRow(groupId:1,groupName:'addme',active:true,groupDesc:'descr')
        adminSecurityService.dao = dao
        adminSecurityService.addNewMtnceGroup(mtnceGroupRow)
        assert true
    }

    @Test
    void testUpdateMtnceGroup() {
        def dao = [
                merge:{}] as DataAccessObject
        MtnceGroupRow mtnceGroupRow = new MtnceGroupRow(groupId:1,groupName:'addme',active:true,groupDesc:'descr')
        adminSecurityService.dao = dao
        adminSecurityService.updateMtnceGroup(mtnceGroupRow)
        assert true
    }

    @Test
    void testGetMtnceGroup() {
        MtnceGroup data = mockMtnceGroup
        def dao = [
                getObjectById:{o1,o2 ->
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        MtnceGroupRow value = adminSecurityService.getMtnceGroup(mockMtnceGroup.mtnceGroupId)
        assert data.mtnceGroupId == value.groupId
        assert value.active == true
        assert value.groupStatus == 'Active'
    }

    @Test
    void testLoadUserByUsername() {
        Users data = mockUser
        def dao = [
                getUniqueObjectByExample:{o1 ->
                    data}] as DataAccessObject
        adminSecurityService.dao = dao
        CustomUser value = adminSecurityService.loadUserByUsername(mockUser.username)
        assert data.userId == value.userId
        assert data.username == value.username
        assert data.password == value.password
        assert value.enabled == true
        value.authorities.collect {GrantedAuthority s ->
                assert data.roleId.role == s.authority
            }
    }

}