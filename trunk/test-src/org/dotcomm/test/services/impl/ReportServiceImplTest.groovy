package org.dotcomm.test.services.impl

import org.dotcomm.dao.WorkOrderDao
import org.dotcomm.dto.propertymaintenance.ServiceGroup
import org.dotcomm.dto.propertymaintenance.ServiceItem
import org.dotcomm.dto.propertymaintenance.WorkOrder
import org.dotcomm.pagecode.rows.ChartRow
import org.dotcomm.pagecode.rows.WorkOrderRow
import org.dotcomm.persistence.DataAccessObject
import org.junit.Test
import org.dotcomm.services.impl.ReportServiceImpl
import org.dotcomm.dto.propertymaintenance.Departments
import org.dotcomm.dto.propertymaintenance.Location
import org.dotcomm.dto.propertymaintenance.WoStatus
import org.dotcomm.dto.propertymaintenance.Archive
import org.dotcomm.dto.propertymaintenance.WoType
import org.dotcomm.dto.propertymaintenance.Users
import org.dotcomm.dto.propertymaintenance.adaptors.WorkOrderRowAdaptor
import org.dotcomm.pagecode.reports.ReportSearchCriteria

/**
 * Junit test cases for the ReportServiceImpl
 */
class ReportServiceImplTest extends GroovyTestCase {

    ReportServiceImpl reportService
    Departments mockDept
    Location mockLocation
    WorkOrder mockWorkOrder
    WoStatus mockWoStatus
    Archive mockArchive
    WoType mockWoType
    Users mockUser

    protected void setUp() {
        reportService = new ReportServiceImpl()
        reportService.workOrderRowAdaptor = new WorkOrderRowAdaptor()
        mockWoStatus = new WoStatus(statusId: 1, name: "test active", descr: "test descr", enabled: 1)
        mockArchive = new Archive(archiveId: 1)
        mockWoType = new WoType(woTypeId: 1)
        mockDept = new Departments(deptId: 1)
        mockLocation = new Location(locationId: 1, deptId: mockDept)
        mockUser = new Users(userId: 1)
        mockWorkOrder = new WorkOrder(workOrderId: 1, archiveId: mockArchive, createDateTime: new Date(), statusId: mockWoStatus, locationId: mockLocation, woTypeId: mockWoType, userId: mockUser, lastUpdatedDateTime: new Date())
    }

    @Test
    void testGetWorkOrderReportData() {
        List<WorkOrder> data = new ArrayList<WorkOrder>()
        data.add(mockWorkOrder)
        def workOrderDao = [
                woReportData: {o1 ->
                    data
                }] as WorkOrderDao
        reportService.workOrderDao = workOrderDao
        List list = reportService.getWorkOrderReportData(new ReportSearchCriteria())
        assertEquals "List should have one element!", 1, list.size()
    }

    @Test
    void testGetServiceItemReportData() {
        List<WorkOrder> data = new ArrayList<WorkOrder>()
        def workOrderDao = [
                servItemReportData: {o1 ->
                    data
                }] as WorkOrderDao
        reportService.workOrderDao = workOrderDao
        List list = reportService.getServiceItemReportData()
        assertTrue "List should be empty, no service items!", list.isEmpty()
        data.add(mockWorkOrder)
        mockWorkOrder.serviceItemMany = new ArrayList<ServiceItem>()
        mockWorkOrder.serviceItemMany.add(new ServiceItem(name: 'junit item', serviceItemId: 1, serviceGroupId: new ServiceGroup(name: "junit group")))
        list = reportService.getServiceItemReportData(new ReportSearchCriteria())
        assertEquals "List should have one element!", 1, list.size()
        assertEquals "Service item names should match!", 'junit group - junit item', ((WorkOrderRow) list.iterator().next()).serviceItems
    }

    @Test
    void testGetWorkOrderStatusesInSystem() {
        List data = [[100, "status1"].toArray(), [99, "status2"].toArray()]
        def dao = [
                getWoStatusCounts: {
                    data
                }] as WorkOrderDao
        reportService.workOrderDao = dao
        ChartRow c = reportService.getWorkOrderStatusesInSystem()
        c.groupLabels.eachWithIndex { String label, int count ->
            assert label == data[count][1]
        }
        c.yValues.eachWithIndex {List values, int count ->
            assert values.get(0) == data[count][0]
        }
    }

    @Test
    void testGetNumberOfWorkOrdersForEachDept() {
        List data = [[6, "dept1"].toArray(), [10, "dept2"].toArray()]
        def dao = [
                getDeptCounts: {
                    data
                }] as WorkOrderDao
        reportService.workOrderDao = dao
        ChartRow c = reportService.getNumberOfWorkOrdersForEachDept()
        c.groupLabels.eachWithIndex { String label, int count ->
            assert label == data[count][1]
        }
        c.yValues.eachWithIndex {List values, int count ->
            assert values.get(0) == data[count][0]
        }
    }

    @Test
    void testGetNumberOfWorkOrdersServiceGroups() {
        List data = [[6, "serv1"].toArray(), [10, "serv2"].toArray()]
        def dao = [
                getServiceGroupCounts: {
                    data
                }] as WorkOrderDao
        reportService.workOrderDao = dao
        ChartRow c = reportService.getNumberOfWorkOrdersServiceGroups()
        c.groupLabels.eachWithIndex { String label, int count ->
            assert label == data[count][1]
        }
        c.yValues.eachWithIndex {List values, int count ->
            assert values.get(0) == data[count][0]
        }
    }

    @Test
    void testGetNumberOfWorkByType() {
        List data = [[3, "name"].toArray(), [6, "wathasdf"].toArray()]
        def dao = [
                getWoTypesCounts: {
                    data
                }] as WorkOrderDao
        reportService.workOrderDao = dao
        ChartRow c = reportService.getNumberOfWorkByType()
        c.groupLabels.eachWithIndex { String label, int count ->
            assert label == data[count][1]
        }
        c.yValues.eachWithIndex {List values, int count ->
            assert values.get(0) == data[count][0]
        }
    }

}
