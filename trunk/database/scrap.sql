--User/Dept role - view sql

select
*
from mtn001.work_order wo
inner join mtn001.wo_type ty
on wo.wo_type_id = ty.wo_type_id
inner join mtn001.wo_status ws
on wo.status_id = ws.status_id
inner join mtn001.location loc
on wo.location_id = loc.location_id
inner join mtn001.departments dept
on loc.dept_id = dept.dept_id
inner join mtn001.users usr
on usr.dept_id = dept.dept_id
where usr.user_id = 1

--Mtnce role - view sql

select
*
from mtn001.work_order wo
inner join mtn001.wo_type ty
on wo.wo_type_id = ty.wo_type_id
inner join mtn001.wo_status ws
on wo.status_id = ws.status_id
inner join mtn001.location loc
on wo.location_id = loc.location_id
inner join mtn001.departments dept
on loc.dept_id = dept.dept_id
where (wo.wo_type_id in
(select
distinct(mgtype.wo_type_id)
from mtn001.users usr
inner join mtn001.roles role
on usr.role_id = role.role_id
inner join mtn001.user_mtnce_group ugroup
on ugroup.user_id = usr.user_id
inner join mtn001.mtnce_group mgroup
on ugroup.mtnce_group_id = mgroup.mtnce_group_id
inner join mtn001.mtnce_group_type mgtype
on mgtype.mtnce_group_id = mgroup.mtnce_group_id
where usr.user_id = 2)
and wo.location_id in
(select
distinct(mgloc.location_id)
from mtn001.users usr
inner join mtn001.roles role
on usr.role_id = role.role_id
inner join mtn001.user_mtnce_group ugroup
on ugroup.user_id = usr.user_id
inner join mtn001.mtnce_group mgroup
on ugroup.mtnce_group_id = mgroup.mtnce_group_id
left outer join mtn001.mtnce_group_location mgloc
on mgloc.mtnce_group_id = mgroup.mtnce_group_id
where usr.user_id = 2)
or wo.wo_type_id in (
select
distinct(mgtype.wo_type_id)
from mtn001.users usr
inner join mtn001.roles role
on usr.role_id = role.role_id
inner join mtn001.user_mtnce_group ugroup
on ugroup.user_id = usr.user_id
inner join mtn001.mtnce_group mgroup
on ugroup.mtnce_group_id = mgroup.mtnce_group_id
inner join mtn001.mtnce_group_type mgtype
on mgtype.mtnce_group_id = mgroup.mtnce_group_id
left outer join mtn001.mtnce_group_location mgloc
on mgloc.mtnce_group_id = mgroup.mtnce_group_id
where usr.user_id = 2
and mgloc.location_id is null
))


--
select
distinct(wo.work_order_id)
from mtn001.users usr
inner join mtn001.roles role
on usr.role_id = role.role_id
inner join mtn001.user_mtnce_group ugroup
on ugroup.user_id = usr.user_id
inner join mtn001.mtnce_group mgroup
on ugroup.mtnce_group_id = mgroup.mtnce_group_id
inner join mtn001.mtnce_group_type mgtype
on mgtype.mtnce_group_id = mgroup.mtnce_group_id
inner join mtn001.mtnce_group_location mgloc
on mgloc.mtnce_group_id = mgroup.mtnce_group_id
inner join mtn001.location loc
on loc.location_id = mgloc.LOCATION_ID
inner join mtn001.WO_TYPE wotype
on wotype.WO_TYPE_ID = mgtype.WO_TYPE_ID
inner join mtn001.WORK_ORDER wo
on wo.location_ID = loc.LOCATION_ID and wo.WO_TYPE_ID = wotype.WO_TYPE_ID
where usr.user_id = 51
order by wo.work_order_id

--NEW  Mtnce role - view sql

select
*
from mtn001.work_order wo
inner join mtn001.wo_type ty
on wo.wo_type_id = ty.wo_type_id
inner join mtn001.wo_status ws
on wo.status_id = ws.status_id
inner join mtn001.location loc
on wo.location_id = loc.location_id
inner join mtn001.departments dept
on loc.dept_id = dept.dept_id
where (wo.work_order_id in
(select
distinct(wo.work_order_id)
from mtn001.users usr
inner join mtn001.roles role
on usr.role_id = role.role_id
inner join mtn001.user_mtnce_group ugroup
on ugroup.user_id = usr.user_id
inner join mtn001.mtnce_group mgroup
on ugroup.mtnce_group_id = mgroup.mtnce_group_id
inner join mtn001.mtnce_group_type mgtype
on mgtype.mtnce_group_id = mgroup.mtnce_group_id
inner join mtn001.mtnce_group_location mgloc
on mgloc.mtnce_group_id = mgroup.mtnce_group_id
inner join mtn001.location loc
on loc.location_id = mgloc.LOCATION_ID
inner join mtn001.WO_TYPE wotype
on wotype.WO_TYPE_ID = mgtype.WO_TYPE_ID
inner join mtn001.WORK_ORDER wo
on wo.location_ID = loc.LOCATION_ID and wo.WO_TYPE_ID = wotype.WO_TYPE_ID
where usr.user_id = 2
)
or wo.wo_type_id in (
select
distinct(mgtype.wo_type_id)
from mtn001.users usr
inner join mtn001.roles role
on usr.role_id = role.role_id
inner join mtn001.user_mtnce_group ugroup
on ugroup.user_id = usr.user_id
inner join mtn001.mtnce_group mgroup
on ugroup.mtnce_group_id = mgroup.mtnce_group_id
inner join mtn001.mtnce_group_type mgtype
on mgtype.mtnce_group_id = mgroup.mtnce_group_id
left outer join mtn001.mtnce_group_location mgloc
on mgloc.mtnce_group_id = mgroup.mtnce_group_id
where usr.user_id = 2
and mgloc.location_id is null
))


--Super role - view sql

select
wo.work_order_id,
wo.status_id,
ws.name as status_name,
wo.location_id,
loc.name as loc_name,
loc.dept_id,
dept.dept_name,
wo.wo_type_id,
ty.name as wo_type
from mtn001.work_order wo
inner join mtn001.wo_type ty
on wo.wo_type_id = ty.wo_type_id
inner join mtn001.wo_status ws
on wo.status_id = ws.status_id
inner join mtn001.location loc
on wo.location_id = loc.location_id
inner join mtn001.departments dept
on loc.dept_id = dept.dept_id

--Note: where clause when needing to see what belongs to a particular maintenance group

select
*
from mtn001.work_order wo
inner join mtn001.wo_type ty
on wo.wo_type_id = ty.wo_type_id
inner join mtn001.wo_status ws
on wo.status_id = ws.status_id
inner join mtn001.location loc
on wo.location_id = loc.location_id
inner join mtn001.departments dept
on loc.dept_id = dept.dept_id
where (wo.wo_type_id in
(select
distinct(mgtype.wo_type_id)
from mtn001.mtnce_group mgroup
inner join mtn001.mtnce_group_type mgtype
on mgtype.mtnce_group_id = mgroup.mtnce_group_id
where mgroup.mtnce_group_id = 1)
and wo.location_id in
(select
distinct(mgloc.location_id)
from mtn001.mtnce_group mgroup
inner join mtn001.mtnce_group_location mgloc
on mgloc.mtnce_group_id = mgroup.mtnce_group_id
where mgroup.mtnce_group_id = 1)
or wo.wo_type_id in (
select
distinct(mgtype.wo_type_id)
from mtn001.mtnce_group mgroup
inner join mtn001.mtnce_group_type mgtype
on mgtype.mtnce_group_id = mgroup.mtnce_group_id
left outer join mtn001.mtnce_group_location mgloc
on mgloc.mtnce_group_id = mgroup.mtnce_group_id
where mgroup.mtnce_group_id = 1
and mgloc.location_id is null
))


hql working
  select wo from WorkOrder as wo inner join wo.woTypeId as wotype
  where wotype.woTypeId in
    (select distinct(wotype.woTypeId)
     from MtnceGroup as mgroup
     inner join mgroup.mtnceGroupType as mgtype
     inner join mgtype.woTypeId as wotype
     left outer join mgroup.mtnceGroupLocation mgloc
     left outer join mgloc.locationId as woloc
     where mgroup.mtnceGroupId = 1
     and woloc.locationId is null
    )
