/*
 * Apache Trinidad client side implementation for validating a work order number.
 * The implementation is used by org.dotcomm.pagecode.validators.PhoneNumValidator
 *
 *  First 4 characters (year) must be between 2000-2100
 *  Characters 5-6 (month) must be between 01-12, currently checking it is a between 00-19
 *                      need to enhance.
 *  Charaters 7-8 (day) is not being validated against month's day, currently checking it is a between 00-39
 *                      need to enhance.
 *  Characters 9-# (id) must be numeric
 */
WorkOrderNumValidator = function() {}

WorkOrderNumValidator.prototype = new TrValidator();

WorkOrderNumValidator.prototype.validate = function (value, label, converter) {
    var facesMessage = new TrFacesMessage("Validation Error",
                                          "Invalid Work Order Number.",
                                          TrFacesMessage.SEVERITY_ERROR);

    var re = new RegExp("^[2][0][0-9][0-9][0-1][0-9][0-3][0-9]\\d+$");

    if (!re.test(value)) {
        //todo get message from resource bundle, or inject
        throw new TrValidatorException(facesMessage);
    }
}